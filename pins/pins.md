######2015Apr02 07:41:05+0900

**Kirill Grouchnikov**

<http://www.pushing-pixels.org/>

---

######2015Apr03 01:15:32+0900

**MIUI for galaxy 2 HD LTE (e120s)**

<http://renewable.tistory.com/630> : cm9 cwm for e120s
<http://en.miui.com/download-221.html> : rom
<http://en.miui.com/forum.php?mod=viewthread&tid=19870&page=1#pid396822> : flash guide

---

######2015Apr24 08:19:15+0900

**malware (chrome extension popups..)**

<http://www.geekstogo.com/forum/topic/347444-please-help-to-remove-pop-ups/> : a good source for the thoughts. methodology.. of professionals.. : ADWCleaner
copy of the thread => [Please help to remove pop-ups - Geeks to Go Forum_files](Please help to remove pop-ups - Geeks to Go Forum_files.txt)

---

######2015Apr25 14:12:01+0900

**happy birthday in Ducth**

<http://goamsterdam.about.com/od/Basic-Facts/fl/How-to-Say-Happy-Birthday-in-Dutch.htm>

---

**OH!P.**

<https://www.youtube.com/watch?v=UEgy_xohTwA&app=desktop> : works
<https://www.youtube.com/watch?v=oSUSkA2soCc> : workshop

---

**Adrien M / Claire B**

<http://www.am-cb.net/> : media artist.

---

**Saloon (England, Reading) 's blog**

<http://salooncouk.blogspot.co.uk/p/blog-page_24.html>

---

**tara lcd monitor..**

좀 가뿐하고 적당한 가격의 모니터.. 구매가능하다는 곳. (hyu 연구실에서 사다가 쓴다는 곳.)

<http://www.taralcd.co.kr/shop/good.php?code=01>

---

**고무인.**

아빠 아는 분이 한다는데? : <http://www.doma.co.kr/> 가격이 더 쌈.
걍 있어보이는데.. 잘되는 집... : <http://dojangplus.com/_shop/shop/shop_item_list.html?start=0&cate_no=128905874763113000&p_cate_no=&sb_lmt=100>

여튼.. 고무인. 하고 싶으면 이런데 맡기면됨.

---

**totalcmd plugins**

<http://totalcmd.net/plugring/imagine.html> : imagine => Korean dev. made this. => <http://www.nyam.pe.kr/>, <http://www.nyam.pe.kr/comm/resume/Resume.html>

<http://totalcmd.net/plugring/slister.html> : slister => sumatraPDF (open source PDF, comic book, epub etc. viewer!) => dev. => <http://www.sumatrapdfreader.org/download-free-pdf-viewer.html>

---

**BLE**

"low power requirements, operating for "months or years" on a button cell"
<https://en.wikipedia.org/wiki/Bluetooth_low_energy>

---

**Gamuza : processing-like OF**

<http://forum.openframeworks.cc/t/gamuza-hybrid-live-of-sketching-ide/13204>

Lua.
Processing-like ide.
many interesting examples.

now => <http://gamuza.d3cod3.org/>

..

---

**GeekTool ! : live monitor your mac**

<http://lifehacker.com/244026/geek-to-live--monitor-your-mac-and-more-with-geektool> : Geek to Live: Monitor your Mac and more with GeekTool

<http://projects.tynsoe.org/en/geektool/ecrans.php>
<http://lifehacker.com/185802/download-of-the-day--geektool>

Rainmeter.. still there is no PROPER alternative for geektool @ windows.. until the best one is this.

<http://docs.rainmeter.net/tips>
<http://docs.rainmeter.net/snippets>
<http://superuser.com/questions/156829/simple-way-to-display-script-output-on-windows-desktop-like-geektool-on-osx>

BgInfo : simple and neat.. but very closed.
<https://technet.microsoft.com/en-us/sysinternals/bb897557.aspx>

didn't try this. but perhaps this could be BETTER than rainmeter.. more close to GeekTool ? : **samurize**
<http://lifehacker.com/213280/geek-to-live--incorporate-text-files-onto-your-desktop>
<http://www.samurize.com/modules/news/>

---

**pidgin @ odroid / linux => USE 'APP PASSWORD'**

odroid에 pidgin이 있어서, 얼씨구나 하고.. 좀 써볼라고 했더니만. (주로 인터넷공유해주는 내 메인 피씨에서 뭐 찾거나 코드 snippet이나 등등을 간단히 메시징하기 위해서..) authrization 에러가 막아서네?

해결법 찾았던 것은 아래 참고..

-

<https://support.google.com/a/answer/49147?hl=en> : google의 공식 정보..
<http://superuser.com/questions/324863/how-do-i-set-up-google-talk-in-pidgin> : 이것도 뭐 괜찮은거 같은데.. server에다가 talk.google.com 안적어도 되는 거 같음.
<http://mylinuxbook.com/how-to-get-pidgin-working-with-gtalk/> : 이게 실제 문제가 되는 구글의 새로운 2단계 인증에 대한 우회방법으로 app password를 생성하고 사용하는 방법을 소개함.

```
At first, I could not understand the reason behind it and tried to click the ‘Enabled’ check box multiple times (in the previous window) but it was of no use. Then I googled a bit and realised that Pidgin gives this kind of a problem while connecting to Gmail accounts that have Two-Step Verification ON. So was in my case.

To make Pidgin connect and sign-in properly, users need to generate and set an application specific password for Pidgin in your Google accounts.

To do this :

Go to your google accounts by logging in to http://accounts.google.com/
In the ‘Security’ menu (left side of the window), go to the 2-step verification section
Under this section., click the ‘Manage your application-specific passwords’ link.
Sign-in again (if prompted)
Set any name relevant to the application. For example, pidgin-messenger and hit the ‘Generate Password’ button.
Copy the password generated (along with the spaces, it doesn’t matter) and hit the ‘done’ button.
Now, hit the ‘Modify account’ button on the Pidgin window, paste the copied password in the ‘password’ text box and click the ‘save’ button. You will again be presented the ‘Add accounts’ window. In this window try again to tick the ‘Enabled’ check box
```

---

######2015Jun16 05:38:21+0900

**interesting person : linuxfactory / odroid kernel dev.**

xu3 커널 코드에 기여하고 있는 사람중 한명인데 아마 하드커널 사람이겠지? (한국 사람)
<http://linuxfactory.or.kr/>
블로그 재밌네.

odroid-pc를 만들어 쓰고 있으심..
**Jenkins**라는 걸 apache랑 쓰고 있다는 데 어떤 것인지 궁금하다
나중에 자세히 보자. => <https://jenkins-ci.org/>

---

**rpi + kinect**

<http://www.mariolukas.de/2015/04/proof-of-concept-3d-scanner-with-kinect-and-raspberry-pi2/> : rpi2 + kinect => 3d scanner
<http://stackoverflow.com/questions/17743479/raspberry-pi-with-kinect>
```
To answer your question, yes it is possible to get Image and depth on the raspberry pi!

Here is how to.

If you want to use just video (color, not depth) there is already a driver in the kernel! You can load it like this:

modprobe videodev
modprobe gspca_main
modprobe gspca_kinect
You get a new /dev/videoX and can use it like any other webcam!

If you need depth (which is why you want a kinect) you need another driver that can be found here: https://github.com/xxorde/librekinect

Both work well on the current Raspbian.
```
<https://github.com/xxorde/librekinect>

---

######2015Jul06 03:22:11+0900

**coderwall**

[coderwall.com : establishing geek cred since 1305712800](https://coderwall.com/)

**OpenRA**

[OpenRA/OpenRA](https://github.com/OpenRA/OpenRA)
[OpenRA - Home](http://www.openra.net/)

---

