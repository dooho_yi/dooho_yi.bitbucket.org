######2015Jun12 21:58:05+0900

adafruit

<http://www.adafruit.com/products/2122> : Laser Break Beam Sensor (OUT OF STOCK)
<http://www.adafruit.com/products/819> : Micro Switch w/Roller Lever - Three Terminal
<http://www.adafruit.com/products/888> : Aluminum iPhone 4S 0.8mm 5-Point Star/Pentalobe Screwdriver
<http://www.adafruit.com/products/1799> : 3.5mm (1/8") DIY 4-Pole (TRRS) Plug
<http://www.adafruit.com/products/1669> : Stereo Enclosed Speaker Set - 3W 4 Ohm
<http://www.adafruit.com/products/725> : Terminal Block - 3-pin 3.5mm - pack of 5!
<http://www.adafruit.com/products/453> : MAX7219CNG LED Matrix/Digit Display Driver - MAX7219
<http://www.adafruit.com/products/732> : MCP23017 - i2c 16 input/output port expander (100k pull-up)
<http://www.adafruit.com/products/1674> : Bone Conductor Transducer with Wires - 8 Ohm 1 Watt
<http://www.adafruit.com/products/856> : MCP3008 - 8-Channel 10-Bit ADC With SPI Interface
<http://www.adafruit.com/products/597> : Mini Thermal Receipt Printer
<http://www.adafruit.com/product/814> : Miniature WiFi (802.11b/g/n) Module: For Raspberry Pi and more
<http://www.adafruit.com/products/1030> : USB WiFi (802.11b/g/n) Module with Antenna for Raspberry Pi
<http://www.adafruit.com/products/1327> : Bluetooth 4.0 USB Module (v2.1 Back-Compatible)
<http://www.adafruit.com/products/1407> : Inductive Charging Set - 5V @ 500mA max
<http://www.adafruit.com/products/1697> : Bluefruit LE - Bluetooth Low Energy (BLE 4.0) - nRF8001 Breakout - v1.0
<http://www.adafruit.com/products/2479> : Adafruit Bluefruit LE UART Friend - Bluetooth Low Energy (BLE)
<http://www.adafruit.com/products/2267> : Bluefruit LE Friend - Bluetooth Low Energy (BLE 4.0) - nRF51822 - v1.0 => this adds BLE for Windows or etc.
