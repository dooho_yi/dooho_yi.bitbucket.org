name: title
layout: true
class: center, middle, title
---
name: content
layout: true
class: left, middle, content

.footnote[ACC/LKS/LCA 용 데이타 로거 / SECO INTERFACE / USE THE CODE.]
---
name: imgonly
layout: true
class: center, middle, imgonly

.footnote[ACC/LKS/LCA 용 데이타 로거 / SECO INTERFACE / USE THE CODE.]
---
layout: false
template: title
## 운전 미숙자 지원을 위한 자동 차선 변경 시스템 개발
### 동계 워크샵
#### 2015-01-30
#### SECO INTERFACE / 이두호

---
template: content

**전체 과제 목표**

* "고속화 도로에서 운전미숙자의 차선 변경 의도가 있는 경우, 자동으로 충돌 위험 인지, 판단, 조향을 수행하여 안전하게 차선을 자동 변경하는 시스템 기술 개발"

**담당 분야**

* ACC/LKS/LXC HVI 개발
* LXC용 영상 모니터링 시스템 개발 (전/후/측방)
* 운전자 의지 판단용 steering wheel switch 시스템 개발

---
template: content

**진행 사항 보고**

* HVI 개선
  * 오프라인 데이터 리플레이 구현
  * LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용
    * LXC 제어 프로파일이 있어야 가능
  * 영상 캘리브레이션 편의 기능 개선
  * 기타 편의 기능 및 정보 모니터링 기능 추가
* HVI 시안
* HYU SCL 워크숍 2회 실시
  * 개발자용 HVI 코드 설명회
  * 영상 켈리브레이션 방법 실습
* steering wheel switch 및 BCM 모듈 제어기 제작 및 검증 완료

---
template: content

**오프라인 데이터 리플레이 구현**

* 4채널 영상과 2채널 CAN 데이터 로그의 오프라인 리플레이 구현
  * 대외 - 개발자들 시험 내용 분석용
  * 대내 - HVI 개발을 용이하게 하기 위한 자체 레퍼런스
  * 일단 기능 중심으로 구현
  * 로그 파일 형식 등 세부사항 조정 예정

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="768" height="480" data-setup="{}" style="background-color: black;"><source src="data/HVI_replay.mov" type='video/mp4' /></video>

---
template: content

**LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용**

* 목표 결과

<img src="data/img1.png" width=200>
<img src="data/img2.png" width=200>
<img src="data/img3.png" width=200>
<img src="data/img4.png" width=200>

---
template: content

**LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용**

* 아이디어 1 : 변경 대상 차선을 기준으로 삼아, 해당 차선의 Position / Heading 측정치를 자차의 이동 Position / Heading으로 역변환 처리
    * 문제점 1 : 해당 차선의 Position / Heading 측정에러가 LXC 수행 중 유지된다 보장 없음
        * -> 에러치의 변동만큼 화면 상의 모든 표시 물체가 영향을 받게 됨.
        * -> 불필요하게 흔들리는 움직임은 운전자에게 괜한 불안감 초래. 
    * 문제점 2 : 차선 변경 도중 차선의 위치가 바뀜. 좌->우(좌측 차선 변경시), 우->좌(우측 차선 변경시)
        * -> 즉, 차선의 정체성이 바뀌는 시점. 좌 차선이 우차선으로 바뀌는 정확한 시점을 알 길이 없음.

* 아이디어 2 : LXC 시작 시점의 자차의 위치로 부터 계획한 LXC 프로파일을 따라 GUI 상의 자차도 이동.
    * 문제점 1 : 정확한 자차의 위치를 표시하고 있는 것은 아님
        * -> 해명 : HVI 상에서는 전반적인 경향만 흔들거림 없이 보여줄 수 있으면 문제 없음.
        * -> 대안 1 : LXC 제어 중 자차의 Heading / 횡Position의 보정된 '추정치'가 계산 및 제공된다면, 이용 가능.
        * -> 대안 2 : 자체적으로 Ego모션 추정
            * -> 프로젝트 범위 벗어남.

---
template: content

**LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용**

* 협의할 내용 - 다음 중 택 1.
  1) LXC 제어 중, 제어 프로파일 생성 후, 자차의 Heading / 횡Position을 일정 주기로 CAN BUS에 송신
  2) LXC 제어 수행 중, 자차의 위치 추정치에서 계산되는 Heading / 횡Position을 일정 주기로 CAN BUS에 송신

* 추후 합의시, 구체적인 CAN 인터페이스 도출.

* 실험
  * 위 경우 중 1번 안의 경우에 대한 실험.
  * 자차의 횡방향위치 변위 +-2.5m (차선 폭 가정치)를 만족하는 좌/우 차선변경 시나리오에 맞춰, 임의의 프로파일 적용
  * 프로파일 정보가 약 40ms 주기로 CAN데이터로 업로드되는 상황 가정.
  * 이 때, 예상되는 카메라 센서의 차선 측정 데이터 값은 '이상적'인 것으로 가정하고 시뮬레이션. (측정 에러 = 0)

---
template: content

**LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용**

* 임의의 프로파일 적용 GUI 작동 테스트
* 임의의 자차속도 가정 (등속도, 프로파일 모양을 그래프로 표현시에만 필요함)

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="768" height="480" data-setup="{}" style="background-color: black;"><source src="data/HVI_lxc_with_profile.mov" type='video/mp4' /></video>

---
template: content

**LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용**

* 임의의 프로파일 적용 GUI 작동 테스트

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="768" height="480" data-setup="{}" style="background-color: black;"><source src="data/HVI_lxc_with_profile_stopped.mov" type='video/mp4' /></video>

---
template: content

**LXC 수행 중 변경 대상 차선을 중심점으로 좌표계 적용**

* 임의의 프로파일 적용 GUI 작동 테스트

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="768" height="480" data-setup="{}" style="background-color: black;"><source src="data/HVI_lxc_profile_2.mov" type='video/mp4' /></video>

* 추가 진행 예정 : 자차 이미지 표시하고, 자차와 측정 좌표계의 이동을 표현.

---
template: content

**영상 캘리브레이션 편의 기능 개선**

* 영상 켈리브레이션 수치를 프로그램 rebuild 없이, 바로 적용할 수 있도록, json 형식으로 입력.

<img src="data/json.png" width=500>

* 추가 진행 예정 : 기준 이미지 1개와 측정 수치를 입력시켜, 켈리브레이션 매트릭스를 바로 계산해주는 간단한 별도 프로그램 제작

---
template: content

**기타 편의 기능 추가**

* 결과 분석을 위한 기능 주가
  * zoom in/out & panning

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="768" height="480" data-setup="{}" style="background-color: black;"><source src="data/HVI_zoom.mov" type='video/mp4' /></video>

---
template: content

**기타 편의 기능 추가**

* 결과 분석을 위한 기능 주가
  * zoom in/out & panning

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="768" height="480" data-setup="{}" style="background-color: black;"><source src="data/HVI_pan.mov" type='video/mp4' /></video>

---
template: content

**HYU SCL 워크숍 2회 실시**

* 개발자용 HVI 코드 설명회 (10월 13일)
  * LXC HVI의 사용법 및 코드 주요 이슈 설명회

* 영상 켈리브레이션 방법 실습 (10월 24일)
  * HVI 사용법 실차 환경에서 실습 및 영상 켈리브레이션 과정 실습

---
template: content

**steering wheel switch 및 BCM 모듈 제어기 제작 및 검증 완료**

<img src="data/SWSBCMs.png" width=700>

* SWS/BCM 모듈 => 상위 제어기 - CAN ID : 0x110 (시험용)
* SWS/BCM 모듈 <= 상위 제어기 - CAN ID : 0x200 (시험용)

* CAN Specification

(그림)

---
template: content

**steering wheel switch 및 BCM 모듈 제어기 제작 및 검증 완료**

* Steering wheel switch 입력 => CAN BUS => 상위 제어기

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="720" height="405" data-setup="{}" style="background-color: black;"><source src="data/SWSBCM_internal_no_audio.mov" type='video/mp4' /></video>

---
template: content

**steering wheel switch 및 BCM 모듈 제어기 제작 및 검증 완료**

* 상위 제어기 => CAN BUS => BCM 모듈 제어

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="720" height="405" data-setup="{}" style="background-color: black;"><source src="data/SWSBCM_inside.mov" type='video/mp4' /></video>

---
template: content

**steering wheel switch 및 BCM 모듈 제어기 제작 및 검증 완료**

* 상위 제어기 => CAN BUS => BCM 모듈 제어

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="720" height="405" data-setup="{}" style="background-color: black;"><source src="data/SWSBCM_outside.mov" type='video/mp4' /></video>

* 현재 적용된 로직은 Steering wheel switch 입력 시 CAN 입출력을 생략하고 BCM을 즉시 제어함 (시험 검증 용)
  * 차후 CAN 스펙 결정과 함께, 개별 동작하도록 업데이트 예정

* CAN 스펙 협의 필요
* 실차 적용 일정 수립 필요

---
template: content

**2차년도 종료시 까지, 진행 내용 및 일정**

* 2차년도 작업 종료 시점 - 3월말까지 완료.
* LXC 수행 시 좌표계 눈금이 따라서 움직이지 않는 상태 개선 (2월 중)
* calibration 알고리즘 계산 로직을 별도의 미니 프로그램으로 구현 (NO MATLAB) (2월 중)
* HVI 시안 3항 도출 (2월~3월 중순)
* 4채널 영상 켈리브레이션 완료 + ME Camera + DELPHI Radar + BCM 모듈 + 기타 등등 설치 상태로 실차 공로 시험 데이터 1set 이상 획득 희망 (HVI 개발용) (2월~3월 중순)

(표 작성)

---
template: content

**3차년도 진행 예상 내용**

* ECU Timestamp를 적용하는 버젼으로 migrate (3차년도)
* OpenCV => OpenGL기반(openframeworks)로 migration 검토 => OpenGL의 장점 : 시각화 기능 대폭 지원됨. / 3차원 모델 기본 지원. (3차년도)
* 도로상 물체를 적절히 표시하기 위한 간단한 Tracking 적용 필요

---
template: content

감사합니다.
