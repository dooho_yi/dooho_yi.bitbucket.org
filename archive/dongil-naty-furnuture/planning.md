
Stage #1 - Research
Stage time frame - 3 weeks
Budget - $xxxx
Activity - research of parallel spaces, available materials from company, available materials from workshop, etc..
Goal - Come up with a plan of space and list of objects that need to be made
Deliverables - Floor plan, mood board (inspiration board) with look and feel decision and spec (decision on style, look, feel, interactive behaviour…)
Mile Stone Deliverable Material - Spec sheet of style, material, floor plan and list of objects…

######2015Jun21 12:06:27+0900

---

* task #1 : concept / inspiration
* **goal : To come up with a plan of space and list of objects that need to be made**
* activities
  1. **draw/benchmark/brainstorm on the concept of the space**
  2. **draw/benchmark/brainstorm on floor plan, flow analysis**
  3. **draw/benchmark/brainstorm on shapes and appearance of the objects**
  4. **draw/benchmark/brainstorm on 'smart' furniture idea for the objects**
  5. **make experimental prototypes of the objects**
* deliverables
  * mood board (inspirational sketches & drawings)
  * floor plan
  * list of objects
  * objects - experimental prototypes
  * 'smart' object idea - experimental prototypes
* time frame
  * **? weeks**
~~* budget~~
  ~~* **USD ??00 (not including material fee)**~~
* decision should be concretely made on...
  * *floor plan*
  * *list of objects*
  * *objects - specification (decision on style/look/feel)*
  * *'smart' object - specification (decision on function)*

---

* task #2 : feasibility / research on methods, materials and analysis
  * **goal : To come up with a more feasible plan of space and objects in the aspect of methodology of making, materials to be used and user interaction**
* activities
  6. **research on material/method of local shops**
  7. **research on material/method of DRB co. (Dongil Rubber Belt) (e.g. rubber etc.)**
  8. **research on material/method on electronics & electrics of local shops**
  9. **research on finishing methods including mastering, coloring**
  10. **research on user interaction of the objects**
* deliverables
  * designs/making plan sheets for the objects as manuals along with research archive
  * objects - where to get materials/how to make/estimated price
  * 'smart' object - where to get materials/how to make/estimated price
* time frame
  * **? weeks**
~~* budget~~
  ~~* **USD ??00 (not including material fee)**~~
* decision should be made on...
  * *objects - specification (decision on where to get materials/how to make/estimated price)*
  * *'smart' object - specification (decision on where to get materials/how to make/estimated price)*

---

* task #3 : prototype / simulation and preview in the space
  * **goal : To simulate precisely the plan so far**
* activities
  10. **build prototypes for the designs and simulate them within the space**
  11. **reflect and update according to the design review meeting with to the design**
* deliverables
  * at least 1 objects for all design
  * a preview site in the space
* time frame
  * **? weeks**
~~* budget~~
  ~~* **USD ??00 (not including material fee)**~~
* decision should be made on...
  * *at least 1 objects for all design*
  * *a preview site in the space*
  * *design confirmation demonstration*

---

* task #4 : making / fill the space as planned
  * **goal : To fill-up the space as planned**
* activities
  12. **make many pieces as needed**
* deliverables
  * a finished sites in the space
* time frame
  * **? weeks**
~~* budget~~
  ~~* **USD ??00 (not including material fee)**~~
* decision should be made on...
  * *a finished site in the space*
  * *final confirmation demonstration*
