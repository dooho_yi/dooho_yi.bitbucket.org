
######2015Mar23 14:57:22+0900

깃 세우기

chrome + sublime text + git + apache server + mdwiki + remark @ github/bitbucket pages

-

오늘 할려고 하는 것들.
mdwiki로 웹문서 만들기.
remark로 웹프리젠테이션 만들기.
-> M$ 워드랑 피피티의 웹 대체품?

-

동기부여 #1

빅데이터에 대한 거리두기.

인터넷 자체에 대한 의심스러운 부분..
shutdown : 서비스 중지
censoring : 검열

대안들

THE PIRATE NETWORK

<http://beforeitsnews.com/alternative/2013/08/could-the-pirate-network-revolutionise-wifi-and-anonymous-comunications-2731436.html>

해적 네트워크

<http://eyebeam.org/events/subnodes-workshop-with-sarah-grant>

<http://subnod.es/>

```
The Subnodes project is an open source initiative focused on streamlining the process of setting up a Raspberry Pi as a wireless access point for distributing content, media, and shared digital experiences. The device behaves as a web server, creating its own local area network, and does not connect with the internet. This is key for the sake of offering a space where people can communicate anonymously and freely, as well as maximizing the portability of the network (no dependibility on an internet connection means the device can be taken and remain active anywhere). The devices are also mesh enabled, implementing the BATMAN Advanced routing protocol.
```

<http://www.open-mesh.org/projects/open-mesh/wiki> : B.A.T.M.A.N. (better approach to mobile ad-hoc networking)

-> 로컬/일시적 네트워크 vs 글로벌/상설 네트워크

구글과 네이버가 문닫으면, 내 데이터는 다 없어짐. 데이터를 내가 가지고 있어야 함.
구글과 네이버가 맘먹으면, 내 데이터는 다 그들의 손에. 적어도 원본은 내가 가지고 있어야 함. 그리고 점차적으로 그들의 접속을 끊어낸다.

-

타협안 : 원본은 내가 가지고 웹은 씽크해서 사용.
-> 특별한 얘기는 아닌데... / 원본에 대한 주권을 지킬 수 있는 노력
-> 씽크를 하지 않아도 사용할 수 있는 로컬 웹 / ad hoc 웹

-

해결 해야 하는 문제들?

문제#1 : 웹서버를 셋업하기 어려움
-> 구글이나 네이버나 페이스북 등이 미리 꾸려놓은 플랫폼을 사용하면 해당 회사에 종속되게 됨
=> 내가 서버를 돌리면 됨. 아파치 웹서버. / openwrt

문제#2 : 웹컨텐츠를 만드는 일이 복잡하고 어려움
-> HTML/CSS/Javascript로 직접 웹사이트를 제작하는 일은 직관적이지도 않고 쉽지도 않다. 누군가에게는 이미 불가능한 일임.
-> 직관적면서 간단하게 이미지와 하이퍼링크만이라도 쉽게 쓸 수 있는 방법?
=> 마크다운?

문제#3 : WWW에 노출 시키는 방법
-> WWW은 영구적인 대안은 아니지만, 현 시점에서는 가장 우세한 네트워크
-> git을 기반으로 웹페이지를 운영할 수 있다.
=> github pages / bitbucket pages

-

문제#1

웹서버를 셋업하는 것은 그렇게 어렵지는 않다.
apache server.
로컬 웹은 언제나 손쉽게 작동
문제는 로컬 웹서버를 웹으로 노출시키는 것. -> 인터넷 공급회사나 회선 상태에 따라서, 복잡함.
일단은 로컬 웹만 되면 OK.

-

문제#2

HTML을 알고, 잘하더라도 일상적인 용어로 포스팅을 할 수 있어야 하고, 그러고 싶다.

-

**인디자인 하듯이 할 순 없을까? 그림 넣고 끌어다 놓고? 저장하면 저장되게.**

WYSIWYG = what you see is what you get.

wix & berta

<http://www.wix.com/> : 상용. 오픈소스 아님.
<http://www.berta.me/> : 상용이지만, 오픈소스.

berta의 경우엔 웹서버에 PHP 엔진이 설치되어야 함.
작성된 사이트와 코드 사이의 연관을 이해하기가 매우 어려움.

-

**복잡한 거 필요없고, 그냥 주욱 적을 수만 있으면 좋겠어.**

마크다운

<http://en.wikipedia.org/wiki/Markdown#Example>
<http://daringfireball.net/projects/markdown/>
이 페이지의 소스코드와 페이지.
-> 직관적이고 이해하기 쉬움.

대세는 마크다운(?)!

마크다운으로 웹페이지를 만들 수 있다면 좋겠다?
=> mdwiki

<http://dynalon.github.io/mdwiki/#!index.md>
<http://dynalon.github.io/mdwiki/#!quickstart.md> : 기본적인 예제
<http://dynalon.github.io/mdwiki/#!gimmicks.md> : 확장.. disqus
<https://disqus.com/> : 코멘트.

-

문제#3

연결되기.

현 시점에서는 WWW의 대안이 없다. WWW은 비교적 공공적인 공공재가 되었긴 하다.
물론 WWW를 운영하는 것은 큰 회사들과 정부들이다. 하지만, 대안이 없으니 일단 임시로 사용한다.

-

자체적으로 서버를 WWW에 연결시키기.

로컬웹을 WWW에 연결하는 방법은 오늘 다루지 않는다. 어렵지는 않지만, 아예 불가능할 수도 있다. KT인지.. SKT인지 회선 제공사의 뜻에 따라서, 연결이 허용되거나 금지되거나 할 수도 있다.
-> 필요하면 함께 연구해 볼 수도 있겠지만.. 머 굳이 안그래도 될 것 같다.

IPTIME 공유기 종류들 괜찮아보이긴 함.

<https://openwrt.org/> : openwrt : 공유기의 해킹. 집에 있는 공유기를 내 것으로 만들 수 있다. (소유하고 있다고 내것이 아닐 수도 있다.. 내가 원하는 일을 해주지 않는다면!)

-

웹사이트 전체를 (컨텐츠를 포스팅하는 종속적 구조가 아닌) 씽크 (synch) 해서, publish 하는 방법.

github pages : private 계정은 유료.
bitbucket pages : private 계정도 무료.

-

sourcetree 사용하기

-

패쓰워드 저장하기

-

텍스트 에디터

notepad
textedit
무슨 에디터 이든지 가능.
...

추천하는 거는, sublime text

-

SSH Key를 이용한 다중 계정 사용하기.

-

씽크.
synch.

