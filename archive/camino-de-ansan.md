######2015Apr23 08:11:45+0900

understanding walking mechanism

<http://www.nature.com/nature/journal/v432/n7015/images/nature03052-f1.2.jpg> : kinematics

music box

<https://www.youtube.com/watch?v=IdmW8K7qkMs> : Harry Potter Hedwig's Theme - Homemade Music Box
<https://www.youtube.com/watch?v=rvK83xglHGw> : Haddaway - What is Love? - Music box version dedicated to Abba G
<http://en.wikipedia.org/wiki/Music_box>
<http://www.instructables.com/id/Build-a-Programmable-Mechanical-Music-Box/?ALLSTEPS> : wooden DIY music box
<http://www.museumwaalsdorp.nl/computer/en/punchcards.html> : punchcard..
<http://0.tqn.com/w/experts/Clocks-Watches-1747/2008/02/backplate-striking-mechanism.jpg> : clock watch striking mechanism.
<https://www.youtube.com/watch?v=MOe5WthyTgA> : striking of bell of huge town clock.
<http://en.wikipedia.org/wiki/Striking_clock#Rack_striking> : rack striking.

-

SaDaDuino @ Seungbum Kim

<http://xenbio.net/mdwiki/#!sadaduino.md>

-

synch-patch @ donghee park / baribari lab.

<http://dh8.kr/work.html> : donghee park's site.
<https://instagram.com/p/tIoIqztsJy/> : example configuration
<http://www.doctronics.co.uk/4060.htm> : on 4060 chip.
<https://i.imgur.com/0uFMO4O.png> : synth-patch schematics

