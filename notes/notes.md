
######2015Jan24 23:21:22+0900

**pduino + GUI**

<http://hasa-labs.org/orbit_en/pduinorewrite.php>
<https://github.com/reduzent/pduino>

---

**vi / vim 에디터를 사용해볼까? / "the zone" / 제로의 영역에 들어가기 위한 노력?**

<http://www.vim.org/>
<https://www.reddit.com/r/programming/comments/2sxj1n/learn_vim_progressively/> => search for the word "zone"
<http://www.viemu.com/a-why-vi-vim.html>
<http://www.joelonsoftware.com/articles/fog0000000339.html>
```
These bouts of unproductiveness usually last for a day or two. But there have been times in my career as a developer when I went for weeks at a time without being able to get anything done. As they say, I'm not in flow. I'm not in the zone. I'm not anywhere.
```

---

**li-ion cell을 건전지마냥 갈아끼울수 있는 배터리팩...? 이거 안전한가? 효율적인가? 말이 되긴되는 건가?**

<http://www.autoboy.pe.kr/m/post/294>
<http://www.aliexpress.com/item/5V-9V-12V-Dual-USB-Power-Bank-6-x-18650-Battery-Charger-Box-For-iPhone-5S/1810670551.html>
<http://shopping.lk/index.php/5v-9v-12v-dual-usb-mobile-power-bank-6x-18650-lithium-battery-charger-box.html>
<http://www.ebay.com/itm/5V-9v-12v-Dual-USB-Power-Bank-6x18650-Battery-Charger-Box-for-iPhone-6-6-Tablet-/261622175625>
=> 만약에 이게.. 셀의 충전량을 모니터링해서 개별 충전을 할 수 있는 거라면, 관심이 간다...

---

**배터리 나간 자동차 시동 걸 수 있는 12V/300A 출력의.. 배터리팩.. (일반용으로는 못쓴다던데.. 어떻게 했는지 궁금하고, 크기작고 디자인 괜춘=> 일반용으로도 쓸 수 있게는 안되는건지?)**

<http://junopower.com/collections/android-compatible-external-batteries/products/junojumpr-junojumper-most-portable-jump-starter>
<http://www.iskyblog.co.kr/602>

---

**그밖에 여러가지 흥미로운 신개념(?) 배터리 팩들.**

<http://www.itworld.co.kr/print/88600>

---

**visualization library.. @ python..**

<http://bokeh.pydata.org/en/latest/> => 꽤 잘된 라이브러리 인것 같고.. 비쥬얼이 이뻐서.

---

**web-based SVG editor**

<http://svg-edit.googlecode.com/svn-history/r1771/trunk/editor/svg-editor.html>

---

**(STL) 벡터 그래픽인데.. 그림으로 그린 담에, 그대로 따다가 선 그림으로 따와서.. 쓸 수 있는 그런 저장 형식이 없나 조사 => STL**

<http://en.wikipedia.org/wiki/STL_(file_format)>
<http://www.ennex.com/~fabbers/StL.asp>
<http://www.ennex.com/~fabbers/StL.asp#Sct_specs>

예를 들면 이런거 아주 간단한건데..

<http://www.thingiverse.com/thing:32601/#files>

다운 받아서 보며는..
```
  facet normal 0.000000e+00 1.000000e+00 0.000000e+00
    outer loop
      vertex -1.095000e+02 -6.950000e+01 1.000000e+00
      vertex 1.095000e+02 -6.950000e+01 0.000000e+00
      vertex -1.095000e+02 -6.950000e+01 0.000000e+00
    endloop
  endfacet
```
이런게 32개로 구성되어있음. 즉, 32개의 삼각형으로 표현되어있다는 말임.. / 흠 쓸만할지 어떨지.. / 하지만, 그중 이거 만큼 간단하게 나와주는 것도 없음. => 적어도 automation은 가능할것 같음.

---

**openframeworks 로 HYU, 차선변경 과제 코드를 업데이트 할까?**

<http://answers.opencv.org/question/17277/efficient-alpha-blending/>
```
@Adi ok. Do you have to render in opencv, or could you render in opengl? I have been using alpha channels in my application, and when I was doing the rendering in opencv, I was doing so with a loop over all pixels... Now I'm rendering in opengl (openframeworks), but applying the alpha channel is not efficient either: http://answers.opencv.org/question/31136/fastest-way-to-apply-alpha-channel-to-image/ OpenCV does need some better alpha handling...
'''

역시, opencv로 그래픽스를 하려니까 답답함이 꽤 있다..

---

**opencv에서 투명도 표현 방법 => 이미지 복제를 만들고, 한쪽에만 그린후 블랜딩.**

<http://bistr-o-mathik.org/2012/06/13/simple-transparency-in-opencv/>
```
As workaround, the documentation remarks that one should paint to a separate buffer and then blend this buffer with the main image.
```

---

**tanh은 부드러운 0 ~ 1 트랜지션을 만들기에 좋은 함수!**

<http://www.j-raedler.de/2010/10/smooth-transition-between-functions-with-tanh/>
```
We define our new function s(x) = 0.5+0.5 tanh((x-a)/b). The function was shifted to return values between 0 and 1 and the parameters a and b can be used to define the switch point and the smoothing level.
```

---

**vs2010 / visual studio / CRT / memory leak detection helper.**

tricky 메모리 leak을 잡는 데 도움을 주는 M$의 헬퍼.

<https://msdn.microsoft.com/en-us/library/x98tx3cf.aspx>
<http://stackoverflow.com/a/8544704>
<http://stackoverflow.com/a/27298781>

---

**sketch-up 에서 그린 그림 / collada .dae 파일 그림을 opencv로 가져오기.**

```
            <mesh>
                <source id="ID5">
                    <float_array id="ID7" count="36">0.0000000 0.0000000 0.0000000 0.0000000 0.0688976 0.0000000 0.0000000 0.1377953 0.0000000 0.0000000 0.2480315 0.0000000 0.0349906 0.2255659 0.0000000 0.1097938 0.2255659 0.0000000 0.1690953 0.1945363 0.0000000 0.1900817 0.1377953 0.0000000 0.1900817 0.0898700 0.0000000 0.1690953 0.0365695 0.0000000 0.1097938 -0.0002358 0.0000000 0.0515771 -0.0002358 0.0000000</float_array>
                    <technique_common>
                        <accessor count="12" source="#ID7" stride="3">
                            <param name="X" type="float" />
                            <param name="Y" type="float" />
                            <param name="Z" type="float" />
                        </accessor>
                    </technique_common>
                </source>
                <vertices id="ID6">
                    <input semantic="POSITION" source="#ID5" />
                </vertices>
                <lines count="12" material="Material2">
                    <input offset="0" semantic="VERTEX" source="#ID6" />
                    <p>0 1 1 2 2 3 2 4 4 5 5 6 6 7 7 8 8 9 9 10 11 10 1 11</p>
                </lines>
            </mesh>
```

이런 간단한 선그림 (면이 없다.. 면이 있더라고 일단 유사하지 않을까?)
```
0.0000000 0.0000000 0.0000000 0.0000000 0.0688976 0.0000000 0.0000000 0.1377953 0.0000000 0.0000000 0.2480315 0.0000000 0.0349906 0.2255659 0.0000000 0.1097938 0.2255659 0.0000000 0.1690953 0.1945363 0.0000000 0.1900817 0.1377953 0.0000000 0.1900817 0.0898700 0.0000000 0.1690953 0.0365695 0.0000000 0.1097938 -0.0002358 0.0000000 0.0515771 -0.0002358 0.0000000
```
이걸 주욱 cvPoint로 로드하고.
총 12개의 vertex가 등록되어있다.
```
0 1 1 2 2 3 2 4 4 5 5 6 6 7 7 8 8 9 9 10 11 10 1 11
```
이걸 보면, 각 vertex를 어떻게 연결해야 하는지 나온다.
이것대로 연결하면 (cvLine) 됨.

######2015Jan25 01:54:02+0900

---

######2015Jan25 20:43:07+0900

**bitbucket에서도 pages/페이지를 쓸수가 있더라! / Bitbucket Pages**

<http://pages.bitbucket.org/>
<https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket> : official docs.

---

######2015Mar20 09:49:17+0900

reading materials : "critical making"

<https://s3-ap-northeast-1.amazonaws.com/apap2013-production/apap_attachments/attach_kos/92/original/%EB%A7%8C%EB%93%A4%EC%9E%90_%EB%A7%88%EB%9D%BC%ED%86%A4_%EC%9D%BD%EA%B8%B0%EA%BE%B8%EB%9F%AC%EB%AF%B8_%EB%B9%84%ED%8F%89%EC%A0%81%EB%A7%8C%EB%93%A4%EA%B8%B0.pdf>


######2015Apr13 23:39:08+0900

귄터 그라스... 타개..

<https://mirror.enha.kr/wiki/%EC%96%91%EC%B2%A0%EB%B6%81>

양철북은 그래도 읽어봐야....


######2015Apr18 13:10:25+0900

**lightroom / photo backup**

lightroom을 다시 구해서 셋업하긴 했다..
근데, 이거 이렇게 백업해놓는거 상당히 일이다.
그래서, 좀 요령이 필요할 거 같은데.

-

1. 일단, 먼저, 사진들을 모아놓는 작업이 우선이다. 윈도우에서 삭제가 가능해야 하므로, NTFS 파티션에 항상 그냥 때려 넣어 놓는 걸로 하나 장만 (지금의 까만 외장하드면 될듯) 해놓고, 그냥 사진을 다 때려넣기.

2. 단, 비디오는 따로 관리한다!! 사진만 이렇게 백업하는게 좋을듯.

3. 그런담에.. 언제 여유있을때, 자기 전에 걸어놓고 잔다..

