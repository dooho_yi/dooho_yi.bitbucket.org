
**symbolic links @ windows**

<http://www.hongkiat.com/blog/symlink-alias-folder/>
```
In Windows, a Symlink is denoted with the mklink command as follows:

mklink /d "path\destination\symlink" "path\to\source"
Launch Command Prompt, type this command below to create a Symlink in WAMPServer directory that points to the source in Dropbox.

mklink /d "c:\wamp\www\project" "c:\Users\thoriq\project"
```
=> 이걸 이용하면 아파치 서버에서 DocumentRoot 밖에 있는 폴더를 플랙서블하게 넣을 수가 있다.

---

######2015Apr25 14:03:06+0900

**vlc 미디어 플레이어에서 utf-8 한글 자막이 안나오는 문제**

사랑하는 오픈소스 vlc 미디어 플레이어에서 무슨 지랄을 해도 nanumgothic으로 한글 자막이 안나오길래.. 한시간더 용을 썼더니.. 문제 해결을 하긴했는데... 폰트를 malgun gothic으로 바꿈. / 중요한 단서는 아래 링크인데.. utf-8로 자막을 다 새로 인코딩하고 설정도 다 utf-8인데, vlc의 freetype에서 거부하고 arial.ttf 로 fallback... / 찾아봤더니, vlc에서 폰트고르는 창에는 떠도, '쓸 수 없는' 폰트가 있다는데! 이게 딱 그거였다. 근데 왜! 쓸 수 없다는 걸까. 왜 쓸 수 없는 건지 엄청 궁금한데, 아는사람!

<https://forum.videolan.org/viewtopic.php?t=97358#p323976>

맑은고딕으로 잘 나옴. ㅡㅡ.. 음 vlc로 한글 자막보는 방법을 해결한 건 좋은데 (1년이상고생했음.. 영화보러 맥으로 재부팅하던 시절.. 안녕~~)... 하고싶은데로 (나눔폰트) 못한다는데, 왜 그런지 아무도 설명을 안해주네.. 코드까지 들어가서 봐야하나. 그렇게 까지 하고 싶진 않은데 ㅡㅡ

<img src="capture_2015-04-25_13-47-54_140.png" width=500px>

vlc는 참 대단하다.. 엄청나게 큰 프로젝트.. 디버그 메세지 한번 열어보면.. 감동.

<https://forum.videolan.org/viewtopic.php?t=97476#p324507>

malgun gothic.ttf 찾기 성공. / nanumgothic.ttf 는 바로 차이고.. arial.ttf가 되버림.. ??? 왜왜~~ 머가 문젠데.

<img src="capture_2015-04-25_13-57-28_142.png" width=500px>

---

**Problem Ejecting USB Mass Storage Device "This device is currently in use. Close any programs or windows that might be using the device, and then try again."**

Q
```
Original title:Problem Ejecting USB Mass Storage Device

When I try to safely eject my external hard drive safely I get an error message with this title telling me that "This device is currently in use. Close any programs or windows that might be using the device, and then try again." I have done this. I have also tried closing every program running on the computer and it does not help. I have also tried restarting the computer and this also does nothing. If anyone has any ideas that would be helpful please let me know.
```

A
```
Hi A. Cook,
1)       How long have you been facing this issue?
2)       Is this issue confined to any specific USB device?
This behavior can occur if there are open handles with any files in the USB (Universal Serial Bus) Mass Storage Device are in use by another program.

 
Method 1:
a)       Download Process Explorer from the link below
http://technet.microsoft.com/en-us/sysinternals/bb896653
b)       Click Find, Find Handle or DLL (Dynamic Link Library) in the tool menu.
c)       Type the drive letter of the USB device in the Handle or DLL substring textbox, and press Search Button.
d)       Find the process and its PID (Process Identifier) in the following box.
e)       In system process tree view, find the process according to the Find Handle or DLL dialog box.
f)        Press Ctrl + H to show Handles in Lower Pane View.
g)       Find the File according with the drive letter, and right click it, choose Close Handle.
 
Reference:

Problem Ejecting USB Mass Storage Device
http://support.microsoft.com/kb/555665
```

``` 
Method 2:  You may refer the steps provided by Diana D in the link below 
http://answers.microsoft.com/en-us/windows/forum/windows_vista-hardware/problem-ejecting-usb-mass-storage-deviceits-in-use/fd56805c-ed8e-491f-8a2b-39ed1c988a71 
 
Method 3: Run an online scan for any threat and try to fix them
http://onecare.live.com/site/en-Us/center/cleanup.htm
```

from M$.. forum..

---

######2015Apr27 09:33:50+0900

sublime text 2 - theming

<http://sublimetext.userecho.com/topic/19274-theming-of-the-sidebar/>
<http://stackoverflow.com/questions/13580561/sublime-text-2-change-side-bar-color/13584477#13584477>
<http://sublimetext.userecho.com/topic/19274-theming-of-the-sidebar/>
<http://williamwalker.me/blog/sublime-text-2-custom-sidebar-theme.html> : sidebar color theme
<https://github.com/buymeasoda/soda-theme> : well made theme example
<http://stackoverflow.com/questions/19544176/sublime-text-theme-change-the-background-of-the-search-bar>

sublime text 2 - shortcut - hide/show menu bar => just 'alt'!

<http://sublimetext.userecho.com/topic/48628-hideshow-menu-bar-shortcut/>

---

######2015Jun07 00:07:15+0900

**외장하드가 분리(Safely removed)가 안되는 win7 문제와 해결방법**

<http://answers.microsoft.com/en-us/windows/forum/windows_7-hardware/cant-eject-usb-hard-drive-drive-always-in-use/f052d0e7-ee89-4946-a6ad-b6e632a65133>
```
The CORRECT answer is that the "Distributed Link Tracking Client" has gone nuts and is groveling the entire hard drive for who knows what reason. In my case, the drive hadn't even been used in this session of Windows. After stopping the service, I was able to eject the hard drive. For this sort of problem, you can get more information about which service svchost.exe is hosting is behind the activity by looking at the stack for a relevant item in the Sysinternals Process Monitor list. I found trkwks.dll in the stack, which is "Distributed Link Tracking Client", which maintains the "tracking.log" you mentioned. As you discovered, the Windows Event Log is no help at all, which unfortunately is very typical.
```

Distributed Link Tracking Client 라는 서비스를 중지시킨다.

<http://superuser.com/questions/491539/how-to-safely-remove-a-device-blocked-by-the-system-process-with-a-handle-on-e>

<http://superuser.com/questions/510968/external-hdd-is-always-in-use-when-trying-to-safely-remove> : 백업 중단.

이건 뭐 거의 버그에 가까운 문제 인거 같다. 인지하고 있으면서도 고쳐지지 않는 고질적인 병신 M$의 문제. (정신병)

<https://social.technet.microsoft.com/Forums/windowsserver/en-US/fb2b8b33-cea7-42e8-b264-09d8c5eab30f/failed-server-backup-dextendrmmetadatatxflogtxflogblf?forum=winserveressentials> : M$ 엔지니어들도 이걸 해결책으로 추천하고 있다.

---

######2015Jun07 00:12:11+0900

**X11 forwarding @ windows**

<http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html> : putty를 다운 받는다.

Xming을 깐다. 별도 ssh client는 깔지 않는다. -> <http://people.arsc.edu/~murakami/xming/>

localhost:0 라고 Connection-X11-X display location에 적는다. -> <https://wiki.utdallas.edu/wiki/display/FAQ/X11+Forwarding+using+Xming+and+PuTTY> : 이 tutorial대로 하니까 됨. 다른 데서는 localhost:0:0 이라고 한 경우도 있는데 그렇게 하면, 안된다.

<http://www.straightrunning.com/XmingNotes/> : 뭔가 심오한 제대로된 매뉴얼.

---

**How to Change the IP Range for the Internet Connection Sharing DHCP service**

ICS를 이용해서, windows 7 에서 인터넷 쉐어링할때, ip range 설정법.

<https://support.microsoft.com/en-us/kb/230148>
```
HKLM\System\CurrentControlSet\services\SharedAccess\Parameters
(REG_SZ) ScopeAddress
(REG_SZ) StandaloneDhcpAddress
```
이 두개만 손보면 된다.

---

######2015Jun20 10:17:29+0900

**odroid/ubuntu/linux - cannot login after invoking init cmd. (probably as a root user..)**

the .Xauthority file's owner & grp modified to be 'root' from 'odroid'..
we need to redo this by
sth. like... "chown odroid:odroid .Xauthority"

<http://superuser.com/questions/576950/ubuntu-can-not-login-after-init>
```
sudo chown -R $USER:$USER $HOME (this may take time)
```
<http://ubuntuforums.org/showthread.php?t=2120307>

---

**office powerpoint tip - theme color**

[microsoft office - How to change settings in a PowerPoint theme? - Super User](http://superuser.com/questions/755479/how-to-change-settings-in-a-powerpoint-theme)
