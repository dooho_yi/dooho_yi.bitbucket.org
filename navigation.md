# D

[todo](todo.md)

[ongoing]()

  * # active
  - - - -
  * [manual-cafe](ongoing/manual-cafe.md)
  * [harrypotter-graffiti-zine](ongoing/harrypotter-graffiti-zine/harrypotter-graffiti-zine.md)
  * [cradl-assemply-manual](ongoing/cradl-assemply-manual/cradl-assemply-manual.md)
  - - - -
  * # opportunity
  - - - -
  * [24-240](ongoing/24-240/24-240.md)
  - - - -
  * # new/concept
  - - - -
  * [material-shop](ongoing/material-shop/material-shop.md)
  - - - -
  * # interesting
  - - - -
  * [ipython](ongoing/ipython.md)
  * [learning-linux-rpi](ongoing/learning-linux-rpi/learning-linux-rpi.md)
  * [rpi-oscilloscope](ongoing/rpi-oscilloscope/rpi-oscilloscope.md)
  * [real-time-linux-kernel](ongoing/real-time-linux-kernel/real-time-linux-kernel.md)

[seco-hyu]()

  * # survival/living : seco-hyu-3rd-year
  - - - -
  * [2015-05-001](ongoing/seco-hyu-3rd-year/seco-hyu-3rd-year-2015-05-001.md)
  * [2015-06-001](ongoing/seco-hyu-3rd-year/seco-hyu-3rd-year-2015-06-001.md)
  * [2015-06-002](ongoing/seco-hyu-3rd-year/seco-hyu-3rd-year-2015-06-002.md)
  * [2015-06-003](ongoing/seco-hyu-3rd-year/seco-hyu-3rd-year-2015-06-003.md)
  * [2015-06-004](ongoing/seco-hyu-3rd-year/seco-hyu-3rd-year-2015-06-004.md)
  * [2015-07-001](ongoing/seco-hyu-3rd-year/seco-hyu-3rd-year-2015-07-001.md)

[archive]()

  * # archive
  - - - -
  * [camino-de-ansan](archive/camino-de-ansan.md)
  * [victoria-and-albert](archive/victoria-and-albert/victoria-and-albert.md)
  - - - -
  * [git-slides-20150324-notes](archive/git-slides-20150324/)
  - - - -
  * [seco-slides-20150130](archive/seco-slides-20150130/slides)

[\*_\*]()

  * # pins
  - - - -
  * [any](pins/pins.md)
  - - - -
  * [anime](pins/anime.md)
  * [songs](pins/songs.md)
  * [recipe](pins/recipe.md)
  * [shopping-list](pins/shopping-list.md)
  - - - -
  * # tips
  - - - -
  * [tips](tips/tips.md)
  - - - -
  * # notes
  - - - -
  * [any](notes/notes.md)
  - - - -
  * [diary](notes/diary.md)
  