######2015Aug28 06:52:08+0900

blackmarket

-

아이디어들..

-

원정

스마트폰 피리 앱 등등..

두호

이것저것..

-

뭔가 결정하고 정해서, 만들어야 함.
시간이 없어서.. 시간투자 대비 아웃풋 쩌는 걸로 골라야 할것 같음..

-

뭐 하노.?

---

**xxx for dummies**

배포하는 거 생각해 봤는데.. 좀 덜 재밌는 거 같긴하다. 물론 나는 재밌지만,

-

이게 재밌는 이유랄까? 그건..
사실 라이센스. 그런게 아니라.. dummies 시리즈 자체가 주는 재미가 쏠쏠한데..

dumb 귀머거리 / 멍청이, dummies.. 멍청이들..

책을 사기 위해서 멍청이임을 인정해야 하는 상황이다. ㅎ 근데 이런 책이 마니 생산되고 나오고 있다는 점.

계급적이다. 정보의 계급. 아는 것에 대한 숭배.

아는 사람들은 귀족과 같다.

자 그런 느낌으로. 이 시리즈를 보면..

사람들이 무엇을 가치롭다고. 또 신분상승에 필요한 지식이라고 여기는지.. 가 보여서 재미있다.

누군가는 부동산 투기하는 법.
누군가는 슬퍼하는 법.
누군가는 뜨개질.
누군가는 다이어트.
누군가는 인생상담.
누군가는 집에서 사업하는 법.

이런것을 얘기하고 있는데..

재미있었다..

-

또 하나는.. 이 영상을 올린 사람의 사고 방식인데..

읽을 수 없을 것 같이 빠르게 지나가는 영상은 마치.. 책이라기보다는 읽을 수 없어서.. 너무 빨리지나가니까.

그냥 정보의 덩어리 혹은 폭포와 같다.

이런 경우는 마치 저작권이 부여된 저작물이 아니라. 또 다른 저작의 소재일 뿐인것 같다는 인상.

youtube에 올리는 자료는 자동으로 CC 라이센스를 받는다고 한다.

즉 하나의 저작물이 매체를 통과하면서 저작권을 상실하게 될 수 있는 상황.

물론 업로더에게 책임을 주고 제제 혹은 징계를 가하겠지만.

인터넷. 유투브. 라는 매체의 압도적인 존재는 그런 작은 이벤트를 간과하거나 무시할 수 있는 가능성이되기도 한다.

즉, 이제는 저작물이 좀 오남용 공유가 되도 그냥 간단히 이메일 받고 징계가 되고.. 그것도 신고가 들어왔을 때만..

youtube의 cc 라이센스 정책이라던가.. 근간을 흔들어 버리는 접근은 누구도 하지 않는다. 할 수가 없고 할라고 해도 작동이 안되고.

즉, youtube는 저작권을 수도 없이 훼손하니까. 문을 닫아야 한다고 누구도 말을 못하고 있다는 것이지!!!

이게 얼마나 모순적이란 말인가 ㅎㅎ 개인유저들만 징계를 받고.. 원인제공자인 youtube는 발뺌이라니.

-

여튼 이런 상황에서.. 이런 영상을 올린 사고방식은 .. 마치..

책의 정보를 읽을 수 없는 방식으로 packing / translate 해서 (빠르게 지나가는 영상) youtube에 올렸는데..

솔직히 유투브의 interface에서는 읽을 수가 없다. frame단위로 볼수가 없으니까.

이건 나에게는 마치 도전과 같은 것이었다.

분명히 정보가 다 들어있을 것이다. 그런게 읽을 수가 없다.

다운로드를 받아서. (이것 자체가 해킹이고..)

그 상태로 vlc 플레이어 등을 이용해서, frame 단위로 넘기면서 읽을 수도 있지만..

그 영상을 프레임단위로 추출해서. (ffmpeg 같은 툴을 사용한다. 요것은 좀더 고급 해킹이고..)

추출된 이미지를 다 모아서 pdf를 만든다음에 책으로 인쇄해서 제본해서 책을 만든다.

영상 --> 책. translation.

-

이와 같은 일련의 과정을.. 업로더가 가정하고 있는 것처럼 보이는 상황.

뭔가 비밀스럽게 이런 과정을 셋업하고 또래 네트워크에서 방법론을 공유하면서, 정보를 spread 하기 위해서 youtube 채널을 사용한 것 처럼 보이는 상황이 포착된다.

그건 마치.. 기술을 가진 사람들을 향한 offer.

그런데 그 책의 내용이란게..

멍청이들을 위한 책들. 이라는 점.

이 아이러니가 아주 재밌게 다가오는 것이다.

-

매체적 암호화 이기도 하고.. 영상 <-> 책의 번역관계는...

그 암호를 다시 풀기도 하고.. encode/decode 의 관계가 성립되어 있으면서.

그 도구에 접근할 수 있는 사람들에게 open network

암호화를 통한 vpn? / freenet?

그런 개념들에 뭔가 primitive 하게 닿아있다.

열려있으면서 닫혀있는 공동체

overlayed community

뭔가 흐트러져서 sparsely,

그러나 강하게 연결되어있으면서.

군중속에 뭍혀있으나.

어떤 trigger를 통해서 연결되어있고.

유연하게 작동하는 sub community

공존하면서도 따로 존재하고

인증이 필요하지만 열려있는 그런 공동체의 인상이 재밌다.

---

그래서.. 

음 이거 xxx for dummies를 이대로 뭐 대충 나눠주고 하는거는 그리 재밌을지 잘 모르겠고..

뭔가 background idea나 intuition에 insight에 대해서 좀더 생각해봐야 할 거 같다.

이야기를 좀더 전개해야 재미있을 내용이지.. 지금은 그냥 나만 재밌지.

정말 관심사가 비슷한 사람들 몇명이랑..

---

######2015Aug28 07:16:35+0900

매체를 통한 암호화라는 거는 재미있다.

이를 테면 black market에서 유투브 링크 뒤에 번호랑 책의 제목. 그리고 framerate랑..(정확한 conversion을 위해서 필요하다) 번역 툴을 나눠주고 다운로드 받게 해서 CC 라이센스를 우회하는 형상을 취하는 것은 재밌을 것 같다..
framerate를 정말 빠르게 해서.. 거의 TV 처럼 만들어버리는 거지.

이런 방식은 마치 예전에 binary data를 (modem 시절에) tape 등에 recording 하던 시절을 연상 시킨다. 정보가 소리가 되면 재미있지..

승범씨가 했던 이미지를 소리로 전송한다는 거나.. 비슷..

이렇게 소리로 변환된/암호화된 데이터는 soundcloud 등을 이용해서 공유가 가능하다.

해당 메체가 가정하는 contents의 대상이 아닌 어떤 것을 갑작스럽게 제시하면서 그것을 왜곡 시키는 것이다.

이런 일련의 가능성을 여러가지로 정리해서.. key를 공유하는 사람들 만의 공유 네트워크를 만들어..

행위자네크워크의 군림하는 권력자들을 당황시킨다는 것이 시나리오의 기본이다.

---

######2015Aug28 07:32:58+0900

**rain transfer**

비를 송수신한다.

송신기와 수신기.

안테나의 형상.

인터폰-폰+넷 = 인터넷.

물질의 전송.

-

일단 수신기를 만들어볼까.

유량 측정은 어떻게 하면 좋을까.

-

*tipping bucket*

비의 양을 측정하는 방법은 이렇게 한다. ㅎㅎ 이미 다 잘 나와있네.

<https://en.wikipedia.org/wiki/Rain_gauge#Tipping_bucket_rain_gauge>

tipping bucket rain gauge 라고 하는 것인데.. 지난 번에 경기도 어린이 박물관에가서 봤던 시소를 이용한 구조물.

물을 좌우로 똑딱 나눠 흘리는 그 구조물을 이용한다. 한번 넘어갈때 얼마만큼의 양이 소모되는지가 정해져 있으니까 가능하다.

<http://www.instructables.com/id/Arduino-Weather-Station-Part3-Rain/?ALLSTEPS>

arduino로 diy한 경우도 이미있다.

대충만들고.. 측정을 통해서 calibration만 잘해주면 정확도는 나올꺼 같다.. 

중요한건 구조물의 지속성일듯.

-

여튼 이렇게 하면 만들수가 있다. 그리고 물은 그대로 전송이된다.

그럼 전송은 어떻게 할까?

flow를 면적으로 펼치려면?

