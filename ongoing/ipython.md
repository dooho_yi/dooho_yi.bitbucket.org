
######2015Apr27 09:17:28+0900

ipython learning.

<http://sebastianraschka.com/Articles/2014_matlab_vs_numpy.html> : array routines.
<http://localhost:8888/tree#notebooks> : the notebook

######2015May20 19:55:07+0900

<http://nbviewer.ipython.org/github/htapia/scientific-python-lectures/blob/master/Lecture-4-Matplotlib.ipynb> : matplotlib
<http://nbviewer.ipython.org/github/adrn/ipython/blob/rel-1.0.0/examples/tests/pylab-switch/Pylab%20Switching.ipynb> : **pylab switch!!**
<http://nbviewer.ipython.org/github/adrn/ipython-notebooks/blob/master/jake%20vdp%20ipython%20notebooks/notebooks/01_basic_plotting.v2.ipynb> : **%pylab magic command**
<http://stackoverflow.com/questions/20961287/what-is-pylab/20961407#20961407> : pylab magic command

very nice learning starting point for ipython
<https://wakari.io/gallery> : wakari gallery
<https://www.wakari.io/nb/aron/Accelerating_Python_Libraries_with_Numba___Part_1> : numba (a just-in-time specializing compiler) - speed up python a lot!

