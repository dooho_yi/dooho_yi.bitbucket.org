######2015Jun20 04:07:14+0900

**ssh X forwarding emacs issue**

이렇게 하면 된다는 건데..

<https://wiki.utdallas.edu/wiki/display/FAQ/X11+Forwarding+using+Xming+and+PuTTY>

되긴 되는데.. 첨에 한~참을 기달려야 한다. 에러메세지가 6번 나오고나면 X display가 연결됨..

이런 문제를 겪는 사람이 없지 않다.

<http://askubuntu.com/questions/135573/gconf-error-no-d-bus-daemon-running-how-to-reinstall-or-fix>
<http://stackoverflow.com/questions/22415126/emacs-hangs-when-run-through-sshing-to-my-ubuntu-box-13-10>
```
(emacs:3306): GConf-WARNING **: Client failed to connect to the D-BUS daemon:
//bin/dbus-launch terminated abnormally without any error message
```

음, 이제 보니까.. 같은 문제 인줄 알았는데.. 메세지가 다르다..
내꺼는..

```
(emacs:5987): GConf-WARNING **: Client failed to connect to the D-BUS daemon:
Failed to connect to socket /tmp/dbus-fE1ayqSy14: Connection refused
```

이런식으로.. "Connection refused" 의 경우네...
아마도 Xauth..  관련해서 문제가 있나본데.. 해결 방법이 있을 것도 같다.
단지.. 지금은 이거에 계속 신경 쓸 수가 없으니깐.. 지금은 그냥 넘어가자..

---

**learning emacs basics with tutorial**

기본적인 사용법을 배웠다. 따라하면.. 꽤 배운다.

그리고나서 코딩을 시작했더니.. auto indentation이 안되서.. 좀 찾아봤다.

<https://www.cs.bu.edu/teaching/tool/emacs/programming/>
```
Automatic Indentation

Another feature you can use when Emacs is in C++ mode is automatic indentation. You can use this before or after the fact. In other words, you can press the <Tab> key before you type a line of code for it to indent to the proper place. Or, you can press the <Tab> key while on a line, to fix its indentation after the fact.
```

아무거나 쓰고.. tab을 누르면, 정렬해서 들어간단다.. **tab** 만 누르면 코드가 bracelet에 맞춰 정렬된다.. 되드라. 좋다.

-

이 튜토리얼이 잘 된건지는 아리송하지만.. 읽을 꺼리가 좀 있다. 굿.

---

**org mode**

<http://orgmode.org/>

노트 / 할일 / 등등..

check this out.
<https://www.youtube.com/watch?v=Q8AqHdZTgNI>

-

**swapping windows(buffer) in emacs needs extension**

<http://www.johndcook.com/blog/2012/03/07/shuffling-emacs-windows/>
<http://www.emacswiki.org/emacs/buffer-move.el>

---

