
####subject => 목표 설정 및 보드 선정. odroid or rpi or 르네사스?

---

######2015May20 20:11:50+0900

3차년도 준비.

차선변경과제 HVI Embedded화하기.

회의내용
```
2개의 프로젝트가 있음.
1 - 차선변경 과제
2 - 만도 과제
 
요구사항
1 - 영상 4채널 / 캔 4채널 / 데이터 로깅 / 실시간 디스플레이
2 - 영상 1/2채널+확장성 / 캔 2채널+확장성 / 데이터 로깅 / 오프라인 디스플레이
 
손차장님 제안.
 
과제1을 위한 솔루션
 
한양대 김회율교수 랩(영상처리랩)에서 2차년도 사용한 르네사스7766 보드 (AVM용으로 사용했음)를 가져다가 작업하는 법
 
르네사스 보드
- OS가 없다. - 해당 랩에서 자체 구현한 펌웨어 소스코드 사용가능
- NTSC 4채널 프레임그래빙. 구현및테스트됨
- CAN 하드웨어 지원되나, 펌웨어 미흡. -> 개발.
- HDD 인터페이스 없다. 개발 필요. -> 하드디스크 드라이버를 짜는 건.. 못할 일인듯.... 기존에 나온 보드를 이용할 수 있도록 해야 함 / 타임스탬프만 찍어주면, LAN으로 받아서 로깅하는 방향도 생각해 볼 수 있음.
- 영상 출력 NTSC 만 지원. 운전자용 HMI 경우에는 NTSC로 하고 LAN으로 개발자용 화면(1280x800)을 뿌리는 방법 (딜레이가 있으면 감수)
- 기타 이슈 -> 컴퓨터로 시리얼 통신을 걸어서 해당 보드를 activate 시켜주지 않으면 사용할 수 없다는 (?말도 안되는) 이슈가 있는데, 해결해야만 standalone으로 사용가능함
 
=> 이런저런 문제와 에러 및 미흡한 점들을 해결하면서 기 설계된 르네사스 평가보드를 활용하는 법.
 
질문 1 : 르네사스 7766 같은 보드에서 OS가 없다고 하는데, 말이 안되는 거 아닌가? 한양대 랩에서 구매당시엔 없었더라도 차기버젼에서 있을 것이다.
대답 1 : (르네사스 측 컨택후 답변) 새로나온 보드는 linux 등이 지원되는데, 뭔가 계약을 체결해야 준다.. 기존의 보드는 공식적으로 지원되지 않지만 일본의 어느 3rd party에서 제작한 것이 있으며, 구매해야 한다. / 기타 상세 내용은 르네사스에 직접 연락을 취할 것 -> 연락처를 손차장님이 주시기로 함.
 
질문 2 : 르네사스 7766 EVM 같은 기존의 보드가 사용가능하다면, Raspberry Pi 2 나 Ordroid XU3 같이 시중에 나와있는 원보드 PC들(linux나 android가 이미 적용되어있고, 여러 유저들이 사용하여 검증된 보드들)도 사용가능하다는 말인가? RPi 2에는 CSI 인터페이스로 HD 30fps 영상을 처리할 수 있는 하드웨어가 있으며, Ordroid XU3는 A15 + A7 의 2개의 쿼드코어를 탑재하고 있으며 가격이 179달러 / RPi 2 는 35달러. 르네사스 같은 폐쇄적이고 가격 비싼 보드에 의지할 필요가 없지 않나? (EVM 보드는 기본 수수백만원 예상..) 자체적으로 PC 급 보드를 제작하는 일은 주어진 기간내에 불가능하다.
대답 1 : 알았다. 문제 없다. 그런 경우도 Embedded 화한 경우로 인정한다.
 
결론 : 르네사스 / RPi / Ordroid 등을 검토하고 1주일 가량 조사 기간을 가진후 결정함. / 르네사스 7766 보드의 펌웨어 코드와 연락처를 손차장님이 주기로 함.
 
-
 
과제2를 위한 솔루션
 
현재 르네사스 7766 보드의 OS가 얻어지고 이를 통해 과제1이 성공적으로 커버되는 경우에는 과제2는 과제1의 부분집합이므로 하나의 시스템을 두가지 과제를 해결가능하다.
 
만일 OS문제가 해결되지 않고 HDD로깅을 위한 HDD 인터페이스(하드웨어+펌웨어) 가 준비되지 못하는 경우에는 기존의 세코인터페이스의 GMT3 보드를 변용하는 방법
 
GMT3
- 영상 1채널 VGA 급 multiplexing 2채널
- 로깅 가능?
- CAN 1채널. 확장성?
 
질문 1 : 해당 보드를 모듈화하는 방안. 1영상+1CAN 을 처리하는 모듈 여러개 + 동기화 clock 발생기로 모듈화하여 구현하는 방법은 어떤가?
대답 1 : 좋은 생각이다.
 
사장님 : 그러나, 2가지 솔루션을 가지고, 각각을 별도로 개발하는 방법은 심각하게 재검토가 필요할 것이다.
 
-
 
약 1주일 후, 기술적 / 인력적 / 가격+수요 등을 종합적으로 검토한 방안 1,2,3.. 을 준비한 후에 만도 측과 재협상한다.
손차장님 : 방안 마련 여부와 상관없이 일단 만나서 이야기 하자.
 
이상.
 
-
 
너무 빨리 얘기해서.. 생각나는대로 적었습니다.
대부분의 내용은 들어간 것 같은데요.
 
저는 일단 시중에 나온 보드를 사용하는 데에 한표를 던지고 싶네요..
rpi 2의 경우는 1GHz 쿼드 코어에 MIPI CSI 카메라를 통한 (USB 2.0카메라는 밴드위쓰가 안나오기 때문에) HD영상 30fps 1채널을 받아들일수 있는 보드&카메라 솔루션이 나와있습니다.
ordroid 경우에는 훨씬 고성능의 프로세서가 탑재되어있는데, MIPI 카메라가 obsolete product로 분류되어있습니다. 더이상 생산하지 않는 것인지... 알수가 없네요.
르네사스 보드의 경우에는 사용자 층이 두텁지 않아, 개발해야 할 부분이 너무 많을까 우려됩니다.
세가지 보드 중에서 라즈베리파이가 가장 사용자 층이 두터워 많은 부분이 기 개발되어있을 것으로 예상됩니다.
여러가지 불명확한 이슈가 있어서.. 아직 속단하긴 이릅니다만..
 
관심 가지고 계셔주시면 감사하겠습니다.
오동희대리님, 전영우과장님께는 계속해서 카톡으로 연락하겠습니다.
 
그럼, 이만,
 
이두호 드림.
```

---

######2015May21 00:02:37+0900

<http://www.hardkernel.com/main/products/prdt_info.php?g_code=G140448267127> : odroid xu3 (one of most powerful SBCs for now)
<http://www.hardkernel.com/main/products/prdt_info.php?g_code=G141351880955> : odroid xu3 lite (moderate version of original xu3 and significantly cheaper!)
<http://www.hardkernel.com/main/products/prdt_info.php?g_code=G133999328931> : odroid x which was used in the DEMO for 4 ch. USB WEBCAM example.
<https://www.youtube.com/watch?v=H2DRkLFsqQ4> : WEBCAM 4ch. example.

OTHER boards?
<http://www.linux.com/news/galleries/top-10-open-source-linux-boards-under-200/cosmic-65> : top 10 open source SBCs for linux.
<http://www.intorobotics.com/compare-10-linux-single-board-computers-to-find-the-right-one-for-you/> : another top 10
<http://iqjar.com/jar/an-overview-and-comparison-of-todays-single-board-micro-computers/> : overview...
<http://en.wikipedia.org/wiki/Comparison_of_single-board_computers> : comparison chart..
=> odroid series is quite high-ranked..
<http://linuxgizmos.com/ringing-in-2015-with-40-linux-friendly-hacker-sbcs/> : list of 40.

---

######2015May21 00:17:01+0900

raspberry pi 2

how fast it is?
<http://haydenjames.io/raspberry-pi-2-overclock/> : same but quad. (or little bit faster?) : but anyway odroid is significantly better.

peripherals
CAN
<http://elinux.org/RPi_CANBus> : a wiki page doing CAN bus @ rpi. => MCP2515
<https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=7027&start=500> : anyway for timestamps.. YOU need to do that.
=> even though rpi support MCP2515 by community directly.. but actually probabily we need to do post-process the CAN bus into another I/F baking timestamps too. so then.. whatever! just arduino-like.. for example arduino micro style USB embedded ECU can be used => but for this.. we have OBD style module @ SECO already develped and easy to modify.. is it good enough?

HDD (SSD)
<http://www.reddit.com/r/raspberry_pi/comments/2ms53n/can_i_use_ssd_drives_with_the_rpi_b/>
=> just use a SSD => USB converter.. (if you are using odroid xu3 which supports usb3.0, get 1 sata-usb3.0 converter to get full speed.)

btw, what is usb OTG (on-the-go) => it means odroid is the host of USB... well still not very clear though.. <http://en.wikipedia.org/wiki/USB_On-The-Go>

==> after talking with Mr. Oh.. one thing very clear is more than 90 percent of cases, we need to have 1 single board to do all the jobs, grabbing 4ch. SD video + capturing 4ch. CAN + data processing + visualizing processing + 1 ch. HD video output. Otherwise, we won't be able to eliminate the latency problem. We want to get < 0.1 sec at most.

the consideration effort for rpi 2, actually assuming that we are able to do distributed processing by collaboration of multiple rpi boards.. but then, we cannot synchronize easily.. the system becomes enormously complex to do this if they are all distributed... ?? => for example, we can do logging in distributed sense. No prob. But for composing/visualizing the output screen and streaming that in real-time, we again gather all data into, anyway, 1 SBC. This means distributing effort becomes really an overhead. if you cannot do that in 1 SBC, then forget about it. go for a PC.

---

######2015May26 01:27:49+0900

odroid

확장보드 (SPI)
<http://www.hardkernel.com/main/products/prdt_info.php?g_code=G138232136481>
<http://www.hardkernel.com/main/products/prdt_info.php?g_code=G141351880955> : odroid xu3 lite
<http://www.samsung.com/global/business/semiconductor/minisite/Exynos/w/solution.html#?v=octa_5422> : processor exynos 5422
<https://www.raspberrypi.org/forums/viewtopic.php?t=24660> : csi-2 / mipi / lvds
<http://com.odroid.com/sigong/nf_file_board/nfile_board.php> : odroid downloads
<http://forum.odroid.com/viewtopic.php?f=63&t=4760> : "Is the "ODROID USB-CAM 720P" Hardkernel sold camera compatible with other than Android? Linux. Mac, Windows?"
<http://com.odroid.com/sigong/blog/blog_list.php?bid=&tag=ODROID-X&page=3&page=4> : odroid xu3 는 odroid xu 계열인가? -> 프로세서가 바뀌면서 넘버링이 함께 바뀌는 것 같다. / xu는 5420 / xu2는 5421 / xu3 는 5422 .. 즉, xu용 이미지는 xu3에서 작동 안할 가능성이 있음.
<http://en.wikipedia.org/wiki/Exynos> : 여기 뒤에 나오는 표 참고.
<http://www.samsung.com/global/business/semiconductor/minisite/Exynos/w/solution.html#?v=octa_5422>
<http://www.samsung.com/global/business/semiconductor/product/application/detail?productId=7978&iaId=2341>
<http://archlinuxarm.org/packages> : odroid에서 돌아가는 archlinux도 있다.

exynos octa 에서 csi-2를 지원하는가? => 칩 자체는 지원한다. xu3는 지원한다는 말이 없다. 일부러 안열어준건가? 의심되는 부분.
<http://electronicdesign.com/communications/understanding-mipi-alliance-interface-specifications>

odroid 이미지/소스들이 있는 곳. (including debian)
<https://github.com/tobiasjakobi/linux-odroid>
<http://odroid.us/odroid/odroidxu/debian/>
<http://odroid.us/mediawiki/index.php?title=Debian_Wheezy_Instructions>
<http://odroid.us/mediawiki/index.php?title=HardwareAndDrivers>
<http://odroid.us/mediawiki/index.php?title=ODROID_Devices> : 5422은 아직 등록이 안되어있다.

odroid & xenomai
<https://www.google.co.kr/search?q=odroid%20xenomai&es_th=1&rct=j>

linux kernel source
<http://www.tldp.org/LDP/tlk/sources/sources.html> : 커널 소스 보는 법.. / arch에서 odroid-xu3를 찾을수가 없었다. 버젼 몇 부터 이게 지원이되는 건지 알수가 없다.
<https://code.google.com/p/odroid-wheezy-retro/downloads/list> : debian wheezy / 분명히 되는 이미지들은 돌아다니는데.. 내가 만일 컴파일을 하고 싶으면 어떤 소스를 가져다가 해야 하는 걸까?
<https://code.google.com/p/odroid-wheezy-retro/wiki/Index> : odroid-wheezy-retro는 프로젝트도 있는데.. 뭐 직접관련은 없지만..
<http://com.odroid.com/sigong/nf_file_board/nfile_board_view.php?keyword=&tag=&bid=235> : 공식적으로는 우분투를 지원한다. / lubuntu도 자동으로 지원이 되고.. ubuntu는 debian의 동생 뻘되는데.. 둘은 커널을 공유할 수는 없다.. 왜 그런지? 여러가지 이야기가 있음. --> <http://ubuntuforums.org/showthread.php?t=1752741> : 이런 질문..
<http://dn.odroid.com/5422/ODROID-XU3/Ubuntu/> : lubuntu image for xu3
<http://com.odroid.com/sigong/nf_file_board/nfile_board_view.php?keyword&tag&bid=241> : 14.04 lts (long time support version)
<http://www.cnx-software.com/2014/12/14/odroid-xu3-lite-board-ubuntu-review-setup-usability-and-performance/> : tutorial for xu3 "Setting Up Ubuntu on ODROID-XU3 Lite" + alpha.
<http://odroid.in/ubuntu_14.04lts/> : xu3 lubuntu 14.04 lts
lubuntu의 정체는? -> ubuntu의 최소 설치 버젼이랑 같다고 보면 되나? 아마도.. 하지만 증거는 못찾았다.
<https://help.ubuntu.com/community/Installation/MinimalCD>
<http://lubuntu.net/>
kernel compiling linux
<http://odroid.us/mediawiki/index.php?title=Step-by-step_Native_Compiling_a_Kernel> : a tut
ubuntu에서 말하는 debian... <https://wiki.ubuntu.com/Debian>
lubuntu는 lxde를 사용한다. <http://askubuntu.com/questions/384546/lubuntu-13-10-minimal-install-shows-no-desktop>
<https://help.ubuntu.com/community/Lubuntu> : ubuntu에서 말하는 lubuntu
<http://ubuntuforums.org/showthread.php?t=1929708> : ubuntu와 debian.. 다른 이야기.
<http://www.researchgate.net/post/What_are_the_major_disadvantages_of_using_Lubuntu_1404_instead_of_Ubuntu_1404_if_any2> : lubuntu와 그냥 ubuntu의 차이점...

일단, 나름의 결론은 lubuntu를 써서.. 하는게 debian으로 하는 것보다 좋은 점은 때때로, debian에서 지원하지 않는 드라이버가 ubuntu에 지원이 되는 경우가 있다는 것인데.. 이 경우 debian에서 ubuntu 드라이버의 바이너리*말고* 소스를 가져다가 컴파일을 시도하는 것도 방법중 하나. 이런게 성공/실패 가능성이 있는데.. lubuntu에서하면 그럴 문제가 없다. 그냥 된다.
그러나 사람에 따라서는 나도 공감하고.. ubuntu보다 debian이 시스템이 훨씬 잘 organize되어있고, 안정적인 느낌이 있다는 것.
그래서 자꾸 debian 노래를 부르게 됨.

일단 보드 받으면, lubuntu로 진행하고 안되면 공식 ubuntu로, 되면 debian으로 진행을 노려본다. 셋중에서 되는 방향으로 가야 할 것임. 어차피.

---

**연구노트를 써야 한다. 먼저 양식부터 만들고..**
