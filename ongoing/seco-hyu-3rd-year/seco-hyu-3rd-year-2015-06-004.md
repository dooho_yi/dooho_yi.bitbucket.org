
####subject => ordoid에서 USB CAM 4채널 재생의 성능 보기 : TRY-OUT ASAP => pthread + V4L2 (MJPG) + OpenCV + jpg decoding (libjpeg)

---

######2015Jun29 17:34:55+0900

```
**목표는 MJPG으로 카메라에서 받아서 (이건 이미 성공을 했다.. v4l2로.. 짰잖아.) 안정적으로 화면에 표시하는 것인데.**
이것도 opencv에 있는 cvDecodeImage를 썼더니.. 잘 안되거나..
그냥 jpg 파일로 저장을 시켜서.. 이미지뷰어로 열어보니.. 됐다 안됐다 하길래..
opencv에 정떨어져서.. 버릴라고 했는데..
일단 libjpeg을 하거나. libjpeg-turbo (ipp나 SIMD 지원을 좀 받으면서..) 를 써서..
이걸 잘 convert하기만 하면 되는 것이 아닐까.
**4채널을 올려서. 그려주기만 하면된다.**
문제는.. *어떻게 그릴까* 였다.
그려주는 것은 일단 opencv를 그대로 이용한다.
```

지난 노트의 결론.

---

######2015Jun29 18:14:56+0900

이에 따라서, 지금 빨리 해내야 할 일은...

* libjpeg으로 기존의 v4l로 넘어온 이미지를 변환해서 opencv에서 읽어서, 화면에 뿌린다. (2hr.)
* 이게 되면 쓰레드기반으로 돌리고. 이게 되면, 4채널 쓰레드를 짜본다. 2채널 부터 짜보고 들어가도되고... (2hr.)

저녁 11시에는 OK?

---

######2015Jun29 18:17:02+0900

아.... 진짜. 잘 안되넹.

지금 상태는..mpjg로 받아서 v4l2convert로 bgr24로 변환해서. opencv로 화면에 그려준다.
boost thread로 threading을 했다.

-

여하튼, mpjg으로 받는데도.. VIDIOC_STREAMON: No space left on device 에러가 나고.
uvcvideo quirks=128로 드라이버를 다시 로드했는데도 문제는 그대로이다.

-

mjpg으로 했는데로 안되는 건, 참내.. 뭔지 모르겠다.
되야 하는 것이 안되는 데 정말 짜증.

-

지금 v4lconvert가 fd에 연결되어있으니.. 정확하게 어떤일이 진행되는지를 모르겠다.. 얘가 뭐낙 장치 메모리를 먹나?
포럼 쓰레드에 따르면, 장치에 공간이 부족하다는 말의 '장치'는 카메라를 말하는게 아니라 USB bandwidth를 말하는 것이라고 한다.
v4lconvert를 그냥 강제로 제거해봤는데.. 그냥 캡쳐쓰레드만 돌리는데 2개 돌리면 segmentation fault가 나거나 돌아가더라도 나머지 한개는 VIDIOC_STREAMON 에서 exit 해버린다.

-

v4lconvert를 아예 안쓰고, tinyjpeg을 바로 연결해서 써보는 수가 있다. 이렇게 되면 확실하게.. 확인이 될 거 같기도 하다.
그렇지만, 지금 v4lconvert를 제외해도 동시에 여러대 카메라가 안되는 걸 보면, 그렇게 해도 나아질 것 같지는 않기도 하고.
지금까지 결과들 중에 가장 좋았던거는 opencv의 vcap으로 여러카메라를 사용하는 경우인데.. 해상도를 320 240으로 한 상태에서 3대까지 작동이 됐다.

이 때만해도, 분명히 bandwidth가 손에 잡히는 어떤 것이었다.

-

다음으로는 뭘 해볼 수 있을까.. 시간은 점점 없어져간다.

-

opencv vcap이 어떻게 하는지 코드를 보고, v4l 부분을 참고해서 뭐가 다른지 왜 그 쪽에선 되는지.. 보는 방법이 하나..
그렇게 해서 custom opencv vcap을.. 강제로 mjpg을 사용하게 하거나.. icv 함수에서 작동을 시키거나해서.. format not supported 문제를 우회해보는 방법이랑.

원래 예제가 안드로이드에 있으니까.. 그걸 보고.. ndk 코드등을 참고해서.. 어떻게 하는지 확인하고 그걸 참고해보는 방법이 있겠다.

---

######2015Jul04 01:40:23+0900

지금 상태를 좀 다시 정리해보자면.
어느정도 문제가 조금 파악된 것이.. opencv vcap에서 했던 코드에서 320 240으로 3대까지 작동이 됐던 것이 가장 괜찮은 결과라고 했는데..

그건 uvcvideo quirks=128 해킹이 yuyv에 대해서만 적용이 되고, mjpg을 했을 때는 적용이 안된다는 점에서 해명이 된다.

yuyv일때는, bandwidth를 장치의 report에 의존하지 않고, 직접계산하기 때문에.. 위에서 말했듯이 "이 때만해도, 분명히 bandwidth가 손에 잡히는 어떤 것이었다." 라는 것이 가능한 것이고, 자연스러운 결과이다.

opencv에서는 mjpg 설정을 제대로 할 수가 없었다. 이건 아마 opencv의 버그겠지. v4l2 라이브러리에서 적용을 했을 때는, 적용이 되지만, 역으로 quirks=128을 적용할 수가 없게 되기 때문에 bandwidth문제가 해결이 안된다. 따라서, mjpg으로 했을 때는 카메라 2대도 제대로 안된다는 것이다.

즉, odroid 포럼에서 quirks=128을 사용해서 카메라 2대를 했다는 것은 틀린 말은 아닐 수 있지만(yuyv에서 했겠지.), 4대를 하기 위해서는 mjpg 상태에서 quirks=128이 적용이 되야 한다는 말이고. 그건 불가능하기 때문에 더 나은 결과를 얻을 수가 없다는 얘기가 된다.

동시에.. uvcvideo 사이트에 가보면 이슈 13번이 발생하는 카메라들이 아직도 있는 것으로 나타나고 있으며 M$의 lifecam 시리즈 중 스튜디오/시네마 등이 이에 속한다.

이런 여러가지 복합적인 문제...

자 여기서 2가지 선택의 길이 있게 되는데.. 카메라를 바꿔야 하는 거냐. 드라이버를 고치거나/바꿔야 하는 거냐.. 하는 것이다.

일단 uvcvideo 드라이버의 버그가 있었는데.. 그 버그는 패치가 나와있고. linux 3.9 이후에는 적용이 되었을 가능성이 있다. 확실하지는 않지만. 만일 안되엇다면, 패치를 적용할 수도? 있을지도 모른다.. 안될 가능성이 높지만...

일단, 뭔가 base가 되는 fact를 찾아야 하는 상황.

1. yuyv에서 카메라 4대는 불가능하다. quirks=128 상태에서 320 240 영상 3대까지. 640 480은 아마도 영상 1대뿐이 작동이 안될 것이다. 즉, 우리는 mjpg으로 반드시 가야 한다.
2. mjpg은 v4l2 / uvcvideo 드라이버가 지원을 해줘야 한다. quirks=128 없이. 정상적으로 지원이 되야만한다. 만일 지금의 드라이버가 이를 지원할 수 없다면 (버그 때문에) 대안은 없다. 드라이버가 바뀌거나 패치 되어야 한다.
3. 드라이버를 아예 고쳐써서. 문제 상황을 회피하게 한다고 하면 좋겠지만(그러니깐, mjpg이든 yuyv 이든 bandwidth 검사를 아예 안하고 그냥 작동하면 되잖아.), 그저 최신의 드라이버로 업데이트 하는게 최선이라면.. quirks=128은 못쓰게 되고.. 정상적으로 지원이 되는 카메라를 찾아내야 한다. 예를 들면, odroid에서 보여준 4채널 영상 예제에서 사용된 로지텍 C270의 경우에는 정상지원이 되는 카메라로 되어있다. 하지만 해당 카메라는 너무 bulky 하니까.. 좀더 작고 소형인 카메라를 원하게 되는데.. 그럴 경우에는 해당카메라가 원하는 기능이 정상지원이 되는지 어떤지를 일일이 검사해야 한다.

결국 uvcvideo 드라이버를 최신으로 업데이트를 하고. 지금의 이름 없는 이 카메라가 새로운 드라이버에서 정상 작동을 하는지 어떤지 봐야 한다. android에서 4채널 영상이 된다고 하니까. 그 쪽의 드라이버를 가져다가 써본다거나.. debian 이미지 등 다른 well-made 이미지를 써서 해보거나. 근데 dmesg를 찍어보면 항상 카메라가 연결될때 samsung의 drm이 들어오는데.. direct rendering 기능이 개입하는 걸 보게 된다. 이건 uvcvideo에 포함이 되는 걸까 아닐까. 어떤 패치가 필요한거라면, 이부분도 살려야할것 같다. 만일 그렇다고 하면 .. 참 어려운 일이다. 불가능.

여튼 되는데 까지 해봐야 하니까.. uvcvideo 드라이버를 업뎃/교체 하고 지금의 카메라를 mjpg 상태에서 quirks=128 없이 작동되는지 본다. 된다면 ok. 안된다면 카메라도 바꿔본다.

이게 수순일 것이다.

현재 uvcvideo 드라이버는 버젼이 1.1.1이다..
```
modinfo uvcvideo
```
라고 하면 그렇게 나온다.

현재의 최신 드라이버의 버젼은...


---

######2015Jul04 01:40:18+0900

아카이브..

#####TEMP

[libjpeg-turbo8 - Google Search](https://www.google.com/search?q=libjpeg+tutorial&oq=libjpeg+tut&aqs=chrome.1.69i57j0l5.5831j0j7&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=libjpeg-turbo8)
[libjpeg-turbo | Documentation / Documentation](http://www.libjpeg-turbo.org/Documentation/Documentation)
[c++ - Need help in reading JPEG file using libjpeg - Stack Overflow](http://stackoverflow.com/questions/5616216/need-help-in-reading-jpeg-file-using-libjpeg)
[VAT's Blog: An updated guide to get hardware-accelerated OpenCV on BeagleBone Black](http://vuanhtung.blogspot.kr/2014/04/and-updated-guide-to-get-hardware.html)
[Image compression with libjpeg. « AaronMR](http://www.aaronmr.com/en/2010/03/test/)
[jpeglib.h - Google Search](https://www.google.co.kr/search?q=jpeglib.h&oq=jpeglib.h&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[libjpeg download | SourceForge.net](http://sourceforge.net/projects/libjpeg/?source=typ_redirect)
[libjpeg/example.c at master · LuaDist/libjpeg](https://github.com/LuaDist/libjpeg/blob/master/example.c)
[c - libjpeg: process all scanlines once - Stack Overflow](http://stackoverflow.com/questions/13773644/libjpeg-process-all-scanlines-once)
[boost libjpeg - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=boost%20libjpeg&es_th=1)
[v4l libjpeg - Google Search](https://www.google.co.kr/search?q=v4l+libjpeg&oq=v4l+libjpeg&aqs=chrome..69i57j0.3775j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[V4l-utils - LinuxTVWiki](http://linuxtv.org/wiki/index.php/V4l-utils)
[odroid-x usb webcam project source - Google Search](https://www.google.co.kr/search?q=v4l-utils&oq=v4l-utils&aqs=chrome..69i57j69i60l2&sourceid=chrome&es_sm=122&ie=UTF-8#q=odroid-x+usb+webcam+project+source&newwindow=1&start=10)
[ODROID Forum • View topic - Reading MJPEG stream from UVC camera](http://forum.odroid.com/viewtopic.php?f=112&t=9017)
[Odroid U2/U3 multithreaded camera code - Chief Delphi](http://www.chiefdelphi.com/forums/showthread.php?t=131144)
[USB Webcam](http://com.odroid.com/sigong/prev_forum/t1208-usb-webcam.html)
[v4l-utils.git - media (V4L2, DVB and IR) applications and libraries](http://git.linuxtv.org/cgit.cgi/v4l-utils.git/tree/README)
[v4lconvert jpeg - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=v4lconvert+jpeg)
[v4l-utils/jpeg.c at master · gjasny/v4l-utils](https://github.com/gjasny/v4l-utils/blob/master/lib/libv4lconvert/jpeg.c)
[v4l library - decoding of Pixart JPEG frames](http://video4linux-list.redhat.narkive.com/sgSnS5cE/v4l-library-decoding-of-pixart-jpeg-frames)
[Chapter 6. Libv4l Userspace Library](http://linuxtv.org/downloads/v4l-dvb-apis/libv4l.html)
[libv4lconvert opengl - Google Search](https://www.google.co.kr/search?q=libv4lconvert&oq=libv4lconvert&aqs=chrome..69i57j69i60l2&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=libv4lconvert+opengl)
[PAC7302/CameraCalibration.cpp at master · mp3guy/PAC7302](https://github.com/mp3guy/PAC7302/blob/master/src/CameraCalibration.cpp)
[libv4l/libv4lconvert.h at master · philips/libv4l](https://github.com/philips/libv4l/blob/master/include/libv4lconvert.h)
[v4lconvert_convert - Google Search](https://www.google.co.kr/search?q=v4lconvert_convert&oq=v4lconvert_convert&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[Raspberry Pi Lab](https://www.pitchforth.com/Raspi/)
[https://www.pitchforth.com/Raspi/v4l2grab.c](https://www.pitchforth.com/Raspi/v4l2grab.c)
[Re: qv4l2-bug / libv4lconvert API issue -- Linux media](http://www.spinics.net/lists/linux-media/msg54206.html)
[Raspberry Pi Lab](https://www.pitchforth.com/Raspi/index.html)
[Appendix E. Video Grabber example using libv4l](http://linuxtv.org/downloads/v4l-dvb-apis/v4l2grab-example.html)

##### TEMP2

[mp3guy/PAC7302](https://github.com/mp3guy/PAC7302)
[PAC7302/V4LCam.cpp at master · mp3guy/PAC7302](https://github.com/mp3guy/PAC7302/blob/master/src/V4LCam.cpp)
[qimage bits - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=qimage%20bits&es_th=1)
[QImage Class | Qt 4.8](http://doc.qt.io/qt-4.8/qimage.html#bits)
[IplImage - Google Search](https://www.google.co.kr/search?q=cvCreateImage&oq=cvCreateImage&aqs=chrome..69i57j0l5.288j0j7&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=IplImage)
[Basic C Structures and Operations — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/core/doc/old_basic_structures.html)
[OpenCV: IplImage Struct Reference](http://docs.opencv.org/master/d6/d5b/structIplImage.html#ab6315f84a34002b616a187f87999f167)
[memcpy - C++ Reference](http://www.cplusplus.com/reference/cstring/memcpy/)
[libv4lconvert.c - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=libv4lconvert.c&es_th=1)
[philips/libv4l](https://github.com/philips/libv4l)
[SYS_IOCTL - Google Search](https://www.google.co.kr/search?q=SYS_IOCTL&oq=SYS_IOCTL&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#q=SYS_IOCTL&newwindow=1&start=10)
[ioctl(2): control device - Linux man page](http://linux.die.net/man/2/ioctl)
[Linux kernel - LXR/Linux Cross Reference](http://lxr.oss.org.cn/ident?i=sys_ioctl)
[order of execution if - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=order+of+execution+if)
[if statement - cppreference.com](http://en.cppreference.com/w/cpp/language/if)
[c - Order of execution in if statement - Stack Overflow](http://stackoverflow.com/questions/15775373/order-of-execution-in-if-statement)
[c - if with multiple conditions, order of execution - Stack Overflow](http://stackoverflow.com/questions/2456086/if-with-multiple-conditions-order-of-execution)
[Short-circuit evaluation - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Short-circuit_evaluation)
[static_cast fpermissive - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=static_cast%20fpermissive&es_th=1)
[c++ - error: cast from 'Foo*' to 'unsigned int' loses precision - Stack Overflow](http://stackoverflow.com/questions/3898158/error-cast-from-foo-to-unsigned-int-loses-precision)
[C++ style cast from unsigned char to const char - Stack Overflow](http://stackoverflow.com/questions/658913/c-style-cast-from-unsigned-char-to-const-char)
[what is the meaning of "broken pipe" - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=what%20is%20the%20meaning%20of%20%22broken%20pipe%22&es_th=1)
[android - What, exactly, does a "broken pipe" exception mean to the Socket? - Stack Overflow](http://stackoverflow.com/questions/10899391/what-exactly-does-a-broken-pipe-exception-mean-to-the-socket)
[User Interface — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/highgui/doc/user_interface.html)
[Basic Structures — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/core/doc/basic_structures.html)
[cvcreateImage global - Google Search](https://www.google.co.kr/search?q=cvcreateImage+global&oq=cvcreateImage+global&aqs=chrome..69i57.4928j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[IplImage as Global Variable doesn't work](http://opencv.yahoogroups.narkive.com/8IKynIPO/iplimage-as-global-variable-doesn-t-work)
[gcc - cmake and threads - Stack Overflow](http://stackoverflow.com/questions/5395309/cmake-and-threads)
[VIDIOC_DQBUF resource temporarily unavailable - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=VIDIOC_DQBUF+resource+temporarily+unavailable)
["pthread" v4l2 capture -ffmpeg - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=%22pthread%22+v4l2+capture+-ffmpeg)
[Paparazzi UAV](http://docs.paparazziuav.org/)
[Overview - PaparazziUAV](http://wiki.paparazziuav.org/wiki/Overview)
[Paparazzi UAS: sw/airborne/modules/computer_vision/viewvideo.h File Reference](http://docs.paparazziuav.org/latest/viewvideo_8h.html)
[Paparazzi UAS: sw/airborne/modules/computer_vision/lib/v4l/v4l2.c Source File](http://docs.paparazziuav.org/latest/v4l2_8c_source.html)

##### TEMP3

[Overview - PaparazziUAV](http://wiki.paparazziuav.org/wiki/Overview)
[Multithreading in C++0x part 2: Starting Threads with Function Objects and Arguments | Just Software Solutions - Custom Software Development](https://www.justsoftwaresolutions.co.uk/threading/multithreading-in-c++0x-part-2-function-objects-and-arguments.html)
[Threading with Boost - Part I: Creating Threads](http://antonym.org/2009/05/threading-with-boost---part-i-creating-threads.html)
[How to get started using Boost threads - CodeProject](http://www.codeproject.com/Articles/279053/How-to-get-started-using-Boost-threads)
[gavinb / boost_samples / Downloads — Bitbucket](https://bitbucket.org/gavinb/boost_samples/downloads)
[gavinb / boost_samples — Bitbucket](https://bitbucket.org/gavinb/boost_samples)
[c++ - static const Member Value vs. Member enum : Which Method is Better & Why? - Stack Overflow](http://stackoverflow.com/questions/204983/static-const-member-value-vs-member-enum-which-method-is-better-why)
[Reading and Writing Images and Video — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html)
[Basic C Structures and Operations — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/core/doc/old_basic_structures.html#IplImagecvCloneImage(const IplImageimage))
[Geometric Image Transformations — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/imgproc/doc/geometric_transformations.html)
[webcam capture odroid-x android example - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=webcam+capture+odroid-x+android+example)
[High-Definition Games on ODROID-X](http://com.odroid.com/sigong/blog/blog_list.php?bid=&tag=ODROID-X&page=3&page=4)
[H2DRkLFsqQ4 - Google Search](https://www.google.co.kr/search?q=H2DRkLFsqQ4&oq=H2DRkLFsqQ4&aqs=chrome..69i57.575j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[ODROID Forum • View topic - ODROID-X : 4 x USB cameras](http://forum.odroid.com/viewtopic.php?f=14&t=1703)
[Logitech C270 가격 - Google Search](https://www.google.co.kr/search?q=Logitech+C270&oq=Logitech+C270&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=Logitech+C270+%EA%B0%80%EA%B2%A9)
[로지텍 HD WebCam C270 종합정보 행복쇼핑의 시작 ! 다나와 (가격비교) - Danawa.com](http://prod.danawa.com/info/?pcode=1227296&cate1=863&cate2=892&cate3=10683&cate4=0)
[HD Webcam C270](http://www.logitech.com/en-hk/product/hd-webcam-c270)
[neuralassembly / SimpleDoubleWebCams2 / source / src / com / camera / simpledoublewebcams2 / CameraPreview.java — Bitbucket](https://bitbucket.org/neuralassembly/simpledoublewebcams2/src/3ba4b383f53338e161d36ceb007f5789662be35c/src/com/camera/simpledoublewebcams2/CameraPreview.java?at=master)
[OpenGL ES 2.0 Programming on ARM Linux X11](http://com.odroid.com/sigong/blog/blog_list.php?bid=148)

##### TEMP4

[Android vs Linux As An Embedded Operating System > Hughes Systique Corp. > Blog](http://hsc.com/Blog/Android-vs-Linux-As-An-Embedded-Operating-System-1)
[Intro to Embedded Linux Part 1: Defining Android vs. Embedded Linux | Linux.com](https://www.linux.com/news/featured-blogs/200-libby-clark/707796-defining-android-vs-embedded-linux)
[Teensy Audio Library, high quality sound processing in Arduino sketches on Teensy 3.1](http://www.pjrc.com/teensy/td_libs_Audio.html)
[TYZX - Google Search](https://www.google.co.kr/search?q=TYZX&oq=TYZX&aqs=chrome..69i57j0l5.143j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)
[About TYZX](http://www.tyzx.com/about/index.html)
[Streaming Point Grey Cameras on Embedded Systems Point Grey USB 3.0, Gigabit Ethernet and FireWire Machine Vision Cameras](https://www.ptgrey.com/tan/10699)
[NVIDIA Jetson TK1 - Google Search](https://www.google.co.kr/search?q=NVIDIA+Jetson+TK1&oq=NVIDIA+Jetson+TK1&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[gumstix 4 usb camera - Google Search](https://www.google.co.kr/search?q=gumstix&oq=gumstix&aqs=chrome..69i57j69i65j0l4.1360j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=gumstix+4+usb+camera)
[Home page](https://store.gumstix.com/)
[5MP HD camera board for Gumstix® Overo® COMs](http://www.e-consystems.com/5MP-Gumstix-Camera.asp)
[Gumstix - Would any normal USB camera work with Overo/Tobi board combination?](http://gumstix.8.x6.nabble.com/Would-any-normal-USB-camera-work-with-Overo-Tobi-board-combination-td3673979.html)
[Tobi/FireStorm - Google Search](https://www.google.co.kr/search?q=Tobi%2FFireStorm&oq=Tobi%2FFireStorm&aqs=chrome..69i57j69i58&sourceid=chrome&es_sm=122&ie=UTF-8)
[Comparison of single-board computers - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Comparison_of_single-board_computers)
[Comparison of single-board computers - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Comparison_of_single-board_computers)
[camera addon odroid - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=camera+addon+odroid)
[ODROID | Hardkernel](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G134111627588)
[BeagleBoard.org - beagleboard-xm](http://beagleboard.org/beagleboard-xm)
[Beagleboard:BeagleBone Capes - eLinux.org](http://elinux.org/Beagleboard:BeagleBone_Capes)
[USB HUB Cape - eLinux.org](http://elinux.org/USB_HUB_Cape)
[BeagleBoard.org - project](http://beagleboard.org/project)
[Beagle Bone black stereo vision application - Google Groups](https://groups.google.com/forum/#!topic/beagleboard/yK74G1UzDLk)
[Fundamental Guide for Stereo Vision Cameras in Robotics - Tutorials and Resources | Into Robotics](http://www.intorobotics.com/fundamental-guide-for-stereo-vision-cameras-in-robotics-tutorials-and-resources/)
[Gumstix - Flashing Overo FireStorm and Tobi](http://gumstix.8.x6.nabble.com/Flashing-Overo-FireStorm-and-Tobi-td4966133.html)
[Gumstix - Best way to power Overo FireStorm+Tobi without plugging in? (USB to 5V cable?)](http://gumstix.8.x6.nabble.com/Best-way-to-power-Overo-FireStorm-Tobi-without-plugging-in-USB-to-5V-cable-td4969339.html)
[Gumstix - USB Webcam Support](http://gumstix.8.x6.nabble.com/USB-Webcam-Support-td4966715.html)
[odroid xu3 - Google Search](https://www.google.co.kr/search?q=odroid+xu3&oq=odroid+xu3&aqs=chrome..69i57j69i65l3j69i60j0.3822j0j9&sourceid=chrome&es_sm=122&ie=UTF-8)
[ODROID | Hardkernel](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G140448267127&tab_idx=2)
[USB 2.0 and how to convert to USB 3.0 [Solved] - USB3 - Storage](http://www.tomshardware.com/answers/id-1763474/usb-convert-usb.html)
[Audio System Design Tool for Teensy Audio Library](http://www.pjrc.com/teensy/gui/)
[https://www.kernel.org/doc/Documentation/devicetree/bindings/media/s5p-mfc.txt](https://www.kernel.org/doc/Documentation/devicetree/bindings/media/s5p-mfc.txt)

##### TEMP5

[gumstix - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=gumstix&es_th=1)
[Gumstix, Inc.](https://www.gumstix.com/)
[Meet Geppetto | Gumstix, Inc.](https://www.gumstix.com/geppetto/)
[camera latency local lan - Google Search](https://www.google.co.kr/search?q=ethernet+camera&oq=ethernet+camera&aqs=chrome.0.0l6.2446j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=camera+latency+local+lan)
[Gigabit Ethernet Cameras - GigE Cameras | Edmund Optics](http://www.edmundoptics.com/cameras/gigabit-ethernet-cameras/)
[Further IP camera latency tests, various modes - YouTube](https://www.youtube.com/watch?v=ztrNLbWYXtw)
[Understanding - and Reducing - Latency in Video Compression Systems](http://www.design-reuse.com/articles/33005/understanding-latency-in-video-compression-systems.html)
[Local Wi-Fi Mode / Local Storage / (no internet connectivity required) : Dropcam Support](http://support.dropcam.com/entries/21816601-Local-Wi-Fi-Mode-Local-Storage-no-internet-connectivity-required-)
[stream mjpg rpi latency ethernet - Google Search](https://www.google.co.kr/search?q=stream+mjpg+rpi&oq=stream+mjpg+rpi&aqs=chrome..69i57.15512j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=stream+mjpg+rpi+latency+ethernet)
[real time - Raspberry Pi no delay (<10ms) video stream - Stack Overflow](http://stackoverflow.com/questions/16750395/raspberry-pi-no-delay-10ms-video-stream)
[Streaming Your Webcam w/ Raspberry Pi | Wolf Paulus](https://wolfpaulus.com/jounal/embedded/raspberrypi_webcam/)
[Raspberry Pi • View topic - Low latency wifi stream possible?](https://www.raspberrypi.org/forums/viewtopic.php?f=43&t=45793)
[Raspberry Pi • View topic - Foundation Camera Board information](https://www.raspberrypi.org/forums/viewtopic.php?f=43&t=32605&start=150#p336453)
[100t network lag latency rpi - Google Search](https://www.google.co.kr/search?q=100T+internal+network&oq=100T+internal+network&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=100t+network+lag+latency+rpi)
[camera - What streaming solution for the Picam has the smallest lag? - Raspberry Pi Stack Exchange](http://raspberrypi.stackexchange.com/questions/13382/what-streaming-solution-for-the-picam-has-the-smallest-lag)
[Anton's Mindstorms Hacks: Realtime video stream with a Raspberry Pi and PiCam](http://antonsmindstorms.blogspot.nl/2014/12/realtime-video-stream-with-raspberry-pi.html)
[raspberry pi camera multiple - Google Search](https://www.google.co.kr/search?q=raspberry+pi+camera&oq=raspberry+pi+camera&aqs=chrome..69i57j69i60j0l4.3726j0j9&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=raspberry+pi+camera+multiple)
[multiplex csi pi - Google Search](https://www.google.co.kr/search?q=multiplex+sci&oq=multiplex+sci&aqs=chrome..69i57j0l5.5142j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=multiplex+csi+pi)
[Raspberry Pi • View topic - Multiplexing CSI for two cameras](https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=91548&p=655502)
[compute module - Google Search](https://www.google.co.kr/search?q=compute+module&oq=compute+module&aqs=chrome..69i57j69i65l2j69i61l3&sourceid=chrome&es_sm=122&ie=UTF-8)
[rpi compute-module adafruit - Google Search](https://www.google.co.kr/search?q=rpi+compute-module+adafruit&oq=rpi+compute-module+adafruit&aqs=chrome..69i57.5080j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[Raspberry Pi Compute Module ID: 2231 - $39.95 : Adafruit Industries, Unique & fun DIY electronics and kits](http://www.adafruit.com/products/2231)
[Compute Module IO Board Hardware Design Files Now Available! #piday #raspberrypi @Raspberry_Pi « Adafruit Industries – Makers, hackers, artists, designers and engineers!](https://blog.adafruit.com/2014/10/17/compute-module-io-board-hardware-design-files-now-available-piday-raspberrypi-raspberry_pi/)
[Compute Module IO rpi rs-online - Google Search](https://www.google.co.kr/search?q=Compute+Module+IO&oq=Compute+Module+IO&aqs=chrome..69i57j69i60&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=Compute+Module+IO+rpi+rs-online)
[BREAKING NEWS! Raspberry Pi Compute Module – Pi on a DIMM! @Raspberry_Pi #raspberrypi « Adafruit Industries – Makers, hackers, artists, designers and engineers!](https://blog.adafruit.com/2014/04/07/breaking-news-raspberry-pi-compute-module-pi-on-a-dimm-raspberry_pi-raspberrypi/)
[Compute Module Kit | Raspberry Pi Compute Module Kit | Raspberry Pi](http://kr.rs-online.com/web/p/processor-microcontroller-development-kits/8134164/)
[docs-asia.electrocomponents.com/webdocs/12e1/0900766b812e183a.pdf](http://docs-asia.electrocomponents.com/webdocs/12e1/0900766b812e183a.pdf)
[Raspberry Pi display - Google Search](https://www.google.co.kr/search?q=Raspberry+Pi+display&oq=Raspberry+Pi+display&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[Raspberry Pi A+ | Raspberry Pi A+ | Raspberry Pi](http://kr.rs-online.com/web/p/processor-microcontroller-development-kits/8332699/)
[Linux man page - nc.traditional](http://www.man-online.org/page/1-nc.traditional/)
[What are the diffrences between netcat-traditional and netcat-openbsd? - Ask Ubuntu](http://askubuntu.com/questions/346869/what-are-the-diffrences-between-netcat-traditional-and-netcat-openbsd)
[How to switch to netcat-traditional in Ubuntu? - Stack Overflow](http://stackoverflow.com/questions/10065993/how-to-switch-to-netcat-traditional-in-ubuntu)

##### TEMP6

[Raspberry Pi • View topic - Multiplexing CSI for two cameras](https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=91548&p=655502)
[Raspberry Pi • View topic - Stereoscopic camera capture - now implemented.](https://www.raspberrypi.org/forums/viewtopic.php?f=43&t=85012)
[compute module 2 pi raspberry rs-online - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=compute+module+2+pi+raspberry+rs-online&newwindow=1&start=10)
[RPi HardwareHistory - eLinux.org](http://elinux.org/RPi_HardwareHistory)
[CSI multiple linux sbc - Google Search](https://www.google.co.kr/search?q=Raspberry+Pi+Compute+Module&oq=Raspberry+Pi+Compute+Module&aqs=chrome..69i57j69i60j69i65j69i61&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=CSI+multiple+linux+sbc)
[Designing Raspberry Pi Compute Module with Ethernet capability - Raspberry Pi Stack Exchange](http://raspberrypi.stackexchange.com/questions/26061/designing-raspberry-pi-compute-module-with-ethernet-capability)
[Inforce 6410Plus Qualcomm Single Board Computer Adds GPS, MIPI CSI and DSI Interfaces](http://www.cnx-software.com/2015/06/13/inforce-6410plus-qualcomm-development-board-adds-gps-mipi-csi-and-dsi-connectors/)
[beaglebone camera cape stereo - Google Search](https://www.google.co.kr/search?q=beaglebone+camera+cape&oq=beaglebone+camera+cape&aqs=chrome..69i57j0l5.9334j0j9&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=beaglebone+camera+cape+stereo)
[BeagleBone Black gains 720p camera cape ·  LinuxGizmos.com](http://linuxgizmos.com/beaglebone-black-gains-720p-camera-cape/)
[Beagleboard:BeagleBone HD Camera Cape - eLinux.org](http://elinux.org/Beagleboard:BeagleBone_HD_Camera_Cape)
[Beaglebone Black Camera Cape -Introduction - YouTube](https://www.youtube.com/watch?v=U-cDFQhI2Gg)
[Single Board Computers - All Products](http://www.inforcecomputing.com/products/single-board-computers)
[Snapdragon 600 Processor Specs and Details | Qualcomm](https://www.qualcomm.com/products/snapdragon/processors/600)
[Raspberry Pi Compute Module Dev Kit - IO Board ... | element14](http://www.element14.com/community/docs/DOC-67392/l/raspberry-pi-compute-module-dev-kit-io-board-schematic?ICID=picompute-module-schematic)
[Raspberry Pi Compute Module Dev Kit - IO Board Schematic.pdf](file:///C:/Users/doohoyi/Downloads/Raspberry%20Pi%20Compute%20Module%20Dev%20Kit%20-%20IO%20Board%20Schematic.pdf)
[Raspberry Pi Compute Module ID: 2231 - $39.95 : Adafruit Industries, Unique & fun DIY electronics and kits](http://www.adafruit.com/products/2231)
[Raspberry Pi Compute Module ID: 2231 - $39.95 : Adafruit Industries, Unique & fun DIY electronics and kits](http://www.adafruit.com/products/2231)
[Camera board extension - Raspberry Pi](https://www.raspberrypi.org/camera-board-extension/)
[Pi Camera HDMI Cable Extension from freto on Tindie](https://www.tindie.com/products/freto/pi-camera-hdmi-cable-extension/)
[Petit Studio](http://petitstudio.blogspot.jp/)
[list sbc linux wikipedia comparison - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=list+sbc+linux+wikipedia+comparison)
[Comparison of single-board computers - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Comparison_of_single-board_computers)
[HummingBoard - Google Search](https://www.google.co.kr/search?q=HummingBoard&oq=HummingBoard&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[Hummingboard -Linux Single Board Computer | Specifications](https://www.solid-run.com/products/hummingboard/linux-sbc-specifications/)
[MicroSOM i1 - SolidRun - Wiki](http://wiki.solid-run.com/index.php?title=MicroSOM_i1)
[CSI "2 lane" wikipedia - Google Search](https://www.google.co.kr/search?q=2+Lane+MIPI+CSI&oq=2+Lane+MIPI+CSI&aqs=chrome..69i57j69i64&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=CSI+%222+lane%22+wikipedia)
[Camera Interface Specifications | MIPI Alliance](http://mipi.org/specifications/camera-interface)
[MIPI Alliance Specification for Camera Serial Interface 2 (CSI-2)_百度文库](http://wenku.baidu.com/view/f870b96f561252d380eb6e9b.html)
[SolidRun Community - Index page](http://forum.solid-run.com/)
[HummingBoard is a Raspberry Pi rival that lets you swap out its processor](http://www.engadget.com/2014/07/03/hummingboard-solid-run/)
[Orange PI - Google Search](https://www.google.co.kr/search?q=Orange+PI&oq=Orange+PI&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[Ringing in 2015 with 40 Linux-friendly hacker SBCs ·  LinuxGizmos.com](http://linuxgizmos.com/ringing-in-2015-with-40-linux-friendly-hacker-sbcs/)

##### TEMP7

[Raspberry Pi • View topic - Multiplexing CSI for two cameras](https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=91548&p=655502)
[MIPI Alliance Specification for Camera Serial Interface 2 (CSI-2)_百度文库](http://wenku.baidu.com/view/f870b96f561252d380eb6e9b.html)
[Comparison of single-board computers - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Comparison_of_single-board_computers)
["VIDIOC_G_COMP" ioctl - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=%22VIDIOC_G_COMP%22+ioctl)
[LinuxTV GIT repositories](http://git.linuxtv.org/)
[ioctl VIDIOC_DECODER_CMD, VIDIOC_TRY_DECODER_CMD](http://linuxtv.org/downloads/v4l-dvb-apis/vidioc-decoder-cmd.html)
[ioctl VIDIOC_QUERYCAP](http://linuxtv.org/downloads/v4l-dvb-apis/vidioc-querycap.html)
[ioctl VIDIOC_G_JPEGCOMP, VIDIOC_S_JPEGCOMP](http://linuxtv.org/downloads/v4l-dvb-apis/vidioc-g-jpegcomp.html)
[guvcview source - Google Search](https://www.google.co.kr/search?q=guvcview+source&oq=guvcview+source&aqs=chrome..69i57j0l4j5.5018j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[guvcview / Git-Master / Commit [3a534b]](http://sourceforge.net/p/guvcview/git-master/ci/3a534b51d45dcd7175646dde869c786bc6d4f954/)
[udev_new - Google Search](https://www.google.co.kr/search?q=udev_new&oq=udev_new&aqs=chrome..69i57j0l5.320j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)
[udev](https://www.kernel.org/pub/linux/utils/kernel/hotplug/libudev/libudev-udev.html#udev-new)
[bind c99 boost - Google Search](https://www.google.co.kr/search?q=g_new0&oq=g_new0&aqs=chrome..69i57j0l5.552j0j7&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=bind+c99+boost)
[Memory Allocation: GLib Reference Manual](https://developer.gnome.org/glib/stable/glib-Memory-Allocation.html#g-new0)
[Boost: bind.hpp documentation - 1.55.0](http://www.boost.org/doc/libs/1_55_0/libs/bind/bind.html#with_function_objects)
[guvcview command line - Google Search](https://www.google.co.kr/search?q=%22Init.+USB+2.0%22&oq=%22Init.+USB+2.0%22&aqs=chrome..69i57.1361j0j7&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=guvcview+command+line)
[g_strdup - Google Search](https://www.google.co.kr/search?q=g_strdup&oq=g_strdup&aqs=chrome..69i57j0l5.193j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[String Utility Functions: GLib Reference Manual](https://developer.gnome.org/glib/stable/glib-String-Utility-Functions.html#g-strdup)
[gboolean - Google Search](https://www.google.co.kr/search?q=gboolean&oq=gboolean&aqs=chrome..69i57j0l5.1312j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)
[Basic Types: GLib Reference Manual](https://developer.gnome.org/glib/stable/glib-Basic-Types.html#gboolean)
[v4l2_read - Google Search](https://www.google.co.kr/search?q=v4l2_read&oq=v4l2_read&aqs=chrome..69i57.120j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)
[Linux/drivers/media/video/v4l2-dev.c - Linux Cross Reference - Free Electrons](http://lxr.free-electrons.com/source/drivers/media/video/v4l2-dev.c?v=3.5#L269)
[v4l2_read site:linuxtv.org - Google Search](https://www.google.co.kr/search?q=v4l2_read+site%3Alinuxtv.org&oq=v4l2_read+site%3Alinuxtv.org&aqs=chrome..69i57.4513j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[Video for Linux Two API Specification](http://www.linuxtv.org/downloads/legacy/video4linux/API/V4L2_API/spec-single/v4l2.html#func-read)
[v4l2grab - Google Search](https://www.google.co.kr/search?q=v4l2grab&oq=v4l2grab&aqs=chrome..69i57j69i60l3&sourceid=chrome&es_sm=122&ie=UTF-8)
[v4l2grab/v4l2grab.c at master · twam/v4l2grab](https://github.com/twam/v4l2grab/blob/master/v4l2grab.c)
[Linux/Documentation/DocBook/media/v4l/v4l2grab.c.xml - Linux Cross Reference - Free Electrons](http://lxr.free-electrons.com/source/Documentation/DocBook/media/v4l/v4l2grab.c.xml?v=3.2)
[V4L2 read()](http://linuxtv.org/downloads/v4l-dvb-apis/func-read.html)
[sys select - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#es_th=1&q=sys%20select)
[select(2) - Linux manual page](http://man7.org/linux/man-pages/man2/select.2.html)
[v4l2_buffer - Google Search](https://www.google.co.kr/search?q=v4l2_buffer&oq=v4l2_buffer&aqs=chrome..69i57j69i59l2j0l3.207j0j9&sourceid=chrome&es_sm=122&ie=UTF-8)
[Buffers](http://linuxtv.org/downloads/v4l-dvb-apis/buffer.html)

##### TEMP8

[thread synchronization only using flag? - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=thread%20synchronization%20only%20using%20flag%3F&es_th=1)
[PaparazziUAV](http://wiki.paparazziuav.org/wiki/Main_Page)
[kernel uvcvideo module source - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=kernel+uvcvideo+module+source)
[yangh/uvcvideo](https://github.com/yangh/uvcvideo)
[linuxtv uvcvideo - Google Search](https://www.google.co.kr/search?q=kernel+code&oq=kernel+code&aqs=chrome..69i57j0l5.2776j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=linuxtv+uvcvideo)
[uvcvideo - LinuxTVWiki](http://linuxtv.org/wiki/index.php/Uvcvideo)
[Linux UVC driver & tools](http://www.ideasonboard.org/uvc/#footnote-12)
[pinchartl/uvcvideo.git - UVC driver development](http://git.linuxtv.org/cgit.cgi/pinchartl/uvcvideo.git/tree/drivers/media/usb/uvc/uvc_driver.c?h=uvcvideo-next)
[pinchartl/uvcvideo.git - UVC driver development](http://git.linuxtv.org/cgit.cgi/pinchartl/uvcvideo.git/tree/drivers/media/usb/uvc/uvcvideo.h?h=uvcvideo-next)
[Linux UVC driver & tools – FAQ](http://www.ideasonboard.org/uvc/faq/#faq5)
[Linux UVC driver & tools – FAQ](http://www.ideasonboard.org/uvc/faq/#faq7)
[Linux UVC driver & tools – FAQ](http://www.ideasonboard.org/uvc/faq/#faq6)
[modprobe quirks 128 - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=modprobe%20quirks%20128&es_th=1)
[v4l2 - How do I enable the UVC_QUIRK_FIX_BANDWIDTH quirk in Linux UVC Driver? - Stack Overflow](http://stackoverflow.com/questions/25619309/how-do-i-enable-the-uvc-quirk-fix-bandwidth-quirk-in-linux-uvc-driver)
[Bug 473878 – Low framerate with uvcvideo device](https://bugzilla.redhat.com/show_bug.cgi?id=473878#c10)
[failed to submit urb 0 (-28) - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=failed%20to%20submit%20urb%200%20(-28)&es_th=1)
[Bug #899165 “uvcvideo: Failed to submit URB 0 (-28).” : Bugs : linux package : Ubuntu](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/899165)
[Linux USB development and users ()](http://comments.gmane.org/gmane.linux.usb.general/55162)
[[PATCH] EHCI : Fix a regression in the ISO scheduler -- Linux USB](http://www.spinics.net/lists/linux-usb/msg54992.html)
[Bug #901476 “uvcvideo regression: Failed to submit URB 0 (-28)” : Bugs : linux package : Ubuntu](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/901476)
[Bug #899165 “uvcvideo: Failed to submit URB 0 (-28).” : Bugs : linux package : Ubuntu](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/899165)
[uvcvideo: Failed to submit URB 0 (-28) -- Linux USB](http://www.spinics.net/lists/linux-usb/msg55008.html)
[kernel/git/stable/linux-stable.git - Linux kernel stable tree](http://git.kernel.org/cgit/linux/kernel/git/stable/linux-stable.git/commit/?id=f0cc710a6dec5b808a6f13f1f8853c094fce5f12)
[Gnus - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Gnus)
[Red Hat Customer Portal](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/sec-Displaying_Information_About_a_Module.html)

##### TEMP9

[Linux UVC driver & tools](http://www.ideasonboard.org/uvc/#footnote-12)
[Logitech C270 가격 - Google Search](https://www.google.co.kr/search?q=Logitech+HD+Webcam+C270&oq=Logitech+HD+Webcam+C270&aqs=chrome..69i57j69i60l2&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=Logitech+C270+%EA%B0%80%EA%B2%A9)
[로지텍 HD WebCam C270 종합정보 행복쇼핑의 시작 ! 다나와 (가격비교) - Danawa.com](http://prod.danawa.com/info/?pcode=1227296&cate1=863&cate2=892&cate3=10683&cate4=0)
[Wonjung messaged you](https://www.facebook.com/)
[tiny usb webcam water resistant - Google Search](https://www.google.co.kr/search?q=tiny+usb+webcam&newwindow=1&espv=2&biw=1280&bih=668&source=lnms&tbm=isch&sa=X&ei=_C6VVdr5JOO9mgXgvr6YDA&ved=0CAYQ_AUoAQ#newwindow=1&tbm=isch&q=tiny+usb+webcam+water+resistant&imgrc=uX5o828ZVA7rmM%3A)
[E306 18mm 170-degree External Water Resistant 1/3" Colored CMOS Car Rearview Camera (Black)](http://www.focalprice.com/ER2248B/E306_18mm_170_degree_External_Water_Resistant_1_3_Colored_CMOS_Car_Rearview.html)
[Shopping Tourism: Shop Internationally at Sears](http://www.sears.com/en_intnl/dap/shopping-tourism.html)
[USB 10 m Night Vision Water-Resistance LED Pipe Endoscope (Black)](http://www.dealwinwin.com/Computers-amp-Networking/USB-10-m-Night-Vision-Water-Resistance-LED-Pipe-Endoscope-Black--ID40926/)
[SJ75 30M Water Resistant 120-degree Wide-angle 5.0MP H.264 Full HD 1080P Sports DV Video Camera with HDMI /AV-out (Grey) - Worldwide Free Shipping on BuySKU.com](http://www.buysku.com/wholesale/sj75-30m-water-resistant-120-degree-wide-angle-5-0mp-h-264-full-hd-1080p-sports-dv-video-camera-with-hdmi-av-out-grey.html)
[Wholesale 20M Water Resistant Full HD H.264 1080P 5MP Sports DV/Video Camera SJ72 (Black/US Plug)](http://www.eachmall.com/goods-24999-24999.html)
[20m H 물 방지. 264 풀 HD HD DV 1080p의 스포츠 비디오 카메라-에서20m H 물 방지. 264 풀 HD HD DV 1080p의 스포츠 비디오 카메라 우리는 선택합니다 빠른 방법을!   당신의 이해를위한 감사합니다! 이 제품은 초- 작은 부터 소형 비디오 촬영기 의 Aliexpress.com | Alibaba 그룹](http://ko.aliexpress.com/item/20M-Water-resistant-H-264Full-HD-1080P-Sports-HD-DV-Video-Camera/1021778481.html)
[Mini USB Webcam 5.0 MP Tiny for Laptops, netbooks, notebooks - For DELL,HP,Sony,TOSHIBA,ACER,ASUS,IBM,Use with any Laptop That has USB Port](http://www.laptopsandparts4less.com/index.php?p=product&id=29348&parent=8&is_print_version=true)
[Y3000 HD 720P Mini Camera With Motion Detection Audio Record Webcam HD Hidden Camera Tiny Camcorder [Y3000] - US$28.00 : Olyeem Electronics Co., Limited](http://www.hiddencamerakit.com/y3000-hd-720p-mini-camera-with-motion-detection-audio-record-webcam-hd-hidden-camera-tiny-camcorder_p243.html)
[IDS uEye XS USB 2 mini camera, 5MP CMOS, Auto Focus, Full HD Video](https://en.ids-imaging.com/store/produkte/kameras/usb-2-0-kameras/ueye-xs/show/all.html)

##### TEMP10

[Would You by KIDS FLY - Google Search](https://www.google.co.kr/search?q=Would+You+by+KIDS+FLY&oq=Would+You+by+KIDS+FLY&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[Linux UVC driver & tools](http://www.ideasonboard.org/uvc/#footnote-12)
[Logitech C270 가격 - Google Search](https://www.google.co.kr/search?q=Logitech+HD+Webcam+C270&oq=Logitech+HD+Webcam+C270&aqs=chrome..69i57j69i60l2&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=Logitech+C270+%EA%B0%80%EA%B2%A9)
[로지텍 HD WebCam C270 종합정보 행복쇼핑의 시작 ! 다나와 (가격비교) - Danawa.com](http://prod.danawa.com/info/?pcode=1227296&cate1=863&cate2=892&cate3=10683&cate4=0)
[automotive webcam - Google Search](https://www.google.co.kr/search?q=tiny+usb+webcam&newwindow=1&espv=2&biw=1280&bih=668&source=lnms&tbm=isch&sa=X&ei=_C6VVdr5JOO9mgXgvr6YDA&ved=0CAYQ_AUoAQ#newwindow=1&tbm=isch&q=automotive+webcam&imgrc=K7vsf1bTZxr3DM%3A)
[Buy Logitech HD Webcam C310 Online at Lowest Price in India | Logitech HD Webcam C310 Reviews and Specifications - Junglee.com](http://www.junglee.com/Logitech-960-000585-HD-Webcam-C310/dp/B003LVZO8S)
[5mp Mini Logitech Usb Webcam - Buy Usb 2.0 Webcam,Mini Usb Pc Camera Clip Webcam,Laptop 1.3m Pixel Pc Usb Webcam Product on Alibaba.com](http://www.alibaba.com/product-detail/5mp-mini-logitech-usb-webcam_278985940.html)
[[추천 이제품]마이크로소프트 HD급 웹캠 - 대한민국 IT포털의 중심! 이티뉴스](http://www.etnews.com/201104280092)
[라이프캠 HD-3000 가격 - Google Search](https://www.google.co.kr/search?q=%EB%9D%BC%EC%9D%B4%ED%94%84%EC%BA%A0+HD-3000&oq=%EB%9D%BC%EC%9D%B4%ED%94%84%EC%BA%A0+HD-3000&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=%EB%9D%BC%EC%9D%B4%ED%94%84%EC%BA%A0+HD-3000+%EA%B0%80%EA%B2%A9)
[Microsoft Lifecam HD-3000 종합정보 행복쇼핑의 시작 ! 다나와 (가격비교) - Danawa.com](http://prod.danawa.com/info/?pcode=1354881&cate1=863&cate2=892&cate3=10683&cate4=0)
[iCubie Webcam - The Tiny Webcam Compatible with Mac and Windows PC](http://www.ecamm.com/mac/icubie/)
[tiny usb webcam water resistant - Google Search](https://www.google.co.kr/search?q=tiny+usb+webcam&newwindow=1&espv=2&biw=1280&bih=668&source=lnms&tbm=isch&sa=X&ei=_C6VVdr5JOO9mgXgvr6YDA&ved=0CAYQ_AUoAQ#newwindow=1&tbm=isch&q=tiny+usb+webcam+water+resistant&imgrc=uX5o828ZVA7rmM%3A)
[E306 18mm 170-degree External Water Resistant 1/3" Colored CMOS Car Rearview Camera (Black)](http://www.focalprice.com/ER2248B/E306_18mm_170_degree_External_Water_Resistant_1_3_Colored_CMOS_Car_Rearview.html)
[USB 10 m Night Vision Water-Resistance LED Pipe Endoscope (Black)](http://www.dealwinwin.com/Computers-amp-Networking/USB-10-m-Night-Vision-Water-Resistance-LED-Pipe-Endoscope-Black--ID40926/)
[SJ75 30M Water Resistant 120-degree Wide-angle 5.0MP H.264 Full HD 1080P Sports DV Video Camera with HDMI /AV-out (Grey) - Worldwide Free Shipping on BuySKU.com](http://www.buysku.com/wholesale/sj75-30m-water-resistant-120-degree-wide-angle-5-0mp-h-264-full-hd-1080p-sports-dv-video-camera-with-hdmi-av-out-grey.html)
[Wholesale 20M Water Resistant Full HD H.264 1080P 5MP Sports DV/Video Camera SJ72 (Black/US Plug)](http://www.eachmall.com/goods-24999-24999.html)
[20m H 물 방지. 264 풀 HD HD DV 1080p의 스포츠 비디오 카메라-에서20m H 물 방지. 264 풀 HD HD DV 1080p의 스포츠 비디오 카메라 우리는 선택합니다 빠른 방법을!   당신의 이해를위한 감사합니다! 이 제품은 초- 작은 부터 소형 비디오 촬영기 의 Aliexpress.com | Alibaba 그룹](http://ko.aliexpress.com/item/20M-Water-resistant-H-264Full-HD-1080P-Sports-HD-DV-Video-Camera/1021778481.html)
[Mini USB Webcam 5.0 MP Tiny for Laptops, netbooks, notebooks - For DELL,HP,Sony,TOSHIBA,ACER,ASUS,IBM,Use with any Laptop That has USB Port](http://www.laptopsandparts4less.com/index.php?p=product&id=29348&parent=8&is_print_version=true)
[Y3000 HD 720P Mini Camera With Motion Detection Audio Record Webcam HD Hidden Camera Tiny Camcorder [Y3000] - US$28.00 : Olyeem Electronics Co., Limited](http://www.hiddencamerakit.com/y3000-hd-720p-mini-camera-with-motion-detection-audio-record-webcam-hd-hidden-camera-tiny-camcorder_p243.html)
[USB 2 uEye XS - USB 2.0 machine vision camera - IDS Imaging Development Systems GmbH](https://en.ids-imaging.com/xs-home.html)

---

######2015Jul05 02:54:25+0900

위에 아카이브에 대한 정리는 미루고...

지금 상태를 다시 정리해 봐야 겠다.

---

######2015Jul05 22:36:57+0900

archlinux로도 해봤는데, uvcvideo 모듈에서 막히는 것은 똑같다. 그래서 uvcvideo 만드는 사람들의 사이트에서 보이는. uvcvideo로 지원이 되는 카메라 리스트라는 것이 암시하는. 지원이 되는 카메라가 있고 안되는 카메라가 있다는 점. 을 다시 생각해보게 되었고.

다시한번 uvc 라는 것의 의미에 대해서 궁금해졌다. 즉, uvc 라는 것은 어떤 standard라고 하는데. 이게 그냥 리눅스 자체의 스탠다드인지. 아님 어떤건지?

찾아본 결과 uvc라는 개념은 usb standard의 한 카테고리로서.. usb device class의 일종이므로. 이것은 그야 말로. usb 자체의 standard 이다. 따라서 윈도우/맥/리눅스.. 모든 플랫폼에서 적용이 되어야 하는 것이고. 장치 개발/제작 업자들도 본인들의 장치가 uvc에 맞게 제작되도록 할 동기부여가 된다.

uvc란 것은 usb의 standard 이므로, 별도의 드라이버 설치가 없이 작동이 되어야 하는 것인데.. 윈도우에서는 대부분의 장치가 드라이버(closed source)를 따로 설치하게 되어있기 때문에. 이 개념이 두드러지지 않는다. mac osx의 경우에도 분위기는 조금 다를 수 있지만, 큰 맥락에서는 동일. 드라이버를 업체에서 제공하고 그걸 깔면된다.

상대적으로 사용자층이 없어서인지, 리눅스용 드라이버를 제작해서 제공하는 업체는 적다. 비싼 장비가 아닌 이상. 그래서 리눅스에서는 uvc에 의존할 수 밖에 없게 되는데.. 결국 uvc의 여러가지 기능들을 부분적으로 지원하거나, 불완전하게 지원하는 장비들이 많이 있게 되고. 우리가 지금 사용하고 있는 카메라는 sonix의 SN9C259 칩을 사용한다는데.. (검색도 안되는 칩이고 자료도 없다. 쩝. 왜 적었는지?) 이것이 uvc standard를 불완전하게 지원하고 있는 것이다.

그래 뭐 그렇다 치자. 그럼 다른 카메라. 잘 지원이 되는 카메라를 찾아야 한다. 지금도 quickcam E 3500 이 한대 있는데 이 카메라는 uvcvideo 모듈을 quirks=128 하지 않다고 작동이 된다. 이런 걸 봐도 카메라를 바꿔야 할 문제이다. odroid 측에서 이래저래 활용할 수 없는 쓰레기 카메라를 팔았다는 부분에 대해서는 실망이 크지만.

원래 youtube의 영상에서, 4채널 usb 카메라를 streaming 하는 예제에서 사용하는 카메라가 logitech c270 이란 것인 걸 봐도 그렇다. 따라서.. android로 한번 해볼라고 했는데. 그거 해봤자 안될 가능성이 높다.

본인들도 자신들의 카메라로 그 예제 안돌리면서 왜 판건지 정말 그지 같다.

-

결국 보고서의 결론은 카메라를 바꿔봐야 함. 이 될 것이다. 내일 당장 카메라를 사다가 해보면 될 일이지만.. 뭐.. 일단 오늘까지 써 내야 하니까..

그렇게 해서 작동이 되는 카메라를 찾았다고 치자. 그래도 렌즈 교체 문제나 차량 외장 문제.. 적절한지 등등 해결해야 할 문제는 계속된다. 그래서 카메라를 좀 찾아봤고. ELF 라는 대만회사가 공급하는 카메라가 좋아보였다. 국내에도 파는 곳이 좀 있다. 일단 사다가 테스트는 가능 할 것이다. 이들도 uvc를 지원한다고는 하나.. 해봐야 알 일이니깐.

-

결국 지금 안드로이드 깔았는데 이거 하는데 또 몇시간이 걸렸는지 모르지만. 헛짓이었단 말이네. 깔았으니까 해보면 좋긴하겠지만. 뭐 됐다.. 아마 못해내지 싶다. 너무 새로운 영역이라. 좋은 경험은 되겠지만, 사실 그렇게 해서 계속 안드로이드로 끝까지 개발하고 ndk로 성능까지 뽑아낼 자신이 있으면 노력해볼 여지라도 있겠지만. 그럴 생각도 없고. 그럴 자신도 없는데 해봐야 뭐하나.

-

결국 안드로이드/ 리눅스를 비교하는 식의 보고는 무의미하다. 나는 애초에 안드로이드에 대해서는 관심도 없다. 미친 오드로이드 새끼들은 개 쓰레기 같은 갤럭시 때문에 안드로이드만 좀나 침흘리면서 관심이지만. 써글.

---

######2015Jul05 22:56:41+0900

일단 그동안 쌓인 링크를 또 한번 깔아놔보자.


#####Sunday, July 5, 2015 1:14 AM

[Linux UVC driver & tools](http://www.ideasonboard.org/uvc/)
[media_build.git - Build system to compile media subsystem on legacy kernels](http://git.linuxtv.org/cgit.cgi/media_build.git/tree/)
[kernel git - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=kernel%20git&es_th=1)
[linux/uvc_video.c at v3.10-rc7 · torvalds/linux](https://github.com/torvalds/linux/blob/v3.10-rc7/drivers/usb/gadget/uvc_video.c)
[History for drivers/usb/gadget/uvc_video.c - torvalds/linux](https://github.com/torvalds/linux/commits/v3.16-rc7/drivers/usb/gadget/uvc_video.c)

**archlinux 를 해볼까. 그러면 다른 패치가 된 드라이버가 들어있을 수 있지 않을까 희망을 가짐**

archlinux에 대한 사전 조사.. - 괜찮겠다. 싶음.

[comparison archlinux - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=comparison+archlinux)
[ODROID-XU3 | Arch Linux ARM](http://archlinuxarm.org/platforms/armv7/samsung/odroid-xu3)
[The Arch Way - ArchWiki](https://wiki.archlinux.org/index.php/The_Arch_Way)
[Comparison of Linux distributions - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Comparison_of_Linux_distributions)

깔기 시작. 여러가지 공부할게 많았으나, 어렵지 않게 따라감. 워낙 체계적으로 잘 되어있는 편이라고 느꼈음..

[Beginners' guide - ArchWiki](https://wiki.archlinux.org/index.php/Beginners%27_guide#Prepare_the_storage_devices)
[General recommendations - ArchWiki](https://wiki.archlinux.org/index.php/General_recommendations)
[Installation guide - ArchWiki](https://wiki.archlinux.org/index.php/Installation_guide)
[Core utilities - ArchWiki](https://wiki.archlinux.org/index.php/Core_utilities#iconv)
[Arch Linux - Downloads](https://www.archlinux.org/download/)
[motd (Unix) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Motd_(Unix))
[https://www.kernel.org/doc/Documentation/kbuild/modules.txt](https://www.kernel.org/doc/Documentation/kbuild/modules.txt)
[ODROID Forum • View topic - ODROID GameStation Turbo with XBMC](http://forum.odroid.com/viewtopic.php?f=11&t=2684)

##### (MAC) TEMP - Jul05 1am
 
[ODROID-XU3 | Arch Linux ARM](http://archlinuxarm.org/platforms/armv7/samsung/odroid-xu3)
[Arch Linux - Downloads](https://www.archlinux.org/download/)
[Installation guide - ArchWiki](https://wiki.archlinux.org/index.php/Installation_guide)
[Partitioning - ArchWiki](https://wiki.archlinux.org/index.php/Partitioning)
[Beginners' guide - ArchWiki](https://wiki.archlinux.org/index.php/Beginners%27_guide)
[General recommendations - ArchWiki](https://wiki.archlinux.org/index.php/General_recommendations#Package_management)
[Users and groups - ArchWiki](https://wiki.archlinux.org/index.php/Users_and_groups#Example_adding_a_user)
[pacman - ArchWiki](https://wiki.archlinux.org/index.php/Pacman#Installing_packages)
[Mirrors - ArchWiki](https://wiki.archlinux.org/index.php/Mirrors)
[Category:X Server - ArchWiki](https://wiki.archlinux.org/index.php/Category:X_Server)
[Xorg - ArchWiki](https://wiki.archlinux.org/index.php/Xorg)

윈도우 매니져 / 데스트탑 매니져 등등 까지 올림.

[Window manager - ArchWiki](https://wiki.archlinux.org/index.php/Window_manager)
[IceWM - ArchWiki](https://wiki.archlinux.org/index.php/IceWM)
[Display manager - ArchWiki](https://wiki.archlinux.org/index.php/Display_manager)
[XDM - ArchWiki](https://wiki.archlinux.org/index.php/XDM)
[Metacity - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Metacity)
[Arch Linux - xorg-drivers (x86_64) - Group Details](https://www.archlinux.org/groups/x86_64/xorg-drivers/)
[Install from existing Linux - ArchWiki](https://wiki.archlinux.org/index.php/Install_from_existing_Linux)
[Oracle VM VirtualBox - UserManual.pdf](file:///Users/doohoyi/Desktop/Oracle%20VM%20VirtualBox%20-%20UserManual.pdf)
[Sudo - ArchWiki](https://wiki.archlinux.org/index.php/Sudo)
[VirtualBox - ArchWiki](https://wiki.archlinux.org/index.php/VirtualBox#Installation_steps_for_Arch_Linux_guests)
[Xfce - ArchWiki](https://wiki.archlinux.org/index.php/Xfce)
[xinitrc - ArchWiki](https://wiki.archlinux.org/index.php/Xinitrc)
[direct usb osx virtualbox - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=direct%20usb%20osx%20virtualbox&es_th=1)
[mavericks - How to set VirtualBox in order to access external hard drives in guest Win7? - Ask Different](http://apple.stackexchange.com/questions/125225/how-to-set-virtualbox-in-order-to-access-external-hard-drives-in-guest-win7)
[VirtualBox 2: How To Pass Through USB Devices To Guests On An Ubuntu 8.10 Host](https://www.howtoforge.com/virtualbox-2-how-to-pass-through-usb-devices-to-guests-on-an-ubuntu-8.10-host)
[How to access an SD card from a virtual machine? - Super User](http://superuser.com/questions/373463/how-to-access-an-sd-card-from-a-virtual-machine)
[ODROID | Hardkernel](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G140448267127&tab_idx=2)
[tutorial: “visudo: command not found” | Erik's blog](https://erikeldridge.wordpress.com/2009/07/30/tutorial-visudo-command-not-found/)

마지막으로.. guvcview를 깔아서 몇가지 테스트 해봄. -> 결과는 same. 혹은 worse..

##### Sunday, July 5, 2015 1:53 PM

**안드로이드에서는 되는데! 라고만 생각함.. 그 영상에 쓰인 카메라는 로지텍꺼임! 안드로이드에서도 이 카메라는 안될꺼임.**

안드로이드의 드라이버를 가져다가 쓸순 없을까? 생각함.
둘의 관계. 기술적으로.. 조사.

[is android driver compatible with linux - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=is%20android%20driver%20compatible%20with%20linux&es_th=1)
[Setting Up ADB/USB Drivers for Android Devices in Linux (Ubuntu) | Esau Silva](http://esausilva.com/2010/05/13/setting-up-adbusb-drivers-for-android-devices-in-linux-ubuntu/)
[Android and Linux re-merge into one operating system | ZDNet](http://www.zdnet.com/article/android-and-linux-re-merge-into-one-operating-system/)

USB URB 란 뭔지 좀 알자.

[usb driver linux urb "failed submit - Google Search](https://www.google.co.kr/search?q=usb+driver+linux+urb&oq=usb+driver+linux+urb&aqs=chrome..69i57j0.11318j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=usb+driver+linux+urb+%22failed+submit)
[13.3. USB Urbs](http://www.makelinux.net/ldd3/chp-13-sect-3)
[Linux Device Drivers, 3rd Edition](http://www.makelinux.net/ldd3/?u=chp-13-sect-4)
[Interactive map of Linux kernel](http://www.makelinux.net/kernel_map/)
[usb_submit_urb](https://www.kernel.org/doc/htmldocs/usb/API-usb-submit-urb.html)
[usb serial driver failing when calling "usb_submit_urb" - why?](http://www.linuxquestions.org/questions/programming-9/usb-serial-driver-failing-when-calling-usb_submit_urb-why-720467/)

카메라의 문제라고 점점 확신 굳어져감.
UVC 카메라란 뭔지? 리눅스에서만 표준인거라고 생각했는데? 그게 아닌건지? 등등 조사.

[quickcam e 3500 가격 - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=quickcam+e+3500+%EA%B0%80%EA%B2%A9)
[Linux UVC driver & tools](http://www.ideasonboard.org/uvc/)
[uvcvideo compatible compact camera - Google Search](https://www.google.co.kr/search?q=uvcvideo+compatible+compact+camera&newwindow=1&espv=2&biw=1280&bih=711&tbm=isch&source=lnms&sa=X&ei=Aq-YVePEIYbFmwXayoeYAg&ved=0CAcQ_AUoAg#imgrc=17RKGfS9UbQ5pM%3A)
[Promotional Uvc Camera Board, Buy Uvc Camera Board Promotion Products at Low Price on Alibaba.com](http://www.alibaba.com/uvc-camera-board-promotion.html)
[Designed For Atm Machine& Industrial Equipment Medical Instrument 1.3mp Atm Machine Usb Mini Pinhole Camera - Buy Mini Pinhole Usb Camera,Usb Mini Pinhole Camera,Usb Mini Camera Product on Alibaba.com](http://www.alibaba.com/product-detail/Designed-For-ATM-Machine-Industrial-Equipment_60213873207.html)
[uvc camera what is that - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=uvc%20camera%20what%20is%20that&es_th=1)
[uvc_camera - ROS Wiki](http://wiki.ros.org/uvc_camera)
[uvc standard - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=uvc%20standard&es_th=1)
[An explanation of the USB Video Class (UVC) | To USB or Not to USB](http://blogs.synopsys.com/tousbornottousb/2012/12/06/an-explanation-of-the-usb-video-class-uvc/)

##### Sunday, July 5, 2015 2:31 PM

[webcam standard - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=webcam+standard)
[USB video device class - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/USB_video_device_class)
[usb standard - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=usb%20standard&es_th=1)
[USB.org - USB Device Class Specifications](http://www.usb.org/developers/docs/devclass_docs/)
[uvcvideo compatible compact camera - Google Search](https://www.google.co.kr/search?q=uvcvideo+compatible+compact+camera&newwindow=1&espv=2&biw=1280&bih=711&tbm=isch&source=lnms&sa=X&ei=Aq-YVePEIYbFmwXayoeYAg&ved=0CAcQ_AUoAg#imgrc=17RKGfS9UbQ5pM%3A)
[linux uvc camera list - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=linux%20uvc%20camera%20list&es_th=1)
[uvc camera with lens - Google Search](https://www.google.co.kr/search?q=uvc+camera+with+lens&newwindow=1&es_sm=122&source=lnms&tbm=isch&sa=X&ei=e7eYVZTMBc7c8AXg-5vYCg&ved=0CAcQ_AUoAQ&biw=1280&bih=668#imgrc=4bxnbV_--XAhXM%3A)

UVC 라고 하면, 국제 표준 급이고.. USB 자체 표준이란 사실 이해.
-> 본격적으로 쓸만한 카메라 찾기 시작... -> 여기서 혼돈... uvcvideo에서 지원하는 카메라리스트에서 고르면 되는지.. 케이싱도 보고.. 모양 가격 따져서.. 웹에서 UVC 지원이라는 걸 믿고 골라야 되는지..

장단점이 있는데.. 지원리스트에 있는 것들을 확실하게 작동이 될 것이므로 일단 필요하다. 그냥 구매. 모양 보지 말고.
근데 그래봤자 이런 카메라를 쓸 수는 없으니.. UVC 지원 주장하는 카메라들 중에 몇가지를 구해서 테스트. 그리고 최종 결정.

[Wholesale Mjpeg YUY2 HD 5 Megapixl usb survillance UVC android linux ov5640 cmos camera lens module - Alibaba.com](http://www.alibaba.com/product-detail/Mjpeg-YUY2-HD-5-Megapixl-usb_60085301717.html)
[uvc camera lens - Google Search](https://www.google.co.kr/search?q=uvc+camera+lens&newwindow=1&espv=2&biw=1280&bih=668&source=lnms&tbm=isch&sa=X&ei=ErmYVfvHAoiM8QXetLuABg&ved=0CAYQ_AUoAQ#imgrc=BvYPwXf3elQFlM%3A)
[usb 카메라 국내 회사 - Google Search](https://www.google.co.kr/search?q=usb+%EC%B9%B4%EB%A9%94%EB%9D%BC+%EA%B5%AD%EB%82%B4+%ED%9A%8C%EC%82%AC&newwindow=1&espv=2&biw=1280&bih=711&site=webhp&tbm=isch&tbo=u&source=univ&sa=X&ei=18CYVcPJKYWl8QXCs6jQBA&ved=0CBsQsAQ#imgrc=-W5c4umfU0MeUM%3A)
[Samyang Optics, Lenses for Camera – Cine – CCTV – Photo, Prime Lenses & Third party Lenses](http://www.syopt.co.kr/kr/)
[전자키트의 모든것 ::: 스마트키트](http://www.smartkit.co.kr/shop/item.php?it_id=9916640730)
[전자키트의 모든것 ::: 스마트키트](http://www.smartkit.co.kr/shop/list.php?ca_id=b0&skin=&ev_id=&sort=&page=1)
[전자키트의 모든것 ::: 스마트키트](http://www.smartkit.co.kr/shop/item.php?it_id=9948471348)
[Google Search](https://www.google.co.kr/search?tbs=sbi:AMhZZitGSHosCfExjN-aurc46o8szcAibpC_1QJXzmkcvrruo9kLfOgbOUlDpazObXcls-eUx1uC8LYHxz4mwYb1FHFajK3AL2eVSJ9NcXcYv_1VHIJhve5Vj0ll-iOq8aHEB9vFIKi-LAc9tn4tOuadNZTX-uLM8wTUmXIu66ABiIQOY7Zjm_1BtHaUT1nL4OYQNgAe5X9GOqVNWRz-09zifp3lKqnF4GrZN05pFRkSA3z8yZYsfPJV5FAckHVEQRsf3yMLmBnpVLKnOCZHC-bvgQuWeK5I9nnnLvk9m9NHravgzbTOjTaLxQfLBYRnl-Ts3Tk0gXuh_1EgTP4IOdUWk0cj2H2NkQbl5TBC69vY4EHIx3JGfYNiimGZcmEjbBr4daLc8np0I0p9St6Ro_1xEgI5qwBZoCpjvWTNnXIpTdXwmRGXo729uE4kDaUclPLb8CotVF56Zru_1p-ZwnHHhuMb399TzEKpLYFFJD9fkXEIRb2PGn9RJrhbfz1NjDTDF846yjBerKoJ1hKHMm0U7VWcR2wRq30iSSzSDg6Wc00zWhaeeqoG4ZtRACOIno9pNOn_1vlFbd8ZKewAh1HbDfE2g-ytVDShBaZua_1T_1uwPq0CIhvxx6MbqAhSMwvlxTmueHjjixNKcvPQWt5bN2F9oLb3l_10KvbLwugtVDn29gQl_1LXjd4ZDC7dcMbh8fIvzFiavz_1XCpzJ6_1GeyOuFJru4IcHSRUB6zXujMw7HmS8TVq9WBajYcsq_18He3UWkh6tMtjuiTyTj5THd1wp47ZYGX5-fFEUMmiMXntAAdlAZNaXtzUwd_1vXftN0vlINSlPqrd5QFIxpikhpPzai2OzUpHHqYB3LS99QV0CxUXjH3Qqh85x2H4XglBSLg2VkaIqKab1oCUOnYJl4NYBYz2xkW_1eKvSZ636TdNmRXio-q-e947j49hX7pQQkS3_13RqWQOFi4m1KG3TnH1BB39mKnn8Xpv-5_178GLeoQSywYvH0B6NO6uZp2T1Ap6v2WRsqDxlWGZUkOCH2DjAQINNFKXwZcRRUYI89ZDFFd9e8MBUFStbcS7PRMN5org0E2lBbafXJQy1ayGNWMd04nNxWAhDvlCwxRzL-Mr_1mScmglQl7adYziJ9a8iGE38nCsu7gBtOz3QMkHqtSIBpsWHO63GYU5b_1N-9l9isGIp0jdeWBopUuTpKleFjXuaFFqNjLAeQxk6VeT9MsumEptsMfj0-qFxtTRBe8FVcQ4juWD3QTw8jofr1bXPX79Sw_1w5M5ncEQztf-AnMKL8zDlBszIsfspqtJ8B2cXby-AwMz9Vzcjn_1TGlrFaORt3JAOk80fFiJYnUdT4Jlmy5zlv5foA-3sBznOEjkzJa7WmG2Imf3FkdS6fHvOIlmXfpW0k0gqGIC9a2BhmV9XdK2TCQRqyydD5maEZZGql-kSi17UCbQJpDBp90u_1f8FMnkC5y7h1AEumG1xxXjE_1P_1RMSHhfh7oc9TU8D6dEVEM7rpoRc8Bbx_1nQwgrm-V4e7n2GYJCYvWS2OSa2k_12lGwoREYwdNsmGWTAY8esbeqRLoUjkqN6FIkOSQt70rQa1TdFAlXZjwlnSjkaRx8YjBWuVY)
[www.partsfriend.com/src/products/products_detail.php?product_category_id=2980&product_category_id_main=0&product_mst_id=2980_00005&now_page=1](http://www.partsfriend.com/src/products/products_detail.php?product_category_id=2980&product_category_id_main=0&product_mst_id=2980_00005&now_page=1)
[Google Search](https://www.google.co.kr/search?tbs=sbi:AMhZZiuBg6hLbrEby8OgGNyPog6JmXrRBEzX-v76p9_1HWcOPdtmGrXTePaJ4iaFT7Zwu7qRkQOxcfIKjaurOr4_1VCILmhIuwCw64RYpsEzvnGHRDreGfffoZi98lZsxFbB-IcWV2PWCttvUW3HPGqBycNWViZs64wnQxxxwW6Jml9ss-_1cJfJjSCWPeDYpvuS8PxgeFoGcjaCv85gZRPt5LSCYpmhebmCLQ9nhiy-oBUHVKNz-CS5ZIaNsTOlsKpuJhvBRvtpv8q8q8Itg7nxqLbJbj2QXTNqFSATSNqpZc2CdIiJ25zAR4YL4BSya0UE3z0Gd_1f-AhWiDxmdKqEw-pGg7zXXlCAeloxmCQGUoDr8HqpSTs_1wDJ9HbctPPRzhl1CIX8F5SrLiHXo7iaSFo9DaUF5waASyBd9O8DvJGoX1UJYAp6c6Hgs-vvTiOLaQDjtAPcLMQd-1evDHQUzp3igH9CEDGhGhPKSBUmz0IlUNAeqJkgrklPO92tDvnDCyOrsOhiNwnLvLKRm20bArYOB1bz0Mqicj1K197wDM-WW9fRpO9MQiHTVFwIZXm5sGyHMh-mzxI1GJZQ_1sE1zGjqiClBGY2wVOt7IhCUsA1XCQhMxQVZN85QQt0TQ-vpF5Z-0_1qlg09IjOKlRIx01Kpo2fYntOTfF-5MJV1PQtAxcqhlknODZlZkAwTLI90zwC_1bjmva41cAP-qH-1q9basP3Iuoa3I-QYyq68q3QI2zyWd0fNAzAD9-oNNR_1XCXK43iuypgImcHStwyWHMOaJgI6stL1YA7t1BEhoAIpT0cbPgcXe5xx384-lo8z0A3hI0TcMj9twWb9AAgyOpwQ3VoNSkZRJ4FhJkD3jPhi4Hre3IVP9Ic-5yvI9ZuSvz_1xznw4YKEo7qjOpqiUWsa1dhUW8_1C1EvY4GJZKL0GhBpT_1SGsTMAD-osWqOZNsX2V2lbABeRezKcacxty91YYWlbOmZa-MnjGQClP9L858kpfPo3hDN-C_1d8K0Z9WxrnGmBtiKlpQ_1WR0AXPvt4gaUddMm-51Q7ZzmnJFZ8_1GobSjUkAAyKVF2xG_1qcIORcRnP1AasQs2OunikuA3Us5F_1xWIAqejgv2PU2a34L9TgOZDCHwNdUMNR84axhUAsLH_1HpI4RA88BgIBELlRFoNMnl8ZSHbPSMuJcEy4gTgmvn8gUKdXeTnZE2SycSTiSrNjN5EE2wxvGFfduhQWVEWPJ0BHlyv0ScnhLbRERjlhykHjj8pMN4b4S2C5i5qGwoWHWqFngC5dvdIRDy2i8xEHgI3rRpX_17x3AhZJS1RryuVoXuIk5GpB9Hl81KmnufkMxl4qQclvai7bVaq9d9aWeQ_1xhU1LYbLL2gRUt43ViFmiOnSYs9z4lULrhDAHchtKXZbHqXyUhGesOqtZHH7C53XZBil6xVtSUEnclYPMHk8LtEwC_1SHEz2zFlsm9DYDlvC9m8VfoZgFJ7BehdifZmrdTPJw5J8GTtlYUWwygwcEoGpsl78AfS83Ng)
[퍼니키트](http://www.funnykit.co.kr/shop/goods/goods_list.php?category=028005004&page=3)
[전자부품/전자공구/개발보드 전문쇼핑몰 - 툴파츠](http://toolparts.co.kr/front/php/category.php?cate_no=1012&page=5&offset=32&&1=1&mode=&sort_method=0&pss_group=&pss_item=#normal_list)
[P5074 uvc - Google Search](https://www.google.co.kr/search?q=P5074&oq=P5074&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=P5074+uvc)
[UVC camera 인식 문제](http://com.odroid.com/sigong/nf_board/nboard_view.php?brd_id=odroidseven&bid=1510)
[MISUMI New Products MISUMI ELECTRONICS CORP.](http://www.misumi.com.tw/NPS.ASP)

##### Sunday, July 5, 2015 2:54 PM

[linux uvc latest version - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=linux%20uvc%20latest%20version&es_th=1)
[Kernel.org git repositories](https://git.kernel.org/cgit/?q=driv)
[kernel/git/geert/renesas-drivers.git - Renesas Kernel Drivers Development Tree](https://git.kernel.org/cgit/linux/kernel/git/geert/renesas-drivers.git/stats/)
[UVC - Community Help Wiki](https://help.ubuntu.com/community/UVC)
[Linux Media kernel patches - Patchwork](https://patchwork.linuxtv.org/project/linux-media/list/)
[Mercurial repositories index](http://linuxtv.org/hg/~pinchartl/)
[[PATCH v1 17/19] uvcvideo: Add UVC 1.5 Encoding Unit controls. -- Linux media](http://www.spinics.net/lists/linux-media/msg67634.html)
[Latest Webcams, UVC and Linux - Logitech Forums](http://forums.logitech.com/t5/Webcams/Latest-Webcams-UVC-and-Linux/td-p/1068779)
[How many versions of the QuickCam Pro 9000, Pro for Notebooks, and Orbit/Sphere AF are there? — QuickCam Team](http://web.archive.org/web/20110903172351/http://www.quickcamteam.net/documentation/faq/how-many-versions-of-the-quickcam-pro-9000-are-there)
[Compiling latest UVC driver - Ask Ubuntu](http://askubuntu.com/questions/386493/compiling-latest-uvc-driver)
[Linux UVC driver & tools](http://www.ideasonboard.org/uvc/#download)
[odroid kernel module compile - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=odroid%20kernel%20module%20compile&es_th=1)
[ODROID Forum • View topic - Compiling custom kernel module for OdroidX2_SD_image_25-Apr-](http://forum.odroid.com/viewtopic.php?f=14&t=1331)
[linux module compile - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=linux+module+compile)
[Kernel modules - ArchWiki](https://wiki.archlinux.org/index.php/Kernel_modules)
[Compiling Kernel Modules](http://www.tldp.org/LDP/lkmpg/2.6/html/x181.html)
[ODROID/Hardkernel Linux kernel - Linux Exynos](http://linux-exynos.org/wiki/ODROID/Hardkernel_Linux_kernel)
[ODROID Forum • View topic - [Solved]Compile v4l for dvbsky 960 (modules missing)](http://forum.odroid.com/viewtopic.php?f=112&t=11018)
[ODROID Forum • View topic - OFFICIAL request for kernel modules/drivers enable here](http://forum.odroid.com/viewtopic.php?f=112&t=7600&start=250)

##### Sunday, July 5, 2015 2:53 PM - *ELP camera*

**ELP라는 좋은 회사 찾음**

원하는 USB 2.0 카메라를 공급할 수 있을 것 같다.
국내에도 들어와있으니 이런것들을 사서 테스트 가능.

[Aliexpress.com : Buy Mjpeg YUY2 Full HD 5 Megapixl cctv survillance usb uvc android linux auto focus lens usb mini camera from Reliable cameras action suppliers on Ailipu Technology co.,LTD | Alibaba Group](http://www.aliexpress.com/store/product/Mjpeg-YUY2-Full-HD-5-Megapixl-cctv-survillance-usb-uvc-android-linux-auto-focus-lens-usb/1762679_2051067049.html)
[Camera Lenses for Machine Vision](http://www.thorlabs.hk/newgrouppage9.cfm?objectgroup_id=1822)
[Factory Outlet wholesale 5mp 2592X1944 12mm lens black box free driver digital uvc usb video camera-in Surveillance Cameras from Computer & Office on Aliexpress.com | Alibaba Group](http://www.aliexpress.com/item/Factory-Outlet-wholesale-5mp-2592X1944-12mm-lens-black-box-free-driver-digital-uvc-usb-video-camera/32348212893.html)
[Google Search](https://www.google.co.kr/search?newwindow=1&tbs=sbi:AMhZZitGSHosCfExjN-aurc46o8szcAibpC_1QJXzmkcvrruo9kLfOgbOUlDpazObXcls-eUx1uC8LYHxz4mwYb1FHFajK3AL2eVSJ9NcXcYv_1VHIJhve5Vj0ll-iOq8aHEB9vFIKi-LAc9tn4tOuadNZTX-uLM8wTUmXIu66ABiIQOY7Zjm_1BtHaUT1nL4OYQNgAe5X9GOqVNWRz-09zifp3lKqnF4GrZN05pFRkSA3z8yZYsfPJV5FAckHVEQRsf3yMLmBnpVLKnOCZHC-bvgQuWeK5I9nnnLvk9m9NHravgzbTOjTaLxQfLBYRnl-Ts3Tk0gXuh_1EgTP4IOdUWk0cj2H2NkQbl5TBC69vY4EHIx3JGfYNiimGZcmEjbBr4daLc8np0I0p9St6Ro_1xEgI5qwBZoCpjvWTNnXIpTdXwmRGXo729uE4kDaUclPLb8CotVF56Zru_1p-ZwnHHhuMb399TzEKpLYFFJD9fkXEIRb2PGn9RJrhbfz1NjDTDF846yjBerKoJ1hKHMm0U7VWcR2wRq30iSSzSDg6Wc00zWhaeeqoG4ZtRACOIno9pNOn_1vlFbd8ZKewAh1HbDfE2g-ytVDShBaZua_1T_1uwPq0CIhvxx6MbqAhSMwvlxTmueHjjixNKcvPQWt5bN2F9oLb3l_10KvbLwugtVDn29gQl_1LXjd4ZDC7dcMbh8fIvzFiavz_1XCpzJ6_1GeyOuFJru4IcHSRUB6zXujMw7HmS8TVq9WBajYcsq_18He3UWkh6tMtjuiTyTj5THd1wp47ZYGX5-fFEUMmiMXntAAdlAZNaXtzUwd_1vXftN0vlINSlPqrd5QFIxpikhpPzai2OzUpHHqYB3LS99QV0CxUXjH3Qqh85x2H4XglBSLg2VkaIqKab1oCUOnYJl4NYBYz2xkW_1eKvSZ636TdNmRXio-q-e947j49hX7pQQkS3_13RqWQOFi4m1KG3TnH1BB39mKnn8Xpv-5_178GLeoQSywYvH0B6NO6uZp2T1Ap6v2WRsqDxlWGZUkOCH2DjAQINNFKXwZcRRUYI89ZDFFd9e8MBUFStbcS7PRMN5org0E2lBbafXJQy1ayGNWMd04nNxWAhDvlCwxRzL-Mr_1mScmglQl7adYziJ9a8iGE38nCsu7gBtOz3QMkHqtSIBpsWHO63GYU5b_1N-9l9isGIp0jdeWBopUuTpKleFjXuaFFqNjLAeQxk6VeT9MsumEptsMfj0-qFxtTRBe8FVcQ4juWD3QTw8jofr1bXPX79Sw_1w5M5ncEQztf-AnMKL8zDlBszIsfspqtJ8B2cXby-AwMz9Vzcjn_1TGlrFaORt3JAOk80fFiJYnUdT4Jlmy5zlv5foA-3sBznOEjkzJa7WmG2Imf3FkdS6fHvOIlmXfpW0k0gqGIC9a2BhmV9XdK2TCQRqyydD5maEZZGql-kSi17UCbQJpDBp90u_1f8FMnkC5y7h1AEumG1xxXjE_1P_1RMSHhfh7oc9TU8D6dEVEM7rpoRc8Bbx_1nQwgrm-V4e7n2GYJCYvWS2OSa2k_12lGwoREYwdNsmGWTAY8esbeqRLoUjkqN6FIkOSQt70rQa1TdFAlXZjwlnSjkaRx8YjBWuVY&ei=p72YVaqXPKilmQXbw4OoAQ&start=10&sa=N&biw=1280&bih=711)
[www.partsfriend.com/src/products/products_detail.php?product_mst_id=P5074-S12](http://www.partsfriend.com/src/products/products_detail.php?product_mst_id=P5074-S12)
[MISUMI New Products MISUMI ELECTRONICS CORP.](http://www.misumi.com.tw/NPS.ASP)
[640X480 USB Cameras,Surveillance Equipment,CCTV Systems,USB Camera Module Supplier](http://www.elpcctv.com/640x480-usb-cameras-c-85_86.html)
[VGA USB BOX Camera USB2.0 OmniVision OV7725 Color Sensor Support YUY and MJPEG with 3.6MM Lens](http://www.elpcctv.com/vga-usb-box-camera-usb20-omnivision-ov7725-color-sensor-support-yuy-and-mjpeg-with-36mm-lens-p-187.html)
[720P Mini Hidden IP Camera with lens support ONVIF](http://www.elpcctv.com/720p-mini-hidden-ip-camera-with-lens-support-onvif-p-45.html)

##### Sunday, July 5, 2015 8:42 PM

**헛짓을 한다.. 안드로이드를 깔고 있다.. 흠..**

[android developer - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=android+developer)
[Download Android Studio and SDK Tools | Android Developers](https://developer.android.com/sdk/index.html#Other)
[Creating an Android Project | Android Developers](https://developer.android.com/training/basics/firstapp/creating-project.html)
[Debug Android applications over Ethernet - TechRepublic](http://www.techrepublic.com/blog/software-engineer/debug-android-applications-over-ethernet/)
[odroid android sdk - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=odroid%20android%20sdk)
[ODROID-XU3 Lite Development Board – Android Setup and Benchmarks](http://www.cnx-software.com/2014/11/21/odroid-xu3-lite-development-board-android-setup-and-benchmarks/)
[odroid 안드로이드 개발 시작하기 - Google Search](https://www.google.co.kr/search?q=5422_4.4.4_master&oq=5422_4.4.4_master&aqs=chrome..69i57j69i60l2&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=odroid+%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C+%EA%B0%9C%EB%B0%9C+%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0)

만일 나중에 다시 안드로이드를 깔 일이 있으면 반드시 아래 이미지를 이용해야 하더라는 점을 명심. 개 쓰레기들. 이런것 때문에 시간 존나 날리게 만든다..

[ODROID-XU3/XU3-Lite Android 4.4.4 OS Alpha 1.5](http://com.odroid.com/sigong/nf_file_board/nfile_board_view.php?keyword&tag&bid=245)

안드로이드에서 해당 예제를 돌려보고 싶어서.. 결국 4채널 영상 loopback을 실현 못했다는게 억울해서... 불쌍하게 노력중..

[codeengn.com/archive/SmartPhone/초보자를 위한 ODROID-7으로 Android 빌드하기 %5BLisa%5D.pdf](http://codeengn.com/archive/SmartPhone/%EC%B4%88%EB%B3%B4%EC%9E%90%EB%A5%BC%20%EC%9C%84%ED%95%9C%20ODROID-7%EC%9C%BC%EB%A1%9C%20Android%20%EB%B9%8C%EB%93%9C%ED%95%98%EA%B8%B0%20%5BLisa%5D.pdf)
[[기초]Exynos-4210 듀얼코어 프로세서로 배우는 안드로이드 플랫폼](http://com.odroid.com/sigong/nf_file_board/nfile_board_view.php?bid=95)
[Exynos4210BeginnerRev10.pdf](file:///C:/Users/doohoyi/Downloads/Exynos4210BeginnerRev10.pdf)
[ODROID Forum • View topic - [Tutorial]AndroidApps with Eclipse on OdroidXu](http://forum.odroid.com/viewtopic.php?f=64&t=5429)
[ODROID Forum • View topic - [Odroid-U3] Android Application Development on Linux](http://forum.odroid.com/viewtopic.php?f=82&t=4134)
[Releases · hardkernel/android](https://github.com/hardkernel/android/releases)

아니, 겨우 앱하나 올려보려는데, 우분투가 필요하대서.. 반신반의.... 버츄얼박스부터 쫙 깜.

[virtualbox - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#es_th=1&q=virtualbox)
[headers for current running kernel ubuntu - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=headers%20for%20current%20running%20kernel%20ubuntu&es_th=1)
[[SOLVED] Unable to install/use guest additions in Ubuntu 13.10!](http://ubuntuforums.org/showthread.php?t=2198058)
[ubuntu 14.04 lts - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=ubuntu%2014.04%20lts&es_th=1)
[Index of /ubuntu-cd/14.04/](http://tw.archive.ubuntu.com/ubuntu-cd/14.04/)
[md5 windows - Google Search](https://www.google.co.kr/search?q=md5+windows&oq=md5+windows&aqs=chrome.0.69i59j69i57j69i60l4.1848j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)
[Is there a built-in checksum utility on Windows 7? - Super User](http://superuser.com/questions/245775/is-there-a-built-in-checksum-utility-on-windows-7)
[odroid 안드로이드 개발 시작하기 - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=odroid%20%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%20%EA%B0%9C%EB%B0%9C%20%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0&es_th=1)
[AppMarkers :: [ODROID-X2] 리눅스 개발환경 설정](http://appmarkers.tistory.com/entry/%EA%B0%9C%EB%B0%9C%ED%99%98%EA%B2%BD-%EA%B5%AC%EC%B6%95%ED%95%98%EA%B8%B0-Setting-up-a-Linux-build-environment)
[몰디브를 꿈꾸며... :: 안드로이드 개발 환경 구축(odroid u3)](http://rin2papa.tistory.com/45)

##### Sunday, July 5, 2015 11:30 PM

[odroid 4 usb - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=odroid+4+usb&newwindow=1&tbm=vid)
[odroid x 4 usb - Google Search](https://www.google.co.kr/search?q=odroid+x&oq=odroid+x&aqs=chrome..69i57j69i61j0j69i65j69i60l2.1327j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#q=odroid+x+4+usb&newwindow=1&tbm=vid)
[H2DRkLFsqQ4 - Google Search](https://www.google.co.kr/search?q=H2DRkLFsqQ4&oq=H2DRkLFsqQ4&aqs=chrome..69i57.1944j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[ODROID Forum • View topic - demo question](http://forum.odroid.com/viewtopic.php?f=21&t=12314)

요게 2채널 영상 loopback 예제고.. 이걸 4채널로 만든게 자기들 꺼란다.. 써글. 코드 공개 할 것이지.. 이래라저래라.. 씨발.

[neuralassembly / SimpleDoubleWebCams2 / source / src / com / camera / simpledoublewebcams2 / CameraPreview.java — Bitbucket](https://bitbucket.org/neuralassembly/simpledoublewebcams2/src/3ba4b383f53338e161d36ceb007f5789662be35c/src/com/camera/simpledoublewebcams2/CameraPreview.java?at=master)
[ODROID | Hardkernel](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G137517754892)
[一箩筐 UVC 摄像头 | dword 的狗窝](http://blog.dword1511.info/?p=3405)
[DriverGuide - Sonix Inc. Windows Driver Download (Page 1)](http://list.driverguide.com/list/company2695/)
[ffmpeg stops when open the audio input. • FFmpeg Support Forum](http://ffmpeg.gusari.org/viewtopic.php?f=12&t=1920)
[UVC Wand « Got Bed Bugs? Bedbugger Forums](http://bedbugger.com/forum/topic/uvc-wand)
[Linux Projects - News](http://www.linux-projects.org/modules/news/)
[android development "like linux" - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=android+development+%22like+linux%22)
[Index of /5422/ODROID-XU3/Android/4.4.2_Alpha_1.0_Aug-13-2014](http://dn.odroid.com/5422/ODROID-XU3/Android/4.4.2_Alpha_1.0_Aug-13-2014/)
[ODROID-X2 Android 4.1 Beta-1.6 image file for SD/eMMC.](http://com.odroid.com/sigong/nf_file_board/nfile_board_view.php?keyword=&tag=&bid=182)
[cannot boot from sd odroid - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=cannot%20boot%20from%20sd%20odroid&es_th=1)
[ODROID Forum • View topic - [Solved] Can't boot the XU3-Lite](http://forum.odroid.com/viewtopic.php?f=93&t=7793)
[ODROID Forum • View topic - eMMC Recovery Tool for XU3](http://forum.odroid.com/viewtopic.php?f=53&t=6173)

######2015Jul05 23:53:35+0900

결국.. 이들의 카메라를 리눅스를 제대로 지원하지 않는 것으로 판명... / 안드로이드도 제대로 지원안할 것이다 보나마나. 결국 사기. 개새끼들이다.

[ODROID | Hardkernel](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G137517754892)
[AOV view - Google Search](https://www.google.co.kr/search?q=AOV&oq=AOV&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=AOV+view)
[Angle of view - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Angle_of_view)
[SONIX SN9C259 - Google Search](https://www.google.co.kr/search?q=SONIX+SN9C259&oq=SONIX+SN9C259&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8)
[Lilliput UK · ODroid USB Camera 720P · Odroid USB Camera](http://www.lilliputuk.com/accessories/odroid-accessories/odroid-usb-camera-720p/)

다른 제품에 대한 설명으로 보이는데, 여튼 데이타시트를 찾을 수 없는 이 칩.. SONIX SN9C259에 대한 feature / 설명이 조금 들어있다.

[Notebook Camera Module](http://webcache.googleusercontent.com/search?q=cache:KRwhxUlqcXcJ:www.wingstek.com/Download/NBCM200USX.pdf+&cd=1&hl=en&ct=clnk&gl=kr)
[dealers.balticdata.lv/ProductImages/Descriptions/8717534015906.txt](http://dealers.balticdata.lv/ProductImages/Descriptions/8717534015906.txt)

```
2. DSP Description SONIX SN9C259 an Video Audio Controller provides a cost-effective solution forUSB2.0 video/image process application. This ASIC supports various CCD or CMOSimage sensor interface, image capture, processing and USB High Speed transmission,which integrates certified USB_IF UTMIPHY in HS/FS mode and Turbo 8051microcontroller. The hi-speed USB2.0 Plug & Play and USB1.1 backward compatibleASIC design provides OEM with the ability to customize product specification. SN9C259products offer high value and performance video imaging controller design due to theflexible architecture and compatibility. Features ● An Video and Audio control backend IC● Support VGA/1.3M/2M CCD and CMOS image sensor.● Support Microphone Audio output.● Bayer pattern / YUV data format.● Frame rate: VGA 30fps; Mega pixel 12fps (UVC uncompressed); 15fps ( Raw )● Up to 480Mb/s High speed USB2.0 compliant device.● Backward compatible with full speed USB1.1 Host.● ISO video high-speed high bandwidth endpoint for 24MB/s transfer.● Support video and audio stream synchronization.● USB video class 1.1 Compliance. (For YUV sensor inputs only, N/A for sensor raw inputs).● Auto light exposure control.● Auto white balance control.● Color enhancement.● 16 segments gamma correction.● 256 Windows RGB average.● Auto Focus adjustment.● Auto Facial tracking.● Contrast enhancement.
```

여기 보면..
```
Frame rate: VGA 30fps; Mega pixel 12fps (UVC uncompressed); 15fps ( Raw )
...
USB video class 1.1 Compliance. (For YUV sensor inputs only, N/A for sensor raw inputs).
```
```
Compression chip    Sonix SN9C259
Technical features  Sensor SNR: 40 dB
Sensor dynamic range: 50 dB
Sensor responsivity: 0.6 V / lux sec
System requirements
Mac compatibility   
Compatible operating systems    Microsoft® Windows® XP (Service Pack 2 or newer), Windows Vista® or Windows® 7, Apple® Mac® OSX™
```
등등이 적혀있는데..

오드로이드측이 고시한 내용과는 많이 차이가 있다.. 물론 여러가지.. 생략된 내용도 있을 수 있고 그사이에 업그레이드도 있을 수 있다.
그렇지만.. UVC를 YUV 로만 지원하겠다는 얘기는 굉장히 문제 상황에 딱 들어맞는다.
다음 링크에서 따온 해당 칩의 compatible operation system 에 리눅스는 없다.
uvc라고는 하지만, compatibility를 보장하지는 않은 것이다.
무책임하게, '어 되네. 그래 이걸로 하자. 값도 싸고.' 했을 것이다. 개 씨 발 새 끼 들.

니들 덕에 한달 조금 안되는 시간을 버렸다. 미친 개 새 끼 야.

---

**odroid SATA3-USB3**

[ODROID | Hardkernel](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G137448544866)

이걸로 한다 담에..