
####subject => ordoid에서 USB CAM 1채널 재생의 성능 보기 :

##### pthread -> USB bandwidth issue -> MJPG -> V4L2 -> OpenCV jpg decoding issue -> libjpeg -> hardware acceleration?
##### -> OpenGL ES 2.0 -> great to do gles2 but too musch time consuming for now. -> go back to libjpeg stage.

---

######2015Jun15 21:28:54+0900

지난번 마지막 로그.

*여튼 결론은 잘 안되더라는 얘기고.*
*오히려 긍정적인 방향은 odroid에서 작업하기 전의 바닐라 커널에다가 패치를 하고, odroid 변경 내용을 diff로 patch로 만들어서 RT 설정이후에다가 패치를 적용한다.*
*근데 분명히 충돌이 날텐데.. 어떤걸 오버라이트하고 어떤걸 스킵할지 나는 알 수가 없으니..*
*이방법은 거의 불가능할 것 같다는게 결론이다.*

* 그러니깐. Xenomai로 기존 커널 (PREEMPT 까지 적용되어있으니 나쁘지 않다.)을 올리거나.
* 기존 커널에서 priority를 50이하 49로 줘서 코드를 짜서 테스트해보고, pthread를 적용해서 성능이 나오는지 보는 2가지 방향이 있다.
* 또는 2가지 다 해보는 3번째 방법까지 3가지 방법을 생각할 수 있다.

-

priority 49를 한번 해보면 좋겠는데..
현재.. 커널 task의 priority를 어떻게 조회할 수 있나? 그것 부터 알아보고..
그담엔 한번 적용해보고.. pthread도 해보자.

-

**nice : priority manipulation**

nice 란 명령어를 찾았는데

nice ./capturetest 로 테스트 해본결과
top으로 확인해보면 즉각적으로 priority를 10으로 조정이 가능했다.
커널 태스크는 20.

최저 fps는 9.5 정도인데.. 이게 나타나는 횟수가 nice 적용한 경우 횟수는 줄어들지만, 들락날락 하는 것은 변화가 없다.

-

즉, 굳이 코드를 짜서 테스트 할 필요가 없어졌다. 해도 개선 안된다.
다음은 pthread로 worker thread로 해서 돌려보는 방법인데.. 별로 기대감은 없지만 한번 해볼 여지는 있음.

---

**pthread**

pthread를 이용해서 비디오 캡쳐를 하고 이를 다시 화면에 뿌려주는 경우를 찾았다.
<http://stackoverflow.com/questions/27333498/opencv-with-pthreads-and-mutexes/27335918#27335918>
이 코드를 참고하면서 profiling 코드를 약간 삽입해서, 성능을 봤다.

그 결과가 아래 코드..
fishmans-odroid 란 이름으로 git를 셋업하고 odroid와 mac에서 씽크하였다.
<https://bitbucket.org/dooho_yi/fishmans-odroid>

**git를 CLI에서** 쓰는 게 안익숙해서 좀 찾아봐야했는데.
기본적으로 다음과 같이 하면 된다.

(bitbucket/github에서 repo를 만들고 이를 로컬에 클론.)
웹에서 주는 명령어를 그대로 실행하면 된다. git clone...

그리고 나서, 바로 pull/push를 하면 잘 안된다.
왠지 몰라도 안되드라..
그래도 hi.txt 파일 같은걸 그냥 하나 만들어주면 잘 되드라. 최초 push 할 때는 내용이 있어야 하는 것 같다. (이건 mac에서 sourcetree로 했음.)

그걸 다시 odroid에서 클론한 다음에..config에.. user.name user.email 정보를 넣고.

push/pull하려고 했는데 잘 안되드라.

add/commit을 해야 하는데..

add는 **"git add -all"** 이라고 하면 변경사항 전부다 stage가 되고.
commit은 **"git commit -m hello"** 이런식으로 하면 되드라.
그런 담에 push 하면 ok.

---

그래서 아래의 코드가 싱크가 되었는데.

```cpp
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <pthread.h>
#include <time.h>
#include <string>

using namespace cv;
using namespace std;

//GLOBALS
VideoCapture vcap;
Mat frame;
pthread_mutex_t frameLocker;

void *UpdateFrame(void *arg)
{
  for(;;)
  {
    Mat tempFrame;
    vcap >> tempFrame;

    pthread_mutex_lock(&frameLocker);
    frame = tempFrame;
    pthread_mutex_unlock(&frameLocker);
  }
}

int main(int argc, char* argv[]) {

  char caStr[100];
  int c;
  double mavg_buf[10];
  double mavg;
  double peak = 0;
  int idx = 0;
  int cnt = 0;

  if (argc > 1)
  {
    vcap.open(string(argv[1]));
  }
  else
  {
    vcap.open(CV_CAP_ANY);
  }

  // profiling
  struct timespec start, end;
  clock_gettime( CLOCK_REALTIME, &start );

  pthread_mutex_init(&frameLocker,NULL);
  pthread_t UpdThread;
  pthread_create(&UpdThread, NULL, UpdateFrame, NULL);

  for(;;)
  {
    Mat currentFrame;
    pthread_mutex_lock(&frameLocker);
    currentFrame = frame;
    pthread_mutex_unlock(&frameLocker);

    if(currentFrame.empty()){
      printf("recieved empty frame\n");
      continue;
    }

    // measure capture + show - end.
    clock_gettime( CLOCK_REALTIME, &end );
    double difference = (end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/1000000000.0d;
    //cout << "It took " << difference << " seconds to process " << framecnt << " frames" << endl;
    cout << "Capturing and processing " << 1.0/difference << " frames per second " << endl;
    idx++;
    idx = idx % 10;
    mavg_buf[idx] = 1.0/difference;
    double sum = 0;
    for (int ii = 0; ii < 10; ii++)
      sum = sum + mavg_buf[ii];
    mavg = sum/10.0;
    sprintf(caStr, "fps(mavg) : %5.1f", mavg);
    putText(currentFrame, string(caStr), Point(320, 120), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 255), 2);
    //
    cnt++;
    if (cnt > 1000)
    {
      if (difference > peak)
      {
        peak = difference;
      }
      sprintf(caStr, "fps(peak) : %5.1f", 1.0/peak);
      putText(currentFrame, string(caStr), Point(320, 360), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 255), 2);
    }

    // exit condition - press ESC.
    c= waitKey(1);
    if (c == 27)
      break;

    // measure capture + show - start.
    clock_gettime( CLOCK_REALTIME, &start );

    //
    imshow("Output Window", currentFrame);
  }
}

```

---

결과는.. 일단은 좋은데.. 좀 믿을 수 없는 구석이 있어서 분석이 좀 필요하다.

10번 해서 moving average한 fps가 200 정도되고..
peak로 가장 느린 경우가 150 fps 보다 크다.(빠르다.)

말이 안되지..

그래서 생각해 볼 필요가 있다.

-

음. 일단. 지금 현재 프로파일링이 drawing loop에 걸려있다. 여기에 mutex로 capture 쓰레드가 물려있다.
운이 좋아 mutex가 작동이 되었을 경우에는.. 측정치가 맞아지겠지만.
capture 쓰레드가 아직 capture 중인 경우일 때는.. drawing thread가 그냥 바로 mutex를 얻고 그림을 그려버리고. profiling되기 때문에.. 제대로 된 측정이라고 보기 어려울 수 있다.

-

전반적으로 속도가 빨라지는 이유는.. 그림을 그리는 중에, 캡쳐를 진행할 수 있기 때문일 것이다. 그렇다고 해도 13프레임 정도였던게.. 갑자기 200 프레임이 되는 건 뭔가 정상은 아니지... 10배이상이 빨라졌다..

-

가능하면 capture 쓰레드에도 따로 프로파일을 걸거나..

예를 들면 capture 에서 profiling start를 찍고, drawing에서 end를 찍어서 재는 걸로 하는 것도 좋겠다.

-

하지만, 그렇다고해도, 여전히 peak 값이 fps 150 이상이기 때문에 상당히 빠르다고 봐야 할 수 도 있다.

즉, 1ms도 안걸린다는 말이다.(=> 계산 잘못했넹. 그게 아니고 10ms도 안된다는 얘기.. 200 fps 면. 5ms. 7.5ms 쯤 되겠네..) 이렇게 되면 CAN쓰레드의 timestamp 해상도보다도 더 빠르다는 얘기다. (=> 아니다. 계산 맞게 하면.)

여전히 미심쩍음을 지울수 없게 하는 부분이있다... => **4채널을 해서..** 확인해야..

**정상적으론 capture 쓰레드에서 프로파일링을 해야...**

-

150 fps로 1채널을 할 수 있다면.
4채널 하는 경우엔 (해봐야 알겠지만.)
150/4 = 35.x fps를 4채널 캡쳐할 능력이 있다는 의미이다.

-

결국 pthread만 적용하면 이렇게 빠르게 작동하게 할 수 있게 된다고 요약할 수 있겠다.

-

(관련 사진)

(영상)

-

근데, delay부분은 역시 해결이 안된다.. 영상에서 보면 커서를 움직이면서 loop된 영상에서 커서가 움직이는 것 까지를 관찰해보면.
delay가 있음을 알 수가 있다.

0.1초 정도 되는 것 같은데.. 해결이 안될 것 같기도 하공..

-

다음으로는 4채널을 받아서, 화면에 뿌려주는 것 까지 해보도록 하자.

---

**pthread cmake**

<http://stackoverflow.com/questions/1620918/cmake-and-libpthread/4774027#4774027>

pthread를 쓰기 위해서 cmake에 변경이 필요한데.
```
cmake_minimum_required (VERSION 2.6)
find_package (Threads)
add_executable (myapp main.cpp ...)
target_link_libraries (myapp ${CMAKE_THREAD_LIBS_INIT})
```

이와 같은 식이다.. find_package (Threads) 하고.. target_link_libraries (myapp ${CMAKE_THREAD_LIBS_INIT}) 하면 됨.

cmake 파일도 수정해야 했다.

```
cmake_minimum_required( VERSION 2.8 )
#project( capture-thread-test C CXX )
project( capture-thread-test )
find_package( Threads )
find_package( OpenCV REQUIRED )
add_executable( capture-thread-test capture-thread-test.cpp )
target_link_libraries( capture-thread-test ${CMAKE_THREAD_LIBS_INIT} ${OpenCV_LIBS} )

```

위에가 현재 상태임.

-

**c와 cpp 가 섞여있는 프로젝트의 경우**

<http://stackoverflow.com/questions/15193785/how-to-get-cmake-to-recognize-pthread-on-ubuntu/15193877#15193877>

이게 필요하진 않았었지만.. 참고로 알고 있으면 좋겠다.

c 라이브러리를 cpp 에서 쓴다던가 하는 경우.. 문제가 생기면 이런 처리 방법도 검토해보자.

질문이..
```
If I compile on the command-line with g++ directly, I can see everything I need is there:

$ g++ -pthread test.cpp
$ ldd a.out
    linux-vdso.so.1 =>  (0x00007fffd05b3000)
    libstdc++.so.6 => /usr/lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f4a1ba8d000)
    libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f4a1b870000)
    ...more...
Then I try to create a simple cmake file for this 5-line test app:

$ cat CMakeLists.txt
PROJECT ( Test CXX )
CMAKE_MINIMUM_REQUIRED ( VERSION 2.8 )
FIND_PACKAGE ( Threads REQUIRED )
ADD_EXECUTABLE ( test test.cpp )
TARGET_LINK_LIBRARIES ( test ${CMAKE_THREAD_LIBS_INIT} )
However, I cannot figure out why CMake doesn't find what it needs to use for Threads:

$ cd build/
$ cmake ..
CMake Error at /usr/share/cmake-2.8/Modules/FindPackageHandleStandardArgs.cmake:97 (MESSAGE):
  Could NOT find Threads (missing: Threads_FOUND)
Call Stack (most recent call first):
  /usr/share/cmake-2.8/Modules/FindPackageHandleStandardArgs.cmake:288 (_FPHSA_FAILURE_MESSAGE)
  /usr/share/cmake-2.8/Modules/FindThreads.cmake:166 (FIND_PACKAGE_HANDLE_STANDARD_ARGS)
  CMakeLists.txt:4 (FIND_PACKAGE)
-- Configuring incomplete, errors occurred!
```
답이..
```
Oh, this was was a pain! I probably lost 2 hours on this. Here is the solution:

CMake uses short 'C' applications to test/try things. If the CMakeLists.txt states that C++ is used for the project, without also listing C, then some of those shorts tests incorrectly fail, and cmake then thinks those things aren't found.

The solution was to change the first line of CMakeLists from this:

PROJECT ( Test CXX )
...to include C as a language:

PROJECT ( Test C CXX )
Then delete build, recreate it, and everything then works:

rm -rf build
mkdir build
cd build
cmake ..
```

---

**uvcvideo module configuration to resolve issues for multiple USB camera capture on linux**

**lagging issue**

딱히 프레임을 잃어버리는 것도 아니고.
캡쳐하는데 걸리는 시간이 그렇게 긴것도 아님. (profiling에서 측정한 capture 시간은 대략 10ms 이내, 즉 이정도면 사람이 보기에 아무 이상을 느낄 수 없을 정도여야 함)

근데도, 왜 눈에 띌 정도의 lag가 존재할까? => buffer가 완전히 비워지지 않았기 때문일 꺼라는 추측.
즉, 장치의 버퍼에 쌓여있는 시간이 좀 된, 이미지들이 순차적으로 전송되어 오기 때문이 아니겠느냐.

wifi / lan등으로 스트리밍을 하는 경우에는. 정말 그럴 수도 있겠다는 생각이 든다. 대부분 이런식의 문제제기를 하는 사람들의 경우에는 그런 셋업을 가지고 있는 사람들이 대부분인 것 같다. => 만약 그렇다고 한다면, 그리고 그렇게 해서 문제가 해결이 가능해진다고 하면. 시스템은 분할 할 수 있는 가능성이 생기므로, 이것도 나쁘지 않다. 주목해 볼 일이다. / 하지만, 일단은 1 시스템에서 모든 기능을 커버할 수 있을것인지를 조사하고 있는 와중이므로, 기록만 해 놓고 넘어가도록 하자.

그리고, 참고로.. 하는 말인데.. skype에서 카메라 테스트를 해보면. 비록 영상 해상도가 조금 낮기는 하지만, 거의 lag가 없이 거울 처럼 움직이는 결과를 볼 수가 있으므로, 기술적으로 불가능한 것은 아니라는 얘기가 된다. 쉽게 희망을 버릴 수 없게 만드는 지점이다.

그렇지만, vlc player에서 카메라를 열어봤을 때는.. 0.3초는 되는 lag가 감지 된다.

skype가 어떻게 하고 있는지를 알면 얘기가 빠르겠지만, 이 개새끼는 open source가 아니므로.. 존나 재쳐둬야 한다.

이 쪽으로 계속 조사를 한다면, openframeworks 에서 구현해서 한번 보는 것도 좋을 것 같다. 왜냐면, of에서 opencv도 지원을 하고 있고.. opencv의 한계라고 하면 opengl을 통해서도 가능할 것이고.. 어떤 방법으로 하면 되는지 찾아내는데 있어서.. 여러가지 옵션에 손쉽게 접근할 수 있을 가능성이 높으므로..

링크들을 차분차분하게 정리할 여력이 없으므로 그냥 관련 링크로 쏟아놓도록 하겠다.

<http://stackoverflow.com/questions/30032063/opencv-videocapture-lag-due-to-the-capture-buffer>
<http://answers.opencv.org/question/29957/highguivideocapture-buffer-introducing-lag/>
<http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-grab>
<http://www.chiefdelphi.com/forums/showthread.php?t=123390>
<http://stackoverflow.com/questions/17411490/opencv-videocapture-reading-issue>
<http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-set>
<https://forum.videolan.org/viewtopic.php?t=68062>
<http://workingwithcomputervision.blogspot.kr/2012/06/issues-with-opencv-and-rtsp.html>
<http://workingwithcomputervision.blogspot.co.uk/2012/07/weve-finally-got-it-going-ill-get-onto.html>

-

**bandwidth(?) or bandwidth declaration issue**

여러대의 카메라를 동시에 사용할 때 작동이 안되는 문제를 만나게 되었는데.. 표면적으로는 USB의 bandwidth의 문제이지만, 사실은 실제로 그렇지는 않을 수도 있다고 한다. uvcvideo라는 커널 모듈 (uvc : USB video capture)을 사용하게 되는데.. 이것이 쓰고 있는게.. v4l2 라고 하는데 (video for linux) .. 여튼 둘중에 정확히 뭔지는 몰라도. 이들은 실제로 bandwidth를 측정하거나 관리하고 있지는 않고. 장치에서 '선언'하는 요구 bandwidth를 기준으로 판단하기 때문에. 장치에서 헛으로 선언을 하면 실제로는 아무 문제 없는대로 안될 수가 있다고 한다.

실제로 odroid의 경우에는
<http://forum.odroid.com/viewtopic.php?f=10&t=1074#p17799>
```
rmmod uvcvideo
modprobe uvcvideo quirks=128
```
라고 하라고 포럼에서 가이드하고 있는데.. 이렇게 하니 되긴 됐더라.
quirks 라는 옵션은 일종의 해킹인데.. 어떤어떤 조건을 무시하고 작동하게 하는 것이다. quirk에 대한 리스트는 다음에 있다.
<http://www.ideasonboard.org/uvc/faq/>

이렇게 해도 select timeout이라던가.. no space left on the device 등의 에러가 나타났고.
<https://www.raspberrypi.org/forums/viewtopic.php?t=35689&p=300710>
<http://stackoverflow.com/questions/11394712/libv4l2-error-turning-on-stream-no-space-left-on-device>

<http://superuser.com/questions/431759/using-multiple-usb-webcams-in-linux/433237#433237>

이 qa에서는 이미지 전송 포맷을 바꾸라는 말도 있는데..
지금은 yuv 형식으로 되어있다. 즉 uncompressed 포맷이다. 속도는 빠르고, 압축이 높은 경우이다.

실제로.. usb 허브를 다르게 써서 bw를 확보하란 충고도 있고. 그렇다.. 지금 쓰고 있는 카메라 경우에는 MJPG를 지원한다.
이렇게 되면 날라오는 데이터가 MJPG으로 압축이 되어서 날라올 것이기 때문에.. bw를 많이 확보할 수 있게 된다.

압축되어서 오면 압축을 또 풀어줘야 하는데? => opencv에서 자동으로 압축을 풀어주게 될 것이라고 한다.

이것을 시도해보려고 했었는데.
<http://answers.opencv.org/question/6805/how-to-get-mjpeg-compression-format-from-webcam/>
<http://answers.opencv.org/question/26608/camera-input-video-compression/>

MJPG 라고 적어서 보냈는데, 그런 포맷이 없다고 나왔다 헐. v4l2로 지원 포맷을 찍어보면 분명히 mjpg이 좌르르 나오는데 ㅡㅡ; 뭔일인지 모르겠다. 분명히 해결방법이 있을 것이다. 시간이 없어서 더 진행 못했었다.

-

bw가 문제 이기 때문에.. usb 3.0으로 하면 어떨까 하는 생각도 든다.
<http://answers.opencv.org/question/4909/using-2-webcams-simultaneously/>
근데, 여기서는 그래봤자 카메라가 3.0으로 작동안하기 때문에 소용없다고 한다.
허브를 다르게 쓰라고 함.. 근데 허브를 더 달수가 없는 경우인데 나는. ㅡㅡ; 흠.
그리고, MJPG을 해보라고 함.
그리고, guvcview 가 잘한다고. 참고하라고 하는데.. 난 그거 좀 넘 느렸던거 같은데 lag.. 모르겠넹. 여튼, open source니까 참고 할수 도 있겠지.

-

그외에 참고 링크는 그냥 나열.

<http://forum.odroid.com/viewtopic.php?f=82&t=6728>
<http://askubuntu.com/questions/501800/cannot-open-2-usb-cameras-simultaneously-on-vlc-player-ubuntu-12-04>
<http://linuxtv.org/wiki/index.php/UVC_Webcam_Devices>
<http://stackoverflow.com/questions/7039575/how-to-set-camera-fps-in-opencv-cv-cap-prop-fps-is-a-fake> : 먼가 opencv에서 해당함수를 짜가로 지원하니까 내부 함수 icv- 를 추천해주는 경우다.
<http://answers.opencv.org/question/9659/solved-how-to-make-opencv-to-ask-libv4l-yuyv-pixelformat-instead-of-bgr24/>
<http://docs.opencv.org/2.4.2/doc/tutorials/highgui/video-write/video-write.html>

---

그외에.. beaglebone에서 작업하는 경우에 대한 (위에 나왔던 건데.. molly 아자씨.) <https://github.com/derekmolloy/boneCV/> 깃허브 소스인데.. 써먹기 좋다. <http://derekmolloy.ie/beaglebone/beaglebone-video-capture-and-image-processing-on-embedded-linux-using-opencv/> 여기 들어있는 영상도 참고하면 좋다. 좀 지루하긴 하지만, 작업하고 설명해주는 과정 참고.

---

**opencv fullscreen window @ linux**

opencv에서 전체 화면 하려고.. 좀 찾다가 말았음.

<http://docs.opencv.org/modules/highgui/doc/qt_new_functions.html?highlight=cv_window_fullscreen>
<http://www.dca.ufrn.br/~rafaelbg/opencv.php?view=setWindowProperty>
<http://stackoverflow.com/questions/7380114/opencv-fullscreen-window>

---

######2015Jun19 18:06:52+0900

오우.. 엄청 덥넹..

-

일을 좀 해서.. feasibility를 결론은 내야 할 것 같다. 오늘 / 내일까지.

-

그럴라면. 일단 시스템을 셋업을 해야겠지?

---

######2015Jun20 04:25:46+0900

**MJPG issue**

일단, emacs를 배우는데 시간을 마니 보냈다.. 이제 뭐 그럭저럭 쓸 수는 있게 되었는데.. 역시. 뭐.. 진행된건 아니고..

-

유사한 문제를 겪는 사람
<http://stackoverflow.com/questions/25003320/highgui-v4l-error-when-using-mjpeg-for-opencv>

-

MJPG이 안되는 문제에 대해서 좀 찾아본 내용은.. opencv에서 v4l2의 기능을 제대로 지원하지 않는다는 게 문제 인 듯하다.
그래서 v4l2로 직접 코딩을 하기도 한다. 사람들이..

---

######2015Jun20 11:33:55+0900

어제 하다가 걍 자버렸는뎅..

<http://gumstix.8.x6.nabble.com/OpenCV-Overo-Webcam-tt570332.html#a570357>
v4l2로 직접 코딩을 하는 방법도 있다고 하는데.. 그렇게 해서 어떤 설정 문제를 해결했다고 함.
<https://code.google.com/p/libv4l2cam/source/browse/trunk/libv4l2cam/libcam.cpp>
이 라이브러리를 썼다고 한다. 근데 이 라이브러리가 MJPG 용이 아니고 yuv용이어서. 난 못씀. ㅡㅡ; (아마도?)

-

v4l2로 직접코딩하는 방식을 몇몇.. 접근하는데..
<http://www.jayrambhia.com/blog/capture-v4l2/>
이걸 보니깐.. 이 사람은 다른 어떤 예제 코드를 가져다가 변용했다는데.. 뭔가 튜토리얼을 좀 쓰다만 느낌이고 작동되 되다 마는 것 같다.

여기서 봐둘거는.. mjpg을 opencv iplimage로 decode해오는 방법인데..
cvImageDecode 라는 함수가 있더라는 것.
<http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#imdecode>

-

여튼 v4l2로 직접 코딩을 하면 많은 문제가 해결된다고는 하는데 (되긴 되겠지 ㅡㅡ; 이견은 없음.)
본격적으로 직접 코딩을 하기 위한 튜토리얼은 여기에 있고.
<http://jwhsmith.net/2014/12/capturing-a-webcam-stream-using-v4l2/>

-

자, 그렇긴한데... 이걸 읽자니 깜깜.. 뭔가 되는 걸 먼저 보고 싶은데.. 공부해도 헛이면 어떡하지. 해서..
그리고, 정확하게 짜지 못하면.. 또 시간만 잡아먹고.
openframeworks에서 v4l2를 제대로 지원하는 코드가 들어있지 않을까?

<http://forum.openframeworks.cc/t/of-works-in-odroid-u2-well/13194>

openframeworks를 odroid에서 쓰는 거는 사람들이 좀 해본거 같고. 마지막 메세지인 xu3에서 안되더라는 것도.. 2월달의 얘기고.. 지금은 Mali 업데이트가 되었으니.. 시도해볼만하고.

-

어떨까? 직접 코딩을 바로 시작하는게 시간을 버는 일일까? 어차피 그렇게 해야 할 수도 있는뎅.
아님, openframeworks로 또 들어가는 게, 현명할까?
...

---

######2015Jun20 14:01:28+0900

**openframeworks @ odroid-xu3**

쩌브.. openframeworks를 해볼라고 마니 했는데...

<https://github.com/glfw/glfw/issues/31>
```
failed to retrieve context version string
```

요 에러에서 막혔다.
glfw를 새로 컴파일도 해봤는데. 소용없다. 일단 컴파일에 실패했기도 하고..

glfw에서 윈도우 생성을 안해주는데 뭐 할 수 있는게 없다.. 나중에 다시 봐야지..

---

**openframeworks에서 dynamic linked library를 못찾아서 runtime-error나는 경우**

bin/libs 에 해당 파일을 찾아서 (내경우엔 libGLESv1_CM.so.1 이었음) 복사해 넣어주면 돌아간다.

```
cp /usr/lib/arm-linux-gnueabihf/mesa-egl/libGLESv1_CM.so.1 .
```

mesa에서 so.1을 릴리즈를 하는데 이건 dev 패키지에 있는게 아니다. / runtime 패키지에서 찾아야한다!
<https://wiki.ubuntu.com/X/EGLDriverPackagingHOWTO>
```
libgles1-mesa (containing /usr/lib/$DEB_HOST_MULTIARCH/mesa-egl/libGLESv1_CM.so.1)
```

-

so.1 이란 명칭은 runtime binding 되는 dynamic linked library를 말하는 것이다.

<http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html>
```
The link to /opt/lib/libctest.so allows the naming convention for the compile flag -lctest to work.
The link to /opt/lib/libctest.so.1 allows the run time binding to work. See dependency below.
```

여튼 이런저런 노력과 배움이있어지만, 안된다. 포기. 다음기회에 계속 해보던지..

-

```
2:22 PM odroid@odroid:~/of/examples/graphics/polygonExample$ make run
checking pkg-config libraries: cairo zlib gstreamer-app-1.0 gstreamer-1.0 gstreamer-video-1.0 gstreamer-base-1.0 libudev freetype2 fontconfig sndfile openal openssl libpulse-simple alsa gtk+-3.0 libmpg123 glesv1_cm glesv2 egl
./polygonExample: error while loading shared libraries: libGLESv1_CM.so.1: cannot open shared object file: No such file or directory
make: *** [run] Error 127
odroid@odroid:~/of/examples/graphics/polygonExample$ cd bin
odroid@odroid:~/of/examples/graphics/polygonExample/bin$ cd libs/
odroid@odroid:~/of/examples/graphics/polygonExample/bin/libs$ cp /usr/lib/arm-linux-gnueabihf/mesa-egl/libGLESv1_CM.so.1 .
odroid@odroid:~/of/examples/graphics/polygonExample/bin/libs$ cd ..
odroid@odroid:~/of/examples/graphics/polygonExample/bin$ cd ..
odroid@odroid:~/of/examples/graphics/polygonExample$ make run
checking pkg-config libraries: cairo zlib gstreamer-app-1.0 gstreamer-1.0 gstreamer-video-1.0 gstreamer-base-1.0 libudev freetype2 fontconfig sndfile openal openssl libpulse-simple alsa gtk+-3.0 libmpg123 glesv1_cm glesv2 egl
[ error ] ofAppGLFWWindow: 65544: Failed to retrieve context version string
[ error ] ofAppGLFWWindow: couldn't create GLFW window
Segmentation fault
make: *** [run] Error 139
```

---

######2015Jun20 18:37:39+0900

######2015Jun20 19:16:23+0900

아 시간 잘간다.. emacs는 좀 그만 봐야 할텐데..

-

그래서 지금은.. v4l2를 직접 해야지.

---

######2015Jun24 20:23:16+0900

지난 번에 정리안하고 chrome 죽은거.
대략.. **v4l로 직접 capture** 하는데 있어서 찾았던 것들인 거 같다.

<http://derekmolloy.ie/beaglebone/beaglebone-video-capture-and-image-processing-on-embedded-linux-using-opencv/>
<http://hansdegoede.livejournal.com/>
<http://jayconrod.com/posts/36/emacs-etags-a-quick-introduction>
<http://jwhsmith.net/2014/12/capturing-a-webcam-stream-using-v4l2/>
<http://linuxconfig.org/introduction-to-computer-vision-with-opencv-on-linux#h4-5-input-from-a-video-camera>
<http://linuxtv.org/wiki/index.php/Developer_Section>
<http://linuxtv.org/wiki/index.php/V4L2_Userspace_Library>
<http://linuxtv.org/wiki/index.php?search=VIDIOC&button=&title=Special%3ASearch>
<http://linuxtv.org/wiki/index.php?title=Special%3ASearch&profile=default&search=VIDIOC_DQBUF&fulltext=Search>
<http://localhost/dooho_yi.bitbucket.org/>
<http://people.atrpms.net/~hdegoede/>
<http://stackoverflow.com/questions/5815782/v4l2-invalid-argument-for-the-ioctl-of-dqbuf>
<http://stackoverflow.com/questions/7039575/how-to-set-camera-fps-in-opencv-cv-cap-prop-fps-is-a-fake>
<http://talk.radxa.com/topic/334/v4l2-issue>
<http://www.avsupply.com/ITM/21366/DFM%2042BUC03-ML.html>
<http://www.gnu.org/software/emacs/manual/html_node/eintr/etags.html>
<http://www.instructables.com/file/F6Y511OIAGMV40Z>
<http://www.theimagingsource.com/en_US/>
<http://www.theimagingsource.com/en_US/products/>
<http://www.theimagingsource.com/en_US/products/grabbers/>
<http://www.theimagingsource.com/en_US/products/grabbers/2>
<http://www.theimagingsource.com/en_US/products/oem-cameras/>
<http://www.theimagingsource.com/en_US/products/oem-cameras/usb-cmos-color/>
<http://www.theimagingsource.com/en_US/products/quotation/dfm42buc03ml/>
<https://github.com/TheImagingSource/tiscamera>
<https://github.com/TheImagingSource/tiscamera/issues/12>

---

######2015Jun24 20:26:18+0900

그 다음에 본것들..

[Capture images using V4L2 on Linux — Jay Rambhia](http://www.jayrambhia.com/blog/capture-v4l2/)
[Reading and Writing Images and Video — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html)

**restore closed tabs chrome crash + strings/regex extracting unique urls**

[restore closed tabs chrome crash superuser - Google Search](https://www.google.co.kr/search?q=restore+closed+tabs+chrome+crash&oq=restroe+closed+tab+crash&aqs=chrome.1.69i57j0.12611j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=restore+closed+tabs+chrome+crash+superuser)
[Strings](https://technet.microsoft.com/en-us/sysinternals/bb897439.aspx)
[User Data Directory - The Chromium Projects](http://www.chromium.org/user-experience/user-data-directory)
[regular expression starts with http - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=regular+expression+starts+with+http)
[Regex Tutorial - Start and End of String or Line Anchors](http://www.regular-expressions.info/anchors.html)
[RegexOne - Learn Regular Expressions - Lesson 10: Starting and ending](http://regexone.com/lesson/10)
[Regex to test if string begins with http:// or https:// - Stack Overflow](http://stackoverflow.com/questions/4643142/regex-to-test-if-string-begins-with-http-or-https)
[What Regex would capture everything from ' mark to the end of a line? - Stack Overflow](http://stackoverflow.com/questions/830855/what-regex-would-capture-everything-from-mark-to-the-end-of-a-line)
[java - Is there a not (!) operator in regexp? - Stack Overflow](http://stackoverflow.com/questions/3866279/is-there-a-not-operator-in-regexp)
[Regexp Example: Deleting Duplicate Lines or Items with Regular Expressions](http://www.regular-expressions.info/duplicatelines.html)

**odroid hardware research**

opencv에서 image show가 깜빡깜빡.. 됐다 안됐다 하는 문제가 있어서.. libjpeg 등으로 직접 decode 해볼까 했음.

libjpeg
libjpeg-turbo : IPP 사용
NEON
odroid 비디오 가속..
내장 video codec 하드웨어. -> 리눅스에서는 드라이버 없음. 병신 같은 놈들.

[Using OpenCV with gcc and CMake — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/doc/tutorials/introduction/linux_gcc_cmake/linux_gcc_cmake.html)
[corrupt jpeg data 4 extraneous bytes before marker 0x8a - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=corrupt%20jpeg%20data%204%20extraneous%20bytes%20before%20marker%200x8a&es_th=1)
[ZoneMinder + Foscam FI8918W. WAR Corrupt JPEG data: extraneous bytes before marker 0xd9 « Blog](http://lachlanmiskin.com/blog/2012/06/25/zoneminder-foscam-fi8918w-war-corrupt-jpeg-data-extraneous-bytes-before-marker-0xd9/)
[cvwaitkey - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=cvwaitkey)
[User Interface — OpenCV 2.4.11.0 documentation](http://docs.opencv.org/modules/highgui/doc/user_interface.html)
[How to Get MJPEG compression format from webcam! - OpenCV Q&A Forum](http://answers.opencv.org/question/6805/how-to-get-mjpeg-compression-format-from-webcam/)
[cse.unl.edu/~rpatrick/code/onelinksys.c](http://cse.unl.edu/~rpatrick/code/onelinksys.c)
[libjpeg-turbo odroid - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=libjpeg-turbo+odroid)
[ODROID Forum • View topic - XU3 - Arch Linux packages (Kodi - Retroarch & NEON pkgs)](http://forum.odroid.com/viewtopic.php?f=96&t=8334)
[libjpeg-turbo | Documentation / Official Binaries](http://www.libjpeg-turbo.org/Documentation/OfficialBinaries)
[libjpeg-turbo | About / Performance](http://www.libjpeg-turbo.org/About/Performance)
[performance - Is there any ARM equivalent of Intel IPP? - Stack Overflow](http://stackoverflow.com/questions/9527881/is-there-any-arm-equivalent-of-intel-ipp)
[Mali-400 GPU's OpenCL support | ARM Connected Community](http://community.arm.com/message/18342)
[[DRAFT] How to Achieve 30 fps with BeagleBone Black, OpenCV, and Logitech C920 Webcam [DRAFT]](http://blog.lemoneerlabs.com/3rdParty/Darling_BBB_30fps_DRAFT.html)
[OMAP - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/OMAP)
[Samsung Exynos 5 Octa 5422 Specs, Reviews, Ratings](http://system-on-a-chip.specout.com/l/433/Samsung-Exynos-5-Octa-5422)
[neon exynos site:forum.odroid.com - Google Search](https://www.google.co.kr/search?q=JPEG+Hardware+Codec&oq=JPEG+Hardware+Codec&aqs=chrome..69i57&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=neon+exynos+site:forum.odroid.com)
[ODROID Forum • View topic - NEON vs VFP](http://forum.odroid.com/viewtopic.php?p=11838)
[ODROID Forum • View topic - Status of Hardware Accelerated Video Encoding under Linux?](http://forum.odroid.com/viewtopic.php?f=22&t=2357)
[ODROID Forum • View topic - HW video deconding with C1](http://forum.odroid.com/viewtopic.php?f=111&t=8819)

######2015Jun24 20:30:06+0900

**alternative (efficient/minimal) graphic possibilities @ linux machine**

libjpeg 하려다가 말고.. 다른 거 찾기 시작했음.
linux native gui..
directfb
wayland
glfw
gtk
xlib
evilwm
등등 두루두루 봤음.

[linux window api - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=linux+window+api)
[c++ - What is Linux’s native GUI API? - Stack Overflow](http://stackoverflow.com/questions/12717138/what-is-linux-s-native-gui-api)
[gtk archlinux - Google Search](https://www.google.co.kr/search?q=gtk+archlinux&newwindow=1&espv=2&biw=1280&bih=711&source=lnms&sa=X&ei=QfmIVbbNPIX38QXQsIOYAQ&ved=0CAUQ_AUoAA&dpr=1)
[gtk linux - Google Search](https://www.google.co.kr/search?q=gtk+odroid&oq=gtk+odroid&aqs=chrome..69i57j0l5.1703j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=gtk+linux)
[Getting Started with GTK+: GTK+ 3 Reference Manual](https://developer.gnome.org/gtk3/stable/gtk-getting-started.html)
[GNOME Library - Users](https://help.gnome.org/users/)
[Wayland - ArchWiki](https://wiki.archlinux.org/index.php/Wayland)
[The Hello Wayland Tutorial | FLOSS & Cia](https://hdante.wordpress.com/2014/07/08/the-hello-wayland-tutorial/)
[Programming a Wayland Client](http://jan.newmarch.name/Wayland/ProgrammingClient/)
[GLFW - An OpenGL library](http://www.glfw.org/)
[glfw/glfw](https://github.com/glfw/glfw)
[opengl opencv equivalent - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=opengl+opencv+equivalent)
[Does opengl help in the display of an existing image](https://www.opengl.org/discussion_boards/showthread.php/181714-Does-opengl-help-in-the-display-of-an-existing-image)
[c++ - How to draw a 2D Image using OpenGL - Stack Overflow](http://stackoverflow.com/questions/25097178/how-to-draw-a-2d-image-using-opengl)
[Tutorial 03 - First Triangle](http://ogldev.atspace.co.uk/www/tutorial03/tutorial03.html)
[Tutorial 3 : Matrices | opengl-tutorial.org](http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/)
[gtk ubuntu package - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=gtk%20ubuntu%20package&es_th=1)
[gtk+3.0 package : Ubuntu](https://launchpad.net/ubuntu/+source/gtk+3.0)
[makefile.am - Google Search](https://www.google.co.kr/search?q=makefile.am&oq=makefile.am&aqs=chrome..69i57j0l5.493j0j4&sourceid=chrome&es_sm=122&ie=UTF-8)
[autotools - What are Makefile.am and Makefile.in? - Stack Overflow](http://stackoverflow.com/questions/2531827/what-are-makefile-am-and-makefile-in)
[abiword - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=abiword)
[AbiWord - Wikipedia, the free encyclopedia](https://en.wikipedia.org/?title=AbiWord)
[abiword 1.0 - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=abiword+1.0)
[List of widget toolkits - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/List_of_widget_toolkits#On_Unix.2C_under_the_X_Window_System)
[IUP - Portable User Interface](http://webserver2.tecgraf.puc-rio.br/iup/)
[Xlib programming tutorial: anatomy of the most basic Xlib program](http://tronche.com/gui/x/xlib-tutorial/2nd-program-anatomy.html)
[how to use x windows cpp - Google Search](https://www.google.co.kr/search?q=writing+configure.ac&oq=writing+configure.ac&aqs=chrome..69i57j0l2.7239j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=how+to+use+x+windows+cpp)
[Linux : Writing Our First X-Windows Application - C++ Tutorials | Dream.In.Code](http://www.dreamincode.net/forums/topic/166837-linux-writing-our-first-x-windows-application/)
[How do you create a window in Linux with C++? - Stack Overflow](http://stackoverflow.com/questions/11190925/how-do-you-create-a-window-in-linux-with-c)
[Autoconf](http://ftp.gnu.org/old-gnu/Manuals/autoconf-2.53/html_node/Writing-configure.ac.html)
[An X Window System tutorial (Part 6) - YouTube](https://www.youtube.com/watch?v=H8ZK_lqakSo)
[evilwm - Google Search](https://www.google.co.kr/search?q=evilwm&newwindow=1&espv=2&biw=1280&bih=711&source=lnms&tbm=isch&sa=X&ei=czmJVcrIFM7t8gXorrLAAQ&ved=0CAYQ_AUoAQ#imgrc=QdQc7ZN3Q8SMiM%253A%3B_BxiTpOBenF2oM%3Bhttp%253A%252F%252Fwww.linux.org.ru%252Fgallery%252FbigxAn9rR.jpg%3Bhttp%253A%252F%252Fwww.linux.org.ru%252Fgallery%252Fscreenshots%252F745906%3B1280%3B1024)
[evilwm - a minimalist window manager for the X Window System](http://www.6809.org.uk/evilwm/)
[icewm - Google Search](https://www.google.co.kr/search?q=icewm&newwindow=1&es_sm=122&tbm=isch&imgil=25kskdVjEJoz8M%253A%253BE3zxa391ors-2M%253Bhttp%25253A%25252F%25252Fpuppylinux.org%25252Fwikka%25252Ficewm&source=iu&pf=m&fir=25kskdVjEJoz8M%253A%252CE3zxa391ors-2M%252C_&biw=1280&bih=711&usg=__6n3NRwpX0ccwtNU6mlZF_p4eEdc%3D&ved=0CDIQyjc&ei=eTmJVYjfIcvt8AWtn6XoAQ#imgdii=RHEvAqrqmAlYkM%3A%3BRHEvAqrqmAlYkM%3A%3BJPRIsuQreWrwBM%3A&imgrc=RHEvAqrqmAlYkM%253A%3Btwt-U8n1xtPHfM%3Bhttp%253A%252F%252Fupload.wikimedia.org%252Fwikipedia%252Fcommons%252F2%252F27%252FIcewm-default.jpg%3Bhttps%253A%252F%252Fen.wikipedia.org%252F%253Ftitle%253DIceWM%3B800%3B600&usg=__6n3NRwpX0ccwtNU6mlZF_p4eEdc%3D)
[icewm](http://www.icewm.org/)
[window manager display manager - Google Search](https://www.google.co.kr/search?q=lightdm&oq=lightdm&aqs=chrome..69i57j0l5.1368j0j7&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=window+manager+display+manager)
[X display manager (program type) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/X_display_manager_(program_type))
[x11 - Terminology reconciliation: Display manager vs. session manager, Windowing system vs. Window manager - Unix & Linux Stack Exchange](http://unix.stackexchange.com/questions/156549/terminology-reconciliation-display-manager-vs-session-manager-windowing-syste)

---

######2015Jun24 20:30:44+0900

**directfb 예제를 찾아서 컴파일하는 과정에서 find_package로 안되는 경우에.. cmake 모듈을 깔아서 알아먹게 하는 방법 알게됨**

뭐.. 맘에 쏙드는 건 아니지만, 이 FindDIRECTFB.cmake라는 파일을 잘 분석해보면, 그냥 raw로 직접 연결시키는 방법도 나중엔 알게 되겠지. 일단 도움을 받아서 성공했으나.. directfb는 /dev/fb0 를 얻지 못해서 실행이 안되었음.
쩝.. 하려면.. 아마도 display manager 없이 console로 부트 해야 하는데.. 이건또.. init.d 에 대해서 잘 알아야 하는지 뭔지.. 잘 안됨.
쩝.. 맘에 안듬.. 지쳤음.

[directfb cmake - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=directfb%20cmake&es_th=1)
[CMake - Cross Platform Make](http://www.cmake.org/cmake/help/cmake2.6docs.html#command:include)
[FreeRDP/FindDirectFB.cmake at master · FreeRDP/FreeRDP](https://github.com/FreeRDP/FreeRDP/blob/master/cmake/FindDirectFB.cmake)
[CMake:How To Find Libraries - KitwarePublic](http://www.cmake.org/Wiki/CMake:How_To_Find_Libraries)
[FreeRDP/FindDirectFB.cmake at master · FreeRDP/FreeRDP](https://github.com/FreeRDP/FreeRDP/blob/master/cmake/FindDirectFB.cmake)
[find_path — CMake 3.0.2 Documentation](http://www.cmake.org/cmake/help/v3.0/command/find_path.html)
[ginga/FindDirectFB.cmake at master · jgrande/ginga](https://github.com/jgrande/ginga/blob/master/config/modules/FindDirectFB.cmake)
[CMake Tutorial | CMake](http://www.cmake.org/cmake-tutorial/)
[add_subdirectory — CMake 3.0.2 Documentation](http://www.cmake.org/cmake/help/v3.0/command/add_subdirectory.html)
[include_directories — CMake 3.0.2 Documentation](http://www.cmake.org/cmake/help/v3.0/command/include_directories.html)
[set — CMake 3.0.2 Documentation](http://www.cmake.org/cmake/help/v3.0/command/set.html)

######2015Jun24 20:31:13+0900

[The Hello Wayland Tutorial | FLOSS & Cia](https://hdante.wordpress.com/2014/07/08/the-hello-wayland-tutorial/)
[hdante/hello_wayland](https://github.com/hdante/hello_wayland)
[directfb example code - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=directfb+example+code)
[DirectFB Tutorials](http://directfb.org/docs/DirectFB_Tutorials/image.html)
[Linux Changing Run Levels](http://www.cyberciti.biz/tips/linux-changing-run-levels.html)
[init - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Init)
[init 1 kills USB keyboard? | UDOO Forum](http://udoo.org/forum/threads/init-1-kills-usb-keyboard.2216/)

directfb에 대한 여러가지.. 알게 된 것들.

dfb 라는 옵션을 command line에 주면 dfb 의 동작을 나름 새로 컴파일 하지 않고.. 조종할 수 있다고...

[directfb command line options dfb - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=directfb+command+line+options+dfb)
[Man page of DIRECTFBRC](http://directfb.org/docs/directfbrc.5.html)
[www.weathergraphics.com/dl/wxchart.pdf](http://www.weathergraphics.com/dl/wxchart.pdf)
[dev/fb linux - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=dev%2Ffb+linux)
[X Forwarding (SSH, The Secure Shell: The Definitive Guide)](http://docstore.mik.ua/orelly/networking_2ndEd/ssh/ch09_03.htm)

**emacs 에 color theme 어떻게 하는지 알게 되었음**

color-theme이라는 패키지(?)를 깔아서 해보기도 했는데.. 영 저급한거 같고..
아래 링크에서 제공된 zenburn은 그나마 쓸만하고 그외에 기본 테마 들이 좋음..

[bbatsov/zenburn-emacs](https://github.com/bbatsov/zenburn-emacs)
[GNU Emacs Manual: Init File](http://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html)
[GNU Emacs Manual: Init Syntax](http://www.gnu.org/software/emacs/manual/html_node/emacs/Init-Syntax.html#Init-Syntax)
[Mac Classic Theme - Emacs Themes](http://emacsthemes.caisah.info/mac-classic-theme/)
[EmacsWiki: Color And Custom Themes](http://www.emacswiki.org/emacs?action=browse;oldid=ColorTheme;id=ColorAndCustomThemes)

아래에서 윈도우의 consolas에 해당하는 open source 폰트. inconsolata 를 깔고..
terminal 설정 창에서 bold / inconsolata 14 정도로 설정하고.. 터미널에서 바로 사용하면 emacs -nw ..
terminal 에서도 꽤 괜찮은 look을 볼 수 있으나... 막상해보고 나서는.. 뭐.. gui로 (윈도우 매니저가 돌고 있는 상황에서는 더욱) 사용이 가능한데. 더 깔끔하고 말야.. 굳이 이렇게 쓸필요가 있나 싶어서.. 쩝.

[Top 10 Programming Fonts](http://hivelogic.com/articles/top-10-programming-fonts)
[Inconsolata](http://www.levien.com/type/myfonts/inconsolata.html)
[Download | proggy fonts](http://www.proggyfonts.net/download/)
[Setting up Ubuntu to use inconsolata - Pixelite](http://www.pixelite.co.nz/article/setting-ubuntu-use-inconsolata/)

---

######2015Jun24 20:32:13+0900

지금까지 있었던 일들 그럭저럭 요점만 정리했다.

저녁을 먹고 온다.

-

뭔가 크게 망가진 일정과 마음을 추스리고..

잘 해봐야 할텐데.

다음 방향은 뭘까..

어쨌든 새로운 파일로 넘어가자.

---

######2015Jun24 20:57:54+0900

SOIL 이라는 라이브러리도 있었다!

**Simple OpenGL Image Library (SOIL)**

opengl을 뭔가 간단하게 쓸 수 있게 해주는 거라고 하니... 관심.
잊지말고 체크.

---

######2015Jun25 15:18:49+0900

SOIL은 해봤고.. 음 그냥 그렇다..
그리고, 중요한 사실이.. glmark2 라는 벤치마크를 odroid xu3 mali 드라이버 관련 thread에서 알게 되었는데..
현재 odroid에서 실행하면, 정말 그지 같은 결과가 나오고 에러가 완전 마니 남.
apt-get으로 설치를 해서 해봤는데.. 그렇고.. 그런데.. esglmark2 였나.. glmark2-es 였나.. 여튼 EGL용이 따로 있더라.
찾아보니까 깔려있더라구..
그걸로 해보니까, 고성능. 나오더라. 64점 나왔다. 아까는 4점 나왔고..
그래서 확실히 알게 된게..
EGL을 써야만 한다는 것.
opengl을 그냥 써서는 안되고.. EGL을 써야하마.. Embedded GL. 말이다.

**emacs에서 테마 바꾸고.. 메일 보내고 하는 것 등등 재밌는 것들..**

[emacs white background - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=emacs+white+background)
[Emacs: How to Set a Color Theme](http://ergoemacs.org/emacs/emacs_playing_with_color_theme.html)
[homepages.inf.ed.ac.uk/s0243221/emacs/](http://homepages.inf.ed.ac.uk/s0243221/emacs/)
[Emacs basics: Changing the background color - sacha chua :: living an awesome life](http://sachachua.com/blog/2009/01/emacs-basics-changing-the-background-color/)
[Configuring the appearance of Putty](http://simms-teach.com/howtos/106-config-putty.html)
[emacs load theme cli - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=emacs%20load%20theme%20cli&es_th=1)
[bbatsov/zenburn-emacs](https://github.com/bbatsov/zenburn-emacs)

[user mail address emacs - Google Search](https://www.google.co.kr/search?q=abbrev&oq=abbrev&aqs=chrome..69i57j0l5.1232j0j7&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=user+mail+address+emacs)
[GNU Emacs Manual: Mail Sending](ftp://ftp.gnu.org/old-gnu/Manuals/emacs/html_node/emacs_368.html#SEC368)
[GNU Emacs Manual: Init Examples](http://www.gnu.org/software/emacs/manual/html_node/emacs/Init-Examples.html)
[GNU Emacs Manual: Sending Mail](http://www.gnu.org/software/emacs/manual/html_node/emacs/Sending-Mail.html)
[Open Call | Transformaking 2015](http://transformaking.org/opencall)

**Ortho/Perspective를 2D pixel perfect로 설정하는 방법, opengl**

[NeHe Productions: Lines, Antialiasing, Timing, Ortho View And Simple Sounds](http://nehe.gamedev.net/tutorial/lines,_antialiasing,_timing,_ortho_view_and_simple_sounds/17003/)

위에 링크 참 괜찮다. 책도 많이 쓰신듯. 예제로 주어진 겜도 재밌고.. 2000년도 작품인데도 괜찮음. 여기서 ortho / perspective / model 설정하는 게 나와있긴한데.. linux 포팅 예제는 안돌더라.
EGL로 해볼 필요가 있다.

[ASCII code Space, American Standard Code for Information Interchange, The complete ASCII table, characters,letters, vowels with accents, consonants, signs, symbols, numbers space,ascii,32, ascii art, ascii table, code ascii, ascii character, ascii text, ascii chart, ascii characters, ascii codes, characters, codes, tables, symbols, list, alt, keys, keyboard, spelling, control, printable, extended, letters, epistles, handwriting, scripts, lettering, majuscules, capitals, minuscules, lower, case, small, acute, accent, sharp, engrave, diaresis, circumflex, tilde, cedilla, anillo, circlet, eñe, enie, arroba, pound, sterling, cent, type, write, spell, spanish, english, notebooks, laptops, ascii, asci, asccii, asqui, askii, aski, aschi, aschii,20150624](http://www.theasciicode.com.ar/ascii-printable-characters/space-ascii-code-32.html)
[cmake modules linux - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=cmake+modules+linux)
[cmakemodules(1) - Linux man page](http://linux.die.net/man/1/cmakemodules)

cmake도 약간더 공부함.
man cmakemodules 라고 하면.. 가용한 모듈의 리스트를 알수 있더라.

---

**EGL + 2D @ odroid**

```
ortho / perspective / model 설정하는 게 나와있긴한데.. linux 포팅 예제는 안돌더라.
EGL로 해볼 필요가 있다.
```

[NeHe Productions: Lines, Antialiasing, Timing, Ortho View And Simple Sounds](http://nehe.gamedev.net/tutorial/lines,_antialiasing,_timing,_ortho_view_and_simple_sounds/17003/)
[GLX - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/GLX)
[Installing OpenGL/Glut libraries in Ubuntu](http://kiwwito.com/installing-opengl-glut-libraries-in-ubuntu/)
[NeHe Productions: Lesson 06 Texturing Update](http://nehe.gamedev.net/tutorial/lesson_06_texturing_update/47002/)
[How to link STL in c++ code? - Stack Overflow](http://stackoverflow.com/questions/8243051/how-to-link-stl-in-c-code)
[OpenGL Utility Toolkit - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/OpenGL_Utility_Toolkit)
[libGL error: MESA-LOADER - Google 검색](https://www.google.co.kr/search?q=libGL+error%3A+MESA-LOADER&oq=libGL+error%3A+MESA-LOADER&aqs=chrome..69i57.219542j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8)
[ODROID Forum • View topic - xbmc doesn't run after update ubuntu to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4652)
[ODROID Forum • View topic - Experience on upgrading Xubuntu 13.10 to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4628&p=37134#p37134)
[ODROID Forum • View topic - [TOOL] ODROID-Utility](http://forum.odroid.com/viewtopic.php?f=55&t=5053)
[ODROID Forum • View topic - Experience on upgrading Xubuntu 13.10 to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4628&p=37134#p37134)
[Mali binary driver - linux-sunxi.org](http://linux-sunxi.org/Mali_binary_driver)

exynos_dri.so 못찾는다는 에러가 나오는데.. 이건 암튼 정상이 아니다. 원래 fbdev는 커널에 들어있다고 하는데.. 이건 무시하면 안됨.
glmark2를 EGL 기반으로 돌렸을때는 이런 에러가 없고 성능이 개 좋아진다.
[exynos_dri - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=exynos_dri)
[site:http://forum.odroid.com/ mali driver "xu3" - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=site:http:%2F%2Fforum.odroid.com%2F+mali+driver+%22xu3%22)

드라이버 업데이트라고 하는데, 써봤다는 사람의 말을 들어보면 그냥 뭐 내꺼가 지금 성적이 더 좋다. 61 -> 64
그리고, 해당 드라이버는 linux 4를 타겟으로 하는 것으로.. 무턱대고 가져다 쓰지 말라는 말도 있고.
[ODROID Forum • View topic - Debian and Mali R5 driver for XU3](http://forum.odroid.com/viewtopic.php?f=96&t=12625)
[Commits · apxii/linux · GitHub](https://github.com/apxii/linux/commits/odroidxu3)
[Index of /meveric/kernel/XU3/r5p0](http://oph.mdrjr.net/meveric/kernel/XU3/r5p0/)
[ARM Mali Midgard GPU User Space Drivers - Mali Developer Center](http://malideveloper.arm.com/develop-for-mali/features/mali-t6xx-gpu-user-space-drivers/)
[ODROID Forum • View topic - Low performance with fbdev drivers (XU3)](http://forum.odroid.com/viewtopic.php?f=99&t=9327)

결국 fbdev 라는게 드라이버 자체의 이름이다. 이놈하고 대화하면 모니터/GPU를 직접 만질 수가 있게 되는 것 같다.
[FBDEV(4) manual page](ftp://www.x.org/pub/X11R6.8.0/doc/fbdev.4.html)
[EGL (API) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/EGL_(API))
[Mesa (computer graphics) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Mesa_(computer_graphics))

EGL.. 좋은데.. glfw를 EGL로 돌리면 좋겠다.. 맹 윈도우 관리해주는 얘는 하나 필요해.
[glfw egl - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=glfw%20egl)
[GLFW / Discussion / Using GLFW:Trouble creating EGL/GLESv2 window on Linux](http://sourceforge.net/p/glfw/discussion/247562/thread/da146ba8/)
[egl hello world - Google 검색](https://www.google.co.kr/search?q=egl+hello+world&oq=egl+hello+world&aqs=chrome..69i57j0l2.4077j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#q=egl+hello+world&newwindow=1&start=10)
[opengl es - How to create a window and fill it with color using OpenES 2.0 + X11? - Stack Overflow](http://stackoverflow.com/questions/9196526/how-to-create-a-window-and-fill-it-with-color-using-openes-2-0-x11)

rpi의 경우다.. 도움이 될 가능성이 높다. 컴파일 커멘드 등이 관심감.
[Raspberry Pi • View topic - Importing OpenGL ES Libraries in C/C++ Project](https://www.raspberrypi.org/forums/viewtopic.php?f=67&t=59372)

---

######2015Jun25 15:36:37+0900

결국 정리하면 지금 가장 해봐야 할 것은.

* EGL에서 2D로 설정해서, 선이랑 네모 같은 것을 pixel perfect로 그려보는 것이고.
* 그런 담에 이미지파일을 하나 로드하던가, 웹캠 캡쳐 쓰레드를 굴리거나 해서.. (일단 yuyv로 해도 좋고.) 이미지 텍스터를 pixel perfect로 올리고.
* 필요하거나, 좋아보이면.. EGL을 glfw에서 사용하게 하면 더 좋을 것 같다. 친절한 윈도우 매니져가 필요해...

---

######2015Jun28 14:39:11+0900

**OpenGL ES 2.0 Programming Guide : pdf**

[OpenGLES20ProgrammingGuide](OpenGLES20ProgrammingGuide.pdf)

---

######2015Jun29 17:34:55+0900

위에 책. 일단 chapter 7 까지 슥슥 보고..

*chapter 8 vertex shader 를 볼 차례*인데..
이제부터 본격적으로 vertex shader / fragment shader를 공부하게 된다.

알아야 할 양이 적지 않고.. 시간도 많이 걸리는데.. 오늘까지는 끝마쳐야 할 일이고...

한번 다시 생각해보게 된다. *왜 opengl을 시작하게 됐는지.* opencv 에서 할 수 는 없는 건지.

history를 살펴본 바로는.. 이걸 시작하게 된 이유는.. *하드웨어 가속*을 libjpeg-turbo 에서 알게 되고.. SIMD 라던가 GPU의 지원에 대해서 알게 되고나서..
기왕 하는 김에 하드웨어 가속기능까지 제대로 써서 하고 싶었는데..
*odroid 에서 지원하는 hardware codec 칩도.. 리눅스 플랫폼에서는 activate(드라이버가 없단다 병신 새끼들.)* 가 안되어있고.

그래서 그냥 opengl / mesa GPU 가속을 하고 싶었다.

근데 뭐 예상을 전혀 못한 건 아니었지만, 상당히 시간이 걸리고 있다.

-

지금 시점에서.. 한번 다시 생각해보게 된다.

*libGL 에러가 안나는 상태에서 glmark2-es 결과가 상당히 인상적*이어서 냉정함을 잃은 것일 수도 있고..
libjpeg-turbo를 쓰거나.. 그게 안되면 libjpeg 을 그냥 써서라도..

-

여튼 **목표는 MJPG으로 카메라에서 받아서 (이건 이미 성공을 했다.. v4l2로.. 짰잖아.) 안정적으로 화면에 표시하는 것인데.**
이것도 opencv에 있는 cvDecodeImage를 썼더니.. 잘 안되거나..
그냥 jpg 파일로 저장을 시켜서.. 이미지뷰어로 열어보니.. 됐다 안됐다 하길래..
opencv에 정떨어져서..
버릴라고 했는데..

-

일단 libjpeg을 하거나. libjpeg-turbo (ipp나 SIMD 지원을 좀 받으면서..) 를 써서..

이걸 잘 convert하기만 하면 되는 것이 아닐까.

-

그렇게 하고. **빨리 4채널을 올려서. 그려주기만 하면된다.**

문제는.. *어떻게 그릴까* 였다.

opencv에서 그려주는 것이 가능하긴 한데.

아예 프레임버퍼에 직접 그리는 방법 directfb 라던가.. gtk 라던가 사용하고 싶었는데..
뭘 해도. libGL 에러가 나거나.. 안된거나. glfw 경우에도 그렇고..

그래서 잠정적으로 opengl es 2 를 써야 한다고 이해하고.
공부를 하고 있는데..

-

libGL 에러가 나는 것이나.. 이미지 decode가 잘 안되는 것 같아 보이는 작은 문제들이 있더라도
일단 그 상태의 결과를 먼저 보고.. 그 이상의 결과가 필요하면 opengl es 2를 하거나. libjpeg-turbo를 하거나..
glfw / openframeworks 등의 도움을 받거나.. wayland 등을 시도해보거나..

이런 것들은 그 다음으로 미뤄도 되지 않을까.. 생각하게 된다.

---

######2015Jun29 17:50:34+0900

이쯤에서 다음 챕터로 넘기자.

지금 남아있는 search links를 여기 dump하고 넘어간다.

---

@ odroid - #1

[NeHe Productions: Lines, Antialiasing, Timing, Ortho View And Simple Sounds](http://nehe.gamedev.net/tutorial/lines,_antialiasing,_timing,_ortho_view_and_simple_sounds/17003/)
[GLX - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/GLX)
[Installing OpenGL/Glut libraries in Ubuntu](http://kiwwito.com/installing-opengl-glut-libraries-in-ubuntu/)
[NeHe Productions: Lesson 06 Texturing Update](http://nehe.gamedev.net/tutorial/lesson_06_texturing_update/47002/)
[How to link STL in c++ code? - Stack Overflow](http://stackoverflow.com/questions/8243051/how-to-link-stl-in-c-code)
[OpenGL Utility Toolkit - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/OpenGL_Utility_Toolkit)
[libGL error: MESA-LOADER - Google 검색](https://www.google.co.kr/search?q=libGL+error%3A+MESA-LOADER&oq=libGL+error%3A+MESA-LOADER&aqs=chrome..69i57.219542j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8)
[ODROID Forum • View topic - xbmc doesn't run after update ubuntu to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4652)
[ODROID Forum • View topic - Experience on upgrading Xubuntu 13.10 to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4628&p=37134#p37134)
[ODROID Forum • View topic - [TOOL] ODROID-Utility](http://forum.odroid.com/viewtopic.php?f=55&t=5053)
[ODROID Forum • View topic - Experience on upgrading Xubuntu 13.10 to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4628&p=37134#p37134)
[Mali binary driver - linux-sunxi.org](http://linux-sunxi.org/Mali_binary_driver)
[exynos_dri - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=exynos_dri)
[site:http://forum.odroid.com/ mali driver "xu3" - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=site:http:%2F%2Fforum.odroid.com%2F+mali+driver+%22xu3%22)
[ODROID Forum • View topic - Debian and Mali R5 driver for XU3](http://forum.odroid.com/viewtopic.php?f=96&t=12625)
[Commits · apxii/linux · GitHub](https://github.com/apxii/linux/commits/odroidxu3)
[Index of /meveric/kernel/XU3/r5p0](http://oph.mdrjr.net/meveric/kernel/XU3/r5p0/)
[ARM Mali Midgard GPU User Space Drivers - Mali Developer Center](http://malideveloper.arm.com/develop-for-mali/features/mali-t6xx-gpu-user-space-drivers/)
[ODROID Forum • View topic - Low performance with fbdev drivers (XU3)](http://forum.odroid.com/viewtopic.php?f=99&t=9327)
[FBDEV(4) manual page](ftp://www.x.org/pub/X11R6.8.0/doc/fbdev.4.html)
[EGL (API) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/EGL_(API))
[Mesa (computer graphics) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Mesa_(computer_graphics))
[glfw egl - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=glfw%20egl)
[GLFW / Discussion / Using GLFW:Trouble creating EGL/GLESv2 window on Linux](http://sourceforge.net/p/glfw/discussion/247562/thread/da146ba8/)
[egl hello world - Google 검색](https://www.google.co.kr/search?q=egl+hello+world&oq=egl+hello+world&aqs=chrome..69i57j0l2.4077j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#q=egl+hello+world&newwindow=1&start=10)
[opengl es - How to create a window and fill it with color using OpenES 2.0 + X11? - Stack Overflow](http://stackoverflow.com/questions/9196526/how-to-create-a-window-and-fill-it-with-color-using-openes-2-0-x11)
[Raspberry Pi • View topic - Importing OpenGL ES Libraries in C/C++ Project](https://www.raspberrypi.org/forums/viewtopic.php?f=67&t=59372)

-

@ odroid - #2

[alternative kdiff3 - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=alternative+kdiff3)
[Good diff tools under Ubuntu GNOME - Stack Overflow](http://stackoverflow.com/questions/4066111/good-diff-tools-under-ubuntu-gnome)
[aptitude apt-get - Google 검색](https://www.google.co.kr/search?q=dselect&oq=dselect&aqs=chrome..69i57j0l5.2788j0j9&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#newwindow=1&q=aptitude+apt-get)
[debian - What is the real difference between "apt-get" and "aptitude"? (How about "wajig"?) - Unix & Linux Stack Exchange](http://unix.stackexchange.com/questions/767/what-is-the-real-difference-between-apt-get-and-aptitude-how-about-wajig)
[wajig - Google 검색](https://www.google.co.kr/search?q=wajig&oq=wajig&aqs=chrome..69i57j69i60&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8)
[Togaware: wajig: Simplyfying Debian Administration](http://wajig.togaware.com/)
[GNU/Linux Desktop Survival Guide](http://linux.togaware.com/survivor/index.html)
[What does the "apt-get dselect-upgrade" command do? - Ask Ubuntu](http://askubuntu.com/questions/585273/what-does-the-apt-get-dselect-upgrade-command-do)
[command line - What is the correct way to completely remove an application? - Ask Ubuntu](http://askubuntu.com/questions/187888/what-is-the-correct-way-to-completely-remove-an-application)
[sudo apt get deb cache - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=sudo+apt+get+deb+cache)
[Show apt-get installed packages history via commandline? - Ask Ubuntu](http://askubuntu.com/questions/21657/show-apt-get-installed-packages-history-via-commandline)
[package management - How do I remove cached .deb files? - Ask Ubuntu](http://askubuntu.com/questions/32191/how-do-i-remove-cached-deb-files)
[playing movie shader opengl - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=playing+movie+shader+opengl)
[Problems applying GLSL shader to texture from a movie - Cinder Forums](https://forum.libcinder.org/topic/problems-applying-glsl-shader-to-texture-from-a-movie)
[how to play video in opengl?](https://www.opengl.org/discussion_boards/showthread.php/156765-how-to-play-video-in-opengl)
[site:nehe.gamedev.net movie - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=site%3Anehe.gamedev.net%20movie)
[NeHe Productions:](http://nehe.gamedev.net/article_index/2/)
[NeHe Productions: Playing AVI Files In OpenGL](http://nehe.gamedev.net/tutorial/playing_avi_files_in_opengl/23001/)
[OpenGL ES 3.0 Programming Guide](http://www.opengles-book.com/)
["XMoveResizeWindow" fullscreen - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=%22XMoveResizeWindow%22+fullscreen)
[c++ - X11, Change Resolution and Make Window Fullscreen - Stack Overflow](http://stackoverflow.com/questions/12706631/x11-change-resolution-and-make-window-fullscreen)
[_NET_WM_STATE - Google 검색](https://www.google.co.kr/search?q=_NET_WM_STATE&oq=_NET_WM_STATE&aqs=chrome..69i57&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8)
[_NET_WM_STATE - Google 검색](https://www.google.co.kr/search?q=override+redirect&oq=override+redirect&aqs=chrome..69i57&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#newwindow=1&q=_NET_WM_STATE)
[Application Window Properties](http://standards.freedesktop.org/wm-spec/1.3/ar01s05.html)
[Xlib Programming Manual: Window Attributes: Override Redirect Flag](http://tronche.com/gui/x/xlib/window/attributes/override-redirect.html)
[xinternatom net wm state fullscreen - Google 검색](https://www.google.co.kr/search?q=XInternAtom&oq=XInternAtom&aqs=chrome..69i57.1533305j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#newwindow=1&q=xinternatom+net+wm+state+fullscreen)
[Xlib Programming Manual: XInternAtom](http://tronche.com/gui/x/xlib/window-information/XInternAtom.html)
[xclient.message_type _NET_WM_STATE_FULLSCREEN - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=xclient.message_type+_NET_WM_STATE_FULLSCREEN)
[X11 Fullscreen - howto? - General Talk - OpenPandora Boards](http://boards.openpandora.org/topic/12280-x11-fullscreen-howto/)
[X11 Fullscreen window (OpenGL) - Stack Overflow](http://stackoverflow.com/questions/9083273/x11-fullscreen-window-opengl)
[X11 Fullscreen window (OpenGL) - Stack Overflow](http://stackoverflow.com/questions/9083273/x11-fullscreen-window-opengl/9083374#9083374)
[Raspberry Pi • View topic - Importing OpenGL ES Libraries in C/C++ Project](https://www.raspberrypi.org/forums/viewtopic.php?f=67&t=59372)
[opengl es - How to create a window and fill it with color using OpenES 2.0 + X11? - Stack Overflow](http://stackoverflow.com/questions/9196526/how-to-create-a-window-and-fill-it-with-color-using-openes-2-0-x11)
[openFrameworks on the UDOO - arm - openFrameworks](http://forum.openframeworks.cc/t/openframeworks-on-the-udoo/13940)
[GLFW: Compiling GLFW](http://www.glfw.org/docs/latest/compile.html)
[Raspberry Pi • View topic - Trying to get GLFW to work](https://www.raspberrypi.org/forums/viewtopic.php?f=68&t=63764)
[findglfw.cmake - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=findglfw.cmake)
[OpenSubdiv/FindGLFW.cmake at master · PixarAnimationStudios/OpenSubdiv](https://github.com/PixarAnimationStudios/OpenSubdiv/blob/master/cmake/FindGLFW.cmake)
[exynos_fimg2d.h - Google 검색](https://www.google.co.kr/search?q=exynos_drm.h+exynos_fimg2d.h&oq=exynos_drm.h+exynos_fimg2d.h&aqs=chrome..69i57.568410j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#newwindow=1&q=exynos_fimg2d.h)
[Direct Rendering Infrastructure / Mailing Lists](http://sourceforge.net/p/dri/mailman/dri-patches/?page=1)
[https://download.tizen.org/misc/media/conference2012/wednesday/ballroom-c/2012-05-09-1330-1410-the_drm_(direct_rendering_manager)_of_tizen_kernel.pdf](https://download.tizen.org/misc/media/conference2012/wednesday/ballroom-c/2012-05-09-1330-1410-the_drm_(direct_rendering_manager)_of_tizen_kernel.pdf)
[OpenGL ES 2.0 orthographic projection - The Code Crate](http://www.thecodecrate.com/opengl-es/constructing-the-orthographic-matrix/)
[Cameras on OpenGL ES 2.x - The ModelViewProjection Matrix](http://blog.db-in.com/cameras-on-opengl-es-2-x/)
[game design - OpenGL ES 2.0: Setting up 2D Projection - Game Development Stack Exchange](http://gamedev.stackexchange.com/questions/4309/opengl-es-2-0-setting-up-2d-projection)
[An intro to modern OpenGL. Chapter 2: Hello World: The Slideshow](http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Chapter-2:-Hello-World:-The-Slideshow.html)
[glGetUniformLocation - OpenGL 4 Reference Pages](https://www.opengl.org/sdk/docs/man/html/glGetUniformLocation.xhtml)
[Tutorials](https://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/uniform.php)
[openFrameworks - ofAppEGLWindow::setGLESVersion()](http://openframeworks.cc/documentation/application/ofAppEGLWindow.html#show_setGLESVersion)
[Using the Raspberry Pi's GPU with OpenGL ES](http://jan.newmarch.name/LinuxSound/Diversions/RaspberryPiOpenGL/)
[orthographic projection matrix in Opengl-es 2.0 - Stack Overflow](http://stackoverflow.com/questions/6465611/orthographic-projection-matrix-in-opengl-es-2-0)
[glUniformMatrix4fv - Google 검색](https://www.google.co.kr/search?q=glUniformMatrix4fv&oq=glUniformMatrix4fv&aqs=chrome..69i57&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8)
[Kriptonitapps » Tutorials](http://kriptonitapps.com/category/tutorials/page/2/)
[Drawing nearly perfect 2D line segments in OpenGL - CodeProject](http://www.codeproject.com/Articles/199525/Drawing-nearly-perfect-D-line-segments-in-OpenGL)
[Totologic: Preview : TotoEngine2D](http://totologic.blogspot.kr/2015/03/preview-totoengine2d.html)
[How To Read and Set Environmental and Shell Variables on a Linux VPS | DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-read-and-set-environmental-and-shell-variables-on-a-linux-vps)
[NeHe Productions: Lines, Antialiasing, Timing, Ortho View And Simple Sounds](http://nehe.gamedev.net/tutorial/lines,_antialiasing,_timing,_ortho_view_and_simple_sounds/17003/)
[GLX - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/GLX)
[Installing OpenGL/Glut libraries in Ubuntu](http://kiwwito.com/installing-opengl-glut-libraries-in-ubuntu/)
[NeHe Productions: Lesson 06 Texturing Update](http://nehe.gamedev.net/tutorial/lesson_06_texturing_update/47002/)
[How to link STL in c++ code? - Stack Overflow](http://stackoverflow.com/questions/8243051/how-to-link-stl-in-c-code)
[OpenGL Utility Toolkit - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/OpenGL_Utility_Toolkit)
[libGL error: MESA-LOADER - Google 검색](https://www.google.co.kr/search?q=libGL+error%3A+MESA-LOADER&oq=libGL+error%3A+MESA-LOADER&aqs=chrome..69i57.219542j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8)
[ODROID Forum • View topic - xbmc doesn't run after update ubuntu to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4652)
[ODROID Forum • View topic - Experience on upgrading Xubuntu 13.10 to 14.04](http://forum.odroid.com/viewtopic.php?f=77&t=4628&p=37134#p37134)
[ODROID Forum • View topic - [TOOL] ODROID-Utility](http://forum.odroid.com/viewtopic.php?f=55&t=5053)
[Mali binary driver - linux-sunxi.org](http://linux-sunxi.org/Mali_binary_driver)
[exynos_dri - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=exynos_dri)
[site:http://forum.odroid.com/ mali driver "xu3" - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#newwindow=1&q=site:http:%2F%2Fforum.odroid.com%2F+mali+driver+%22xu3%22)
[ODROID Forum • View topic - Debian and Mali R5 driver for XU3](http://forum.odroid.com/viewtopic.php?f=96&t=12625)
[Commits · apxii/linux · GitHub](https://github.com/apxii/linux/commits/odroidxu3)
[Index of /meveric/kernel/XU3/r5p0](http://oph.mdrjr.net/meveric/kernel/XU3/r5p0/)
[ARM Mali Midgard GPU User Space Drivers - Mali Developer Center](http://malideveloper.arm.com/develop-for-mali/features/mali-t6xx-gpu-user-space-drivers/)
[ODROID Forum • View topic - Low performance with fbdev drivers (XU3)](http://forum.odroid.com/viewtopic.php?f=99&t=9327)
[FBDEV(4) manual page](ftp://www.x.org/pub/X11R6.8.0/doc/fbdev.4.html)
[EGL (API) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/EGL_(API))
[Mesa (computer graphics) - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Mesa_(computer_graphics))
[glfw egl - Google 검색](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8&client=ubuntu#q=glfw%20egl)
[GLFW / Discussion / Using GLFW:Trouble creating EGL/GLESv2 window on Linux](http://sourceforge.net/p/glfw/discussion/247562/thread/da146ba8/)
[egl hello world - Google 검색](https://www.google.co.kr/search?q=egl+hello+world&oq=egl+hello+world&aqs=chrome..69i57j0l2.4077j0j7&client=ubuntu&sourceid=chrome&es_sm=93&ie=UTF-8#q=egl+hello+world&newwindow=1&start=10)

@ host pc

[OpenGL ES 3.0 Programming Guide](http://www.opengles-book.com/)
[What is the difference between OpenGL ES and OpenGL? - Game Development Stack Exchange](http://gamedev.stackexchange.com/questions/150/what-is-the-difference-between-opengl-es-and-opengl)
[OpenGL ES 3.0 Programming Guide, 2nd Edition.pdf](file:///C:/Users/doohoyi/Downloads/OpenGL%20ES%203.0%20Programming%20Guide,%202nd%20Edition.pdf)
[opengles-book-samples/LinuxX11 at master · danginsburg/opengles-book-samples](https://github.com/danginsburg/opengles-book-samples/tree/master/LinuxX11)
[Dither - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Dither)
[localhost/dooho_yi.bitbucket.org/ongoing/seco-hyu-3rd-year/OpenGLES20ProgrammingGuide.pdf](http://localhost/dooho_yi.bitbucket.org/ongoing/seco-hyu-3rd-year/OpenGLES20ProgrammingGuide.pdf)
[Dan Ginsburg - Google Search](https://www.google.co.kr/search?q=Dan+Ginsburg&oq=Dan+Ginsburg&aqs=chrome..69i57j0l5.263j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)
[OpenGL ES 3.0 Programming Guide, 2nd Edition [DrLol].pdf - Other » Ebooks - Torrent Download | Bitsnoop](http://bitsnoop.com/opengl-es-3-0-programming-guide-2nd-q71069658.html)
[OpenGL ES 3.0 Programming Guide - Google Search](https://www.google.co.kr/search?q=OpenGL+ES+3.0+Programming+Guide&oq=OpenGL+ES+3.0+Programming+Guide&aqs=chrome..69i57j69i60l3&sourceid=chrome&es_sm=122&ie=UTF-8)
[Author : Dan Ginsburg](http://4free-ebooks.com/author/Dan-Ginsburg)
[OpenGL ES 3.0 Programming Guide, 2nd Edition.pdf - Free Download - File Hosting Service](http://filepi.com/i/2BiQ2rZ)
[OpenGL ES subset - Google Search](https://www.google.com/search?q=351ed8b050c904cf701948fa3f10200dad1f4b6b&gws_rd=ssl#newwindow=1&q=OpenGL+ES+subset)
[OpenGL ES 2.0 Programming Guide](http://opengles-book.com/es2/)
[OpenGLES20ProgrammingGuide.pdf](file:///C:/Users/doohoyi/Downloads/OpenGLES20ProgrammingGuide.pdf)
[glOrtho](https://www.khronos.org/opengles/sdk/1.1/docs/man/glOrtho.xml)
[Cameras on OpenGL ES 2.x - The ModelViewProjection Matrix](http://blog.db-in.com/cameras-on-opengl-es-2-x/)
[mmal math 1998 mathml viewer - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=mmal%20math%201998%20mathml%20viewer&es_th=1)
[MathML - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/MathML)
[MathML 1 renderer - Google Search](https://www.google.co.kr/search?q=MathML+1&oq=MathML+1&aqs=chrome..69i57j69i60&sourceid=chrome&es_sm=122&ie=UTF-8#newwindow=1&q=MathML+1+renderer)
[Rendering MathML in HTML5 - Murray Sargent: Math in Office - Site Home - MSDN Blogs](http://blogs.msdn.com/b/murrays/archive/2011/07/31/rendering-mathml-in-html5.aspx)
[Firemath - The MathML-Editor for Firefox](http://www.firemath.info/)
[mathml chrome - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=mathml%20chrome&es_th=1)
[Math Anywhere - Chrome Web Store](https://chrome.google.com/webstore/detail/math-anywhere/gebhifiddmaaeecbaiemfpejghjdjmhc/related?hl=en)
[orthogonal projection shader gles2 - Google Search](https://www.google.co.kr/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#newwindow=1&q=orthogonal+projection+shader+gles2)
[ios - How to achieve glOrthof in OpenGL ES 2.0 - Stack Overflow](http://stackoverflow.com/questions/7317119/how-to-achieve-glorthof-in-opengl-es-2-0)