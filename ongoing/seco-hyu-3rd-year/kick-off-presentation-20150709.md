p.0

##### 세코 인터페이스 / 차선변경과제 3차년도 Kick-off Meeting 발표

*(발표 시간 : 15분 내외)*

---
p.1

- 주요 내용 (agenda)

- 과제 목표 설정
- 요구 사항 및 추가 지원 예정 사항
- 개발 진행 roadmap
- 핵심이 되는 개발 플랫폼 결정을 위한 feasibility 실험 진행 내용
- 현재 상태 정리 및 앞으로 진행 방향 확인

---
p.2

- 과제 목표
  - 기 제작된 PC 기반의 HVI 시스템의 Embedded System 화.
    - 제품화 여부가 가능한 수준의 컨셉 프로토타입
    - 독립적으로 enclosed / modularized 시스템 구현.

(그림)

---
p.3

- 요구 사양
  - 정보 수집
    - 차량 상태 정보 수집 - CAN 4 채널
    - 차선 변경 시스템 작동시 자차 주변 도로 상황 정보 수집 - 영상 4채널
  - HVI 화면 구성하여 운전자에게 실시간 제공

- 추가 지원 예정 사양 (과제 목표 외)
  - 차량 및 도로 정보 수집 후 분석을 위해 저장
  - 저장된 작동 기록을 열람할 수 있는 PC 기반 소프트웨어

(그림)

---
p.4

- 개발 시스템 개요

(그림)

  - 필수 : CAN 4채널, 영상 4채널, RTC
  - 옵션 : HDD I/F, 오프라인 replay S/W

---
p.5

- 개발 의의
  - 과제 임무 완수
  - 상품화 가능성 타진 -> 추가 목표
    - 소형화 / 경량화
      - 체적 <= 10cm x 10cm x 10cm = 1000cm3
      - 무게 <= 1kg
    - 안정화(repeatibility & robustness)
      - Boot-up time <= 10sec
      - Continuous running >= 1 day
    - 저가격
      - 재료비 <= 100만원

---
p.6

- 주요 이슈
  - 2차년도 기 개발된 시스템
    - 가격 : 재료비. CAR-PC + Frame Grabber + Kvaser CAN receiver = 약 700만원 가량 + 카메라 4대 등등 추가로 더해짐.
      - 컴퓨팅 시스템의 획기적인 변화가 필요.
    - 운영체제 : Microsoft Windows 7 -> UNSTABLE
    - 크기 및 무게 -> 목표 수치의 3~5배
    - 구성 -> S/W 위주 (MFC 5% + OpenCV 90% + Win32 5%) -> Cross platform 가능
  - 과제 기간 대비 목표
    - 과제 투입되는 인력 대비 기간이 충분하지 않음
      - 고속의 CPU가 탑재되는 복잡도 높은 시스템의 H/W + F/W + S/W 를 자체적으로 제작하는 일은 주어진 인력으로는 과제 기간내에 불가능
        - 기 개발된 Linux 지원 SBC(Single Board Computer) 사용.

---
p.7

- 개발 방향
  - 기 개발된 Linux 지원 SBC(Single Board Computer) 중에서 개발 용이한 플랫폼을 선정하고 여기에 기존 S/W를 포팅하는 것이 유일하게 가능한 방향으로 판단

- 이슈 및 그에 따른 개발 방향
  - 기 개발된 시스템의 개발 플랫폼이었던 SB100-NRM 보드는 i7-3770 (3.9GHz Quad-core) CPU를 탑재한 고성능 플랫폼
    - 가격적으로 접근이 가능한 한에서 최고 성능의 SBC를 선정하고, 부족한 부분은 효율적인 자원 관리로 대응해야 함
    - **후보 1 (XU3) : HardKernel Odroid XU3 (Samsung Exynos 5422) / Android or Linux** - Wikipedia의 SBC 비교표, cpu-freq 기준 3위. $179
  - 개발 시간을 단축하기 위해서는 사용자 층이 두터운 SBC 선정이 매우 중요
    - **후보 2 (RPI) : Raspberry Pi 1 B+/Raspberry Pi 2 B (Broadcom BCM2835/BCM2836) / Linux only** - 가장 사용자층이 두터운 플랫폼. 대부분의 이슈가 이미 논의된 상태.
  - SBC는 일반적인 기능만 구현하고 있으므로 추가적으로 필요한 기능을 확장해 삽입할 수 있는 가능성을 확인해야 함. 확장해야 하는 기능으로는 현재,
    - *CAN Interface*
      - XU3 / RPI 모두 CAN 자체 지원하지 않음. 기능 확장을 위한 interface 방법 중 속도면에서 유리한 SPI로 확장 (최대 clock 속도 제한 없음)
      - 확장 보드는 제작하거나 기존의 사내 **기 개발된 보드**를 변용.
      - 자체 제작 시에는 **Teensy 3.1 (Freescale MK20DX256VLH7)** 의 CAN I/F, SPI 활용 가능.
  - 성능상 벽에 부딪힐 경우 Distributed Computing 가능
    - *Time Syncronization을 위한 RTC가 필요*
      - XU3 - RTC 내장
      - RPI - RTC 비내장
  - 카메라 interface
    - XU3
      - USB 카메라 4대, MIPI CSI-2 카메라 1대 -> USB 카메라 (USB Bandwidth 제약으로 MJPG Compression/Decompression 필수)
    - RPI
      - USB 카메라 4대, MIPI CSI-2 카메라 1대 -> USB 카메라 (USB Bandwidth 제약으로 MJPG Compression/Decompression 필수)
    - RPI COMPUTE MODULE + I/O MODULE
      - USB 카메라 1대, MIPI CSI-2 카메라 2대 -> MIPI CSI-2의 경우 FULL HD 가능. (USB Bandwidth 제약) -> 2대를 RTC로 synchronize하는 방법 가능.

---
p.8

- 개발 방향 CASE 1 : Odroid XU3 기반

(그림:구성도)

---
p.9

- 개발 방향 CASE 2 : RPI 2 B 기반

(그림:구성도)

---
p.10

- 개발 방향 CASE 2 : RPI Compute + I/O Board 기반

(그림:구성도)

---
p.11

- Feasibility 검증 : XU3 + Odroid USB Camera x 4

- XU3
  - ARM 프로세서 - SIMD / NEON
  - GPU 하드웨어 가속 - Mali graphic driver - OpenGL ES 2.0
  - DRM (Direct Rendering Manager) - Exynos specific feature
  - Hardware Codec - Exynos specific feature -> NO DRIVER for LINUX. -> USELESS

- Odroid USB Camera
  - Hardkernel claims that this model supports UVC.
  - For 1 or 2 channel capturing, could be acceptable. But it was not for our case (4 ch) !
    -> NEED to try with different camera,
      - which is confirmed by uvcvideo module developers. -> Logitech C270 etc.
      - just by trial & error.

---
p.12

- New possibilities

  - xenomai & rt-patch / latency issue strategy
  - glmark2-es 64 points. - more than 100 fps for complex 3d model rendering. @ xu3
  - no X server. directframebuffer directFB.
  - rt-patch / CONFIG_PREEMPT / CONFIG_PREEMPT_RT
  - netcat lag < 0.1 over LAN.

- New learnings

- Camera Capturing FPS setting doesn't work?
  - Due to AGC, shutter speed changing according to the ambient light. So, simply try to beam a strong light into the camera, then fps will match the setting.

- New spec.
  - lag < 0.1

pthread + vidioc(v4l2)

---
p.13

- Feasibility 테스트 결과.
  - Odroid USB Camera x 4
    - 320x240 3채널 (yuyv/30fps)
      - uvcvideo 모듈 에러 -> USB Bandwidth 설정 실패, 모듈 설정 quirks=0x80 적용하더라도 Uncompressed(YUYV) 포맷에만 적용가능
      - USB Bandwidth 설정 문제 우회 코드 (quirks=0x80) 적용하더라도 MJPEG 포맷을 사용 못하면 무의미함.
  - Odroid USB Camera x 1 + Logitech Quickcam E 3500 x 1
    - 1280x700 1채널 (mjpg/30fps) + 640x480 1채널 (mjpg/30fps)
      - 반드시 우회 코드 없이 해야할 필요가 있음. 즉, 기기에서 정상적으로 bandwidth 요구사항을 지원해야 함.
      - Odroid USB Camera는 우회 코드 없이 1대 이상 동시 작동이 안됨. 1대의 카메라가 USB Bandwidth 전체를 요구하기 때문에.
      - uvcvideo 측에서 제공하는 정상 지원되는 카메라 리스트에 들어있는 카메라 1 대를 같이 사용하면 작동이 됨.

      
---
p.14

- 

uvcvideo

어렵지 않게 생각했던 카메라가 말썽을 일으켜서, 플랫폼 결정을 위한 실험이 제대로 진행이 안되고 있는 상태.

odroid 측에서 제공하는 camera가 uvc standard를 제대로 지원하지 않는 것으로 판명.
다른 카메라를 찾아서 테스트를 마무리할 필요가 있음.

---

2015/07/09 - 2015/07/09 : ** weeks : 과제 Kick-off Meeting
2015/07/09 - 2015/07/16 : 01 weeks : 로지텍 C270 4대 구매 (시험용)
2015/07/09 - 2015/07/16 : 01 weeks : ELP 사의 UVC 웹캠 (국내)
2015/07/16 - 2015/08/13 : 04 weeks : ELP 사의 UVC 웹캠 + 렌즈 4대 (대만)
2015/07/09 - 2015/08/13 : 05 weeks : 4채널 웹캠 Grabbing & Stream-on 디스플레이 성능 시험 (Multi-thrd)
2015/08/14 - 2015/08/14 : ** weeks : 타당성 시험(feasibility test) 종료, 개발 계획 확정
2015/08/09 - 2015/09/09 : 04 weeks : RTC 기반 Multi-threading 환경 구축
2015/09/09 - 2015/10/09 : 04 weeks : CAN 인터페이스 보드 H/W 수정 또는 제작
2015/09/20 - 2015/10/09 : xx weeks : CAN 인터페이스 보드 F/W 구현
2015/10/01 - 2015/11/15 : xx weeks : CAN 인터페이스 보드 드라이버 구현 (옵션)
2015/11/16 - 2015/11/30 : xx weeks : CAN 인터페이스 보드 성능 시험 및 수정 보완
2015/11/30 - 2015/11/30 : ** weeks : H/W, F/W 개발 종료
2015/12/01 - 2016/03/31 : 00 weeks : 4 채널 영상, 4 채널 CAN 작동 실차 시험 시작 및 개선
2015/12/01 - 2016/01/31 : 00 weeks : 기존 Display 알고리즘의 OpenGL ES 2.0 변환
2016/02/01 - 2016/02/28 : 00 weeks : OpenGL ES 2.0 기반 Display 성능 시험 및 개선
2016/03/01 - 2016/03/01 : ** weeks : 시스템 릴리즈
2016/03/01 - 2016/05/31 : 00 weeks : 시스템 버그 수정
2016/02/01 - 2016/03/31 : 00 weeks : HVI 운전자 대응 그래픽 디자인
2016/02/01 - 2016/04/31 : 00 weeks : HVI 개발자 대응 그래픽 개선 및 보완
2016/04/01 - 2016/04/15 : 00 weeks : USB-to-SATA3 Intergration 테스트
2016/04/16 - 2016/04/30 : 00 weeks : 데이터 로깅 개발 및 시험
2016/05/01 - 2016/05/31 : 00 weeks : 데이터 로깅 문제 수정
2016/05/01 - 2016/05/01 : 00 weeks : 데이터 replay S/W 릴리즈
2016/05/20 - 2016/05/31 : 00 weeks : 보고서 작성
2016/05/31 - 2016/05/31 : ** weeks : 프로젝트 종료