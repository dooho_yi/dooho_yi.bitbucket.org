
####subject => ordoid에서 USB CAM 1채널 재생의 성능 보기

---

######2015Jun13 01:43:37+0900

모니터 가져다가. 해보자.
일단 카메라 1대를 캡쳐하고. 표시하는 것까지만 해보지 뭐.

-

해봤는데.. guvcvideo 라는 툴이 있어서 실행하니가. 영상이 나왔는데, fps를 30으로 설정해도 최대 12 까지밖에 안된다. 여러가지 모드가 잘 작동은 하는데.. capture도 잘 안되는 거 같고.. 뭐..

-

일단 1 채널 영상에서도 30fps가 안되면 뭘 할 수 있겠나 싶다. 좀더 효율이 높은 결과를 얻어보자.
c코드로 하나 짜거나,
of를 설치했으니. 그걸 이용하거나.

-

뭔가 하려면, 인터넷을 연결하면 좋겠는데.. 지금 of가 설치가 된게 아니다 생각해보니..
그건 rpi에 설치했던거지..odroid에는 설치 안했어.

-

그냥 c코드로 짜봐야 하나.
opencv로 해보자.

---

######2015Jun13 01:58:23+0900

opencv는 일단 깔려있고.

sudo dpkg -l => 설치된 패키지 전체 목록, 여기서 원하는 패키지의 명칭을 확인
sudo dpkg -s => 해당 패키지의 정보 조회
sudo dpkg -L => 해당 패키지의 파일들의 위치!

이렇게 해서 opencv와 gcc가 깔려있는 걸 확인했고.. 도큐멘테이션 폴더도 있긴한데..

makefile을 쓸 줄 몰라서 그런가..
개발환경을 셋업하는게 무지 막막하네.

1. 인터넷을 연결해서 codeblocks 같은 걸 깔거나.
2. makefile을 작성하면 되는데.. 그랬을 때는.. 리눅스 시스템 라이브러리 같은 것들을 잘 이해하고 챙겨야 한다.

음.. 일단 뭐 복잡한거 할 것도 아니니.. 2번방법으로 해보자.

-

opencv 리눅스 도큐멘테이션에 보면..

<http://docs.opencv.org/doc/tutorials/introduction/linux_gcc_cmake/linux_gcc_cmake.html>

cmake를 쓰라고 하는데.. 이것도 깔려있었다.
그럼 이방법으로 해보자.

---

######2015Jun13 02:16:32+0900

**setting up a dev. env. @ linux with cmake/opencv**

오케이. 일단 DisplayImage라는 프로젝트를 cmake의 도움을 받아 makefile 생성해서 실행 성공.

-

이번에는 웹캡을 잡아서 그거를 보여주는 걸 만들건데..
관련 예제를 찾아보자.

---

######2015Jun14 12:32:14+0900

**연구노트** 만들어야 함.

---

######2015Jun14 13:11:10+0900

한번 정리를 해야 겠다.

-

**opencv video @ linux machine.**
<http://linuxconfig.org/introduction-to-computer-vision-with-opencv-on-linux#h4-5-input-from-a-video-camera>
"Introduction to Computer Vision with the OpenCV Library on Linux"

-

**cpp로 안하고 c 파일로 했더니.. 'dso missing from command line' 에러가 나서. 좀 시간좀 보냈는데..**
glibc2.4 (보다 정확하게는, lrintGLIBC_2.4 였던듯.) 를 찾아달라고 하는 거 같은데.. 잘 모르겠다. 나중에라도 c코드를 바로 써야 할 때가 있으면, 어떻게 해결하지 않을 수 없겠는데.. 일단은 cpp로 바꾸니까 됐음.

관련 searches.
<http://stackoverflow.com/questions/19901934/strange-linking-error-dso-missing-from-command-line>
<http://stackoverflow.com/questions/24096807/dso-missing-from-command-line>
<http://askubuntu.com/questions/521706/error-adding-symbols-dso-missing-from-command-line-while-compiling-g13-driver>
<http://www.linuxquestions.org/questions/linux-software-2/how-to-check-glibc-version-263103/>
<http://askubuntu.com/questions/230207/how-to-install-glibc-devel-in-ubuntu-12-04>

---

######2015Jun14 16:08:11+0900

계속 정리중이다.

**utilizing cmake**

<http://docs.opencv.org/doc/tutorials/introduction/linux_gcc_cmake/linux_gcc_cmake.html>

일반적인 과정. cmake를 써서 makefile 생성하고 진행하는 과정을 알려줌.

cmake를 써서, makefile을 직접 안쓰고, 조금 managed 하게.. 개발하는 방법이 적혀있다. 시작점이 됨.

여기에 개념중 find_package의 behaviour가 궁금했는데..

<http://www.cmake.org/Wiki/CMake:How_To_Find_Libraries#How_package_finding_works>

이 부분을 참고하면 이해가 된다.

```
The find_package() command will look in the module path for Find<name>.cmake, which is the typical way for finding libraries. First CMake checks all directories in ${CMAKE_MODULE_PATH}, then it looks in its own module directory <CMAKE_ROOT>/share/cmake-x.y/Modules/.
If no such file is found, it looks for <Name>Config.cmake or <lower-case-name>-config.cmake, which are supposed to be installed by libraries (but there are currently not yet many libraries which install them) and that don't do detection, but rather just contain hardcoded values for the installed library.
The former is called module mode and the latter is called config mode. Creation of config mode files is documented here. You may also need the documentation for importing and exporting targets.
For the module system there seems to be no documentation elsewhere, so this article concentrates on it.
No matter which mode is used, if the package has been found, a set of variables will be defined:
<NAME>_FOUND
<NAME>_INCLUDE_DIRS or <NAME>_INCLUDES
<NAME>_LIBRARIES or <NAME>_LIBRARIES or <NAME>_LIBS
<NAME>_DEFINITIONS
All this takes place in the Find<name>.cmake file.
Now, in the CMakeLists.txt file in the top level directory of your code (the client code that is actually going to make use of the library <name>, we check for the variable <NAME>_FOUND to see whether the package has been found or not. For most packages the resulting variables use the name of the package all uppercased, e.g. LIBFOO_FOUND, for some packages the exact case of the package is used, e.g. LibFoo_FOUND. If this variable is found, then, we pass the<NAME>_INCLUDE_DIRS to the include_directories() command and <NAME>_LIBRARIES to the target_link_libraries() command of CMake.
These conventions are documented in the file readme.txt in the CMake module directory.
The "REQUIRED" and other optional find_package arguments are forwarded to the module by find_package and the module should affect its operation based on them.
```

관련 searches.

<http://www.cmake.org/cmake/help/v3.0/command/find_package.html>
<http://stackoverflow.com/questions/11449499/how-does-cmake-command-find-package-work-with-variable-name-dir>
<http://stackoverflow.com/questions/11938996/cmakes-find-package-not-setting-variables>
<http://www.cmake.org/cmake/help/v3.0/command/set.html>

-

cmake에서 자동 설정하는 변수의 리스트. 와 이를 조회하는 스크립트.

<http://www.cmake.org/cmake/help/v3.0/manual/cmake-variables.7.html>
<http://www.cmake.org/Wiki/CMake_Useful_Variables/Logging_Useful_Variables>

-

튜토리얼 : <http://www.cmake.org/cmake-tutorial/>

-

어떻게 cmake에서 c 컴파일할 때 안되는 아까 그 문제를 해결할 수 있는 건지에 대한.. 리서치.

<http://www.systutorials.com/5217/how-to-statically-link-c-and-c-programs-on-linux-with-gcc/> : How to Statically Link C and C++ Programs on Linux with gcc
<http://stackoverflow.com/questions/7690800/can-cmake-use-g-to-compile-c-files>

<http://choorucode.com/2013/12/24/how-to-set-c-or-c-compiler-for-cmake/> : 결국 cmake에 컴파일러 설정하는 변수가 있다는 이해에 다다름.
```
Sometimes you might need to use a different version of a default compiler (like gcc) or a different compiler (like Intel compiler) to build your code. Specifying this is easy in CMake. Add these lines to your CMakeLists.txt:

SET(CMAKE_C_COMPILER /path/to/c/compiler)
SET(CMAKE_CXX_COMPILER /path/to/cpp/compiler)
```

---

######2015Jun14 16:17:23+0900

**opencv video @ linux machine.**

<http://linuxconfig.org/introduction-to-computer-vision-with-opencv-on-linux#h4-5-input-from-a-video-camera> : Introduction to Computer Vision with the OpenCV Library on Linux

일반적인 과정. cmake를 써서 makefile 생성하고 진행하는 과정을 알려줌.
이 글에서.. 사실은 여러가지 보여주는데.. video i/o를 보여주고 있다.. 참고.

-

<http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html> : Reading and Writing Images and Video

<http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture> : videocapture를 쓰는 법.

```
#include "opencv2/opencv.hpp"

using namespace cv;

int main(int, char**)
{
    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    Mat edges;
    namedWindow("edges",1);
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        cvtColor(frame, edges, CV_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
        imshow("edges", edges);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}```

<http://derekmolloy.ie/beaglebone/beaglebone-video-capture-and-image-processing-on-embedded-linux-using-opencv/> : beaglebone 같은.. sbc linux machine에서 opencv 개발하는 내용. 좀더 생생하고 정확하다.

---

######2015Jun14 16:19:55+0900

현재 상태를 보면.

카메라 1대를 capture해서. 화면에 뿌리는 것 까지만 시켰는데 속도가 나오긴하는데 불안정하고. 피크 타임이 100ms에 가깝게 올라가기 때문에 10fps. 더 안정적이고 빠르게 처리할 수 있게 하고 싶다.

방법들을 보면,

1. pthread를 써서. 어플을 최적화하는 법.
2. config_preemt_rt를 써서. linux를 최적화하는 법.
3. xenomai를 써서, 인터럽트 큐를 최적화하는 법.

이렇게 있다.
1번은 어쨌든 해야 할 것 같고.. 연습삼아서라도 해봐야 할 것 같다.
2번은 커널 컴파일을 해야 하는데, 이것도 연습상해봐야 할 것 같고.. 그렇긴한데.. 소요 시간이 한정이 잘 안된다. 공부를 해야 해서.. 또 한다고 해도 chrt를 통해서. 운영체제를 최적화해야 한다. (뭐 이런 공부도 필요할 것 같다.)
3번은 궁극적으로 할 수 있는 방향인데.. 2번에 비해서, 좀더 deterministic 한 방향인 것도 같다. chrt를 신경 안써도 되니까.. 그렇긴한데.. 생각대로 안된다거나 하면 곤란하고.. 2번이랑 같이 해야 할 것 같기도 하다.

어찌됐든, 1/2/3을 진행하면서 뭔가 획기적으로 상태가 개선되지 않으면 좀 힘들 것 같긴함.

운영체제를 혹은 GUI를 거치지 않고. 즉, window manager를 통하지 않고, 즉, x를 통하지 않고 바로 gui를 하는 방법도 알아야 할 것 같다.

---

**doing graphics without any overhead**

let's do it without even X!

<http://stackoverflow.com/questions/21724554/command-line-linux-opengl-processing> : commandline openGL processing
<https://www.opengl.org/discussion_boards/showthread.php/165610-OpenGL-under-Linux-without-X-Windows> : OpenGL under Linux without X Windows?
<https://www.opengl.org/discussion_boards/showthread.php/165068-OpenGL-on-Linux-without-X11?p=1166484#post1166484> : OpenGL on Linux without X11
<http://www.directfb.org/index.php?path=Main%2FScreenshots&page=1> : DirectFB (direct frame buffer)
There is also DirectFBGL for openGL EGL support for this.

=> there is SOME ways.. but not certain.

---

######2015Jun14 17:45:32+0900

아까 하던거..

1. pthread를 써서. 어플을 최적화하는 법.
2. config_preemt_rt를 써서. linux를 최적화하는 법.
3. xenomai를 써서, 인터럽트 큐를 최적화하는 법.

이중에서. 1번은.. 어쨌거나 해야 하는 일이긴한데.. 사실, odroid를 안하게 되면 얘기가 달라지니깐. 굳이 꼭 해야 할일은 아닐 수도 있다.

지난번에 전영우 과장이 말했던게.. 영상 실시간을 뒷처리 방식으로 해서.. 어느정도 부정확도를 인정하면서. 타임스탬프를 실시간으로 찍어주는 걸로.. 보상해주면서 가는 방식이었는데.. 음. 좀 잘 기억이 안나는 거 같고..

-

그렇다면, 일단 여러가지 최적화 이슈도 있고 하니.. 2번을 해야 하나?

-

또는 일단 4채널 영상을 보는게 우선이니까. 1번을 해서.. 4채널을 해보나?

-

일단 실적을 좀 내보려면, 1번을 해서, 4채널을 보고.. (언제 추궁이 들어올지모르니깐..)
현재 리눅스에서 어느정도까지 나오는지 볼까?

-

그래 그럼. 그렇게 하지 뭐.

1번을 하면서 살펴보자.

---

######2015Jun14 17:49:22+0900

**pthread**
