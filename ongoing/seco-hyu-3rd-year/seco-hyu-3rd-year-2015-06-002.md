
####subject => ordoid에서 USB CAM 1채널 재생의 성능 보기 : 성능 개선을 위해서 => 커널 컴파일 시도.

---

######2015Jun14 17:53:31+0900

근데 또 막상하려니깐... 쩝.. 커널 컴파일이 해보고 싶은거 같어 ㅜㅜ
어떡하징 쩝. 그럼 해봐야징..

-

그렇다면, 그전에 먼저 지금의 상태를 이미지를 뜨고, 원상복구 하는 법을 알아야 할 것 같은데.

-

ㅇㅇ 그거야 뭐 eMMC 어답터가 있으니까. 어답터 꽂아서 뜨면 되지.

-

하나 뜨자.

-

---

######2015Jun14 18:32:02+0900

이미지 떴음.

doohoyimac 하드에.. odroid-image.dmg 파일.

-

이제 안심하고 커널 컴파일 놀이 해보자.

-

**odroid kernel compile**

lubuntu를 기반으로 하자.
of의 kernel compile 가이드 + adafruit kernel-o-matic 을 참고 한다.

<https://learn.adafruit.com/raspberry-pi-kernel-o-matic/overview>
<http://openframeworks.cc/setup/raspberrypi/Raspberry-Pi-Cross-compiling-guide.html>
<http://openframeworks.cc/setup/raspberrypi/Raspberry-Pi-DISTCC-guide.html> : processing power support through network.

-

of의 가이드는 크로스 컴파일 자체에 대한 것이지, 커널 컴파일에 대한 것은 아니네.. 잘되면 응용은 할 수 있겠지만, 직접적인 것은 아님.

-

odroid kernel compile에 대해서는.

<http://odroid.us/mediawiki/index.php?title=Step-by-step_Native_Compiling_a_Kernel>

---

######2015Jun14 19:44:31+0900

**debian @ odroid**

odroid에서 데비안을 쓰는 방법 조사.

kernel과 distro의 관계에 대하여..
kernel만 있으면, 아무 distro나 쓸 수 있는 것인가?
<http://askubuntu.com/questions/172927/do-all-linux-distros-use-the-same-kernel>
답 : 그렇긴하다. distro에서 공식 kernel에 약간의 수정을 가하는 경우가 있다. 대략 잘 작동하는 결과는 얻을 수 있지만, 최선의 조합을 찾기 위해서, 조작을 좀 하기 때문에. 일부러 old kernel을 쓰기도 하고 그러는 것이다.. 이부분을 감안할것.
```
All distros use the same "Linux" kernel, however all distros make slight changes to it in order make the kernel work best for them, however these changes will almost always get uploaded back to the top where Linus will merge them himself. So all use the Linux kernel, however they all have a few different lines of code in them to make them work best for that distro. It is also worth noting that distros will ship with the version of the kernel that they see fit for each version. Some distros choose to choose a newer kernel then others. The main pro of a new kernel are improvments in driver's and hardware compability. The con is a loss in stability as all new code has bugs in it. So you trade features for stability. This is why disrtos known for being more stable will usually always ship an older kernel then the more risky distros. To find what kernel you are running enter:

uname -r
This will show you what you are running in the version of Ubuntu you have on your computer currently. Hope you enjoy!
```

<https://wiki.debian.org/HowToRebuildAnOfficialDebianKernelPackage> : debian 측의 커널 빌드 튜토리얼.

odroid 측의 debian build 관련 이야기들.
<http://forum.odroid.com/viewtopic.php?f=96&t=12625> : Debian and Mali R5 driver for XU3 => 여기에는 특별한 이야기는 없다.
<http://forum.odroid.com/viewtopic.php?f=96&t=7198> : new Debian with new XU/XUlite
```
Porting Debian Wheezy to XU3 is not very hard, done it and it's working without major issues. There is just ONE thing missing.
libMali.so the binary blob for graphics acceleration.
```
걍 잘 된단다. Mali라는 그래픽 가속기능이 잘 안된단다 그래서 위에 있는 이야기들이 계속되는 듯.
여기보면, meveric 이라는 유저가 있는데 이사람이 old game emulator/ gamebox용으로 debian 기반 이미지를 만들었다.
<http://oph.mdrjr.net/meveric/kernel/XU3/> : 이게 그사람의 커널이고.
<http://forum.odroid.com/viewtopic.php?f=11&t=2684&start=550> : 이게 그 사람이 해당 이미지를 릴리즈하는 쓰레드이고.
우분투의 경우에 어떻게 전체 이미지를 리빌드할 수 있는지 가이드 => <http://forum.odroid.com/viewtopic.php?f=52&t=3662&start=50>
<http://oph.mdrjr.net/meveric/images/OGST/> : 이게 meveric의 게임박스 이미지들이다.
다운 받아놔야 할지도 모를.. 것.. 같기도 하고..

---

######2015Jun14 20:11:11+0900

여튼 debian을 쓸라면 쓸 방법은 마련이 되어있고....

그렇긴한데.. config_preempt_rt 패치를 하기 위해서 굳이 debian으로 가야 할 필요까진 없는 것 같다.
지금의 lubuntu에서 커널만 컴파일 새로 해서 넘어가면 된다.

**config_preempt_rt for ubuntu on odroid**

커널 컴파일을 하기 위한 커널 코드는..
<https://github.com/hardkernel/linux/tree/odroidxu3-3.10.y>
이것을 사용하면 될 것으로 보인다.

-

여튼, commit을 보니까. Mali 도 기본 커널에 많이 올라와있는 것 같다. triple buffering 지원이 최근에 commit 되었다..

-

일단 밥을 먹어야하넹.

---

######2015Jun14 22:53:31+0900

밥 먹었고. naty랑 전화도 했고.

-

인제 다시 시작해봐야겠넹

-

그냥 커널 컴파일을 다시 하는데.. config_preempt_rt 패치를 적용해서 하면 된다.

---

######2015Jun14 23:53:43+0900

일단 아까 하던거 계속.

**odroid kernel compile**

<https://learn.adafruit.com/raspberry-pi-kernel-o-matic/overview>
<http://odroid.us/mediawiki/index.php?title=Step-by-step_Native_Compiling_a_Kernel>

이 2개 링크를 읽어보자.

-

<https://github.com/hardkernel/linux/tree/odroidxu3-3.10.y> : 이걸 일단 clone.

---

######2015Jun15 03:23:55+0900

WELL..

이래 저래 노력은 해봤는데..

첫째,
odroid 기본 kernel은 CONFIG_PREEMPT 까지 패치가 되어있다.

**CONFIG_PREEMPT와 CONFIG_PREEMPT_RT 랑 다른 거냐? 그렇다.**

어떻게 다른지는..

<https://rt.wiki.kernel.org/index.php/Frequently_Asked_Questions#What_are_real-time_capabilities_of_the_stock_2.6_linux_kernel.3F> 이걸봐바라..

```
The 2.6 Linux kernel has an additional configuration option, CONFIG_PREEMPT, which causes all kernel code outside of spinlock-protected regions and interrupt handlers to be eligible for non-voluntary preemption by higher priority kernel threads. With this option, worst case latency drops to (around) single digit milliseconds, although some device drivers can have interrupt handlers that will introduce latency much worse than that. If a real-time Linux application requires latencies smaller than single-digit milliseconds, use of the CONFIG_PREEMPT_RT patch is highly recommended.
```

즉, CONFIG_PREEMPT 로 대략적으로 최대 10ms 이내의 latency를 가지게 만들 수 있다고 한다.
그렇긴한데.. 1ms 언더를 원한다.. 나는..
CONFIG_PREEMPT로도 성능이 나올 수는 있는데.. 뭔가 reliablity가 문제가 되는 경우에는. 중간중간 느려지는 결과가 완전히 배제되진 않는다...
CONFIG_PREEMPT_RT를 쓰면 그게 가능하단다. 어쨌든. 얼마 이내에 작동한다는 것을 보장해준다는 것이지..

-

다시 말하면.. RT패치는 아직 공식 linux 커널에 올려지지를 않았다.. 공식 커널 = 바닐라 커널. 이랑 버젼 맞춰서 셋트로 패치도 나오는가 본데.. 일단은 먼저 내 커널의 버젼을 알아야할 것 같다.

<http://stackoverflow.com/questions/12151694/how-to-find-the-version-of-a-linux-kernel-source-tree>
```
make kernelversion
```
소스 폴더가서 이렇게 치면 나온다는 게지.

현재 실행중인 리눅스 커널의 정보를 알아보는 것은 2가지방법이 있다...

먼저 기본이.. uname -a 로 알아보는 것인데.. PREEMPT 설정의 커널은 이 텍스트에 PREEMPT라고 적혀있다.
예를 들면, 지금 odroid에서 uname -a 라고 치면.
```
Linux odroid 3.10.72 #1 SMP PREEMPT Wed Apr 1 21:33:21 BRT 2015 armv7l armv7l armv7l GNU/Linux
```
라고 나온다.
커널 버젼.. 컴파일 횟수.. SMP PREEMPT 설정.. 언제 만든거지 하고.. 아키텍쳐 등등..의 기본정보가 나온다.

-

지금 사용중인 커널이 컴파일을 할 때 어떤 설정으로 컴파일 됐었는지 알아보는 것은 다음 방법으로 할 수 있다.

다음 파일에 그 정보가 있다고 한다.
<http://superuser.com/questions/287371/obtain-kernel-config-from-currently-running-linux-system/287372#287372>
```
/proc/config.gz
/boot/config
/boot/config-$(uname -r)
```

gz 파일의 내용을 보기 위해서는 다음과 같이 할 수 있다..

<http://superuser.com/questions/287371/obtain-kernel-config-from-currently-running-linux-system/538110#538110>
```
cat /proc/config.gz | gunzip > running.config
```

-

이 방법으로 조사를 해보니까.. PREEMPT 패치가 적용이 되어있더라. / uname -a에서도 확인이 가능하고.

-

아예 PREEMPT_RT라는 선택 옵션이 없더라. 커널 컨피그에 들어가봤는데..
그래서 보니까, 이게 공식적으로 지원되는 그런게 아니고, 따로 패치를 해서 만들어 내줘야 하는 설정이었다.
바닐라 커널에다가 패치를 해야 하는 것이고..
뭐 일단 해도 잘안되더라는 얘기도 하나 찾았었지만,
<http://forum.odroid.com/viewtopic.php?f=77&t=4870>

이거 보면, PREEMPT 태그에다가 커널 버젼에 -rt## 이라는 게 붙어있다. 이렇게 되면 PREEMPT RT 패치가 되서 해당설정이 적용됐다는 얘기다.

근데, 이 사람의 얘기가 결국 자기가 이렇게 저렇게 해봤는데 잘안되더라는 얘기였다.

뭐 일단 나도 뭐.. 기대감없이 한번 패치 적용해봤다.

-

패치 적용하는 방법은.. 일단 아래를 따라하면 되는데.
<https://rt.wiki.kernel.org/index.php/RT_PREEMPT_HOWTO#About_the_RT-Preempt_Patch>
<https://github.com/hardkernel/linux/tree/odroidxu3-3.10.y>
<https://www.kernel.org/pub/linux/kernel/projects/rt/3.10/older/>
```
bzcat ../patch-2.6.23.1-rt11.bz2 | patch -p1
```
이렇게 하라고 해서. 위에 2개 링크에서 받은 파일들을 가지고 진행했는데..
받은 파일이 bzip이 아니고 gzip이어서..이렇게는 할 수가 없다.
어디서 찾았었는지는 모르겠는데 어떻게 어떻게 해서 gzip 으로 된 패치를 적용하는 명령을 찾았는데, (만들어냈나?)
다음과 같이 하면 된다.
```
sudo gzip -cd /usr/src/patches-3.10.72-rt77.tar.gz | patch -p1 --verbose
```

-

패치가 어떻게 동작하는지는 어떻게해서 만들어지는지와도 관계가 있는데 다음을 보면 된다.

<http://www.linuxchix.org/content/courses/kernel_hacking/lesson9>
```
diff -uNr linux.vanilla linux.new > patchfile
```

즉 diff를 통해서 나온게 patch다. 변경 내용을 적용하는 것 뿐이란 말이지.

-

그외에 패칭을 관리하는 툴로 ketchup이라는 게 있는데.. 참고해보자.
<https://rt.wiki.kernel.org/index.php/Ketchup>

-

패치를 적용하면서 이런저런 에러가 났는데..
```
reversed patch detected
```
이라고 하는 에러가 났다.
즉, diff에서 가정하는 몇번째 줄에 어떤게 있을 것이고.. 그것을 뭘로 바꿔라라는게 있는데.
그게 이미 수정된 파일이면 그게 될리가 없다. 아마도 그런 종류의 에러가 아닐까 싶다.
<http://www.linuxquestions.org/questions/linux-software-2/reversed-or-previously-applied-patch-detected-389687/>

-

그래서 바닐라에 패치하고 odroid 패치를 이후에 적용하는 걸 한번 생각해봤는데...
<http://forum.odroid.com/viewtopic.php?f=111&t=7866>

odroid 패치 같은건 따로 없는 것 같고.. 아마 없을 것 같다.

-

**odroid howto's and guides.**
<http://forum.odroid.com/viewforum.php?f=50>

**ubuntu's kernel patch guide**
<http://ubuntuforums.org/showthread.php?t=2273355>

<http://raspberrypi.stackexchange.com/questions/8970/default-kernel-preemption-vs-real-time-patch>
기본 PREEMPT 와 RT_PREEMPT 에 대한 성격/성능 질문.
결국 대답자는 hard real-time을 원하다면 Xenomai등을 사용할 것을 추천하고 있다.

CONFIG_PREEMPT의 의미도 아키텍쳐에 따라서 조금씩 다르다고 한다.
<http://cateee.net/lkddb/web-lkddb/PREEMPT.html>

**where to get vanilla kernel?**
바닐라 (vanilla) 커널을 구하려면 kernel.org로 가면된다.
<https://www.kernel.org/>

**7z (7zip) @ linux**
7z을 실행하기 위해서는 깔아야 되더라.
<http://askubuntu.com/questions/348173/how-to-install-7zip-through-terminal-to-extract-rar-files>

**What does 2>/dev/null mean…?**
<http://askubuntu.com/questions/350208/what-does-2-dev-null-mean>
리눅스 기본중의 하나인데.. 스크립트를 보다가 궁금해져서..
```
The > operator redirects the output usually to a file but it can be to a device. You can also use >> to append.

If you don't specify a number then the standard output stream is assumed but you can also redirect errors

> file redirects stdout to file
1> file redirects stdout to file
2> file redirects stderr to file
&> file redirects stdout and stderr to file

/dev/null is the null device it takes any input you want and throws it away. It can be used to suppress any output.
```

**rootfs expansion @ odroid**

커널 컴파일에 10G가 필요하다고 해서.. eMMC 파티션 확장을 해야 했다.
뭘 깔아야 되더라.
<http://forum.odroid.com/viewtopic.php?f=112&t=8487>
```
/usr/local/bin/root-utility.sh
```
인터넷이 연결되어있어야 한다.

<http://forum.odroid.com/viewtopic.php?f=22&t=1046>

**make -j8**

커널 빌드 커멘드로 나오는 이거의 의미는.. 쿼드 코어라는 말이다. 프로세서 갯수 *2 를 줘서 컴파일을 시킨다고 한다.
<http://www.linuxtopia.org/online_books/linux_kernel/kernel_configuration/ch05s04.html>
멀티 쓰레드가 어떻게 될 건지에 관련된 설정인것 같다.

일단 커널 컴파일을 하는데 odroid에서.. 이걸 따라서 했다.
<http://odroid.us/mediawiki/index.php?title=Step-by-step_Native_Compiling_a_Kernel>

수십번 적었지만. <https://github.com/hardkernel/linux/tree/odroidxu3-3.10.y> 이게 내 odroid용으로 현재 소스다.

**cloud with odroid as a server : Seafile Cloud**

<https://hackaday.io/project/2290/logs>
이 프로젝트 로그. 불친절하지만, 몇가지 쓸만한 팁이 있을 것 같다. 과정이 다 기록되어있다.

---

######2015Jun15 21:15:00+0900

*여튼 결론은 잘 안되더라는 얘기고.*
*오히려 긍정적인 방향은 odroid에서 작업하기 전의 바닐라 커널에다가 패치를 하고, odroid 변경 내용을 diff로 patch로 만들어서 RT 설정이후에다가 패치를 적용한다.*
*근데 분명히 충돌이 날텐데.. 어떤걸 오버라이트하고 어떤걸 스킵할지 나는 알 수가 없으니..*
*이방법은 거의 불가능할 것 같다는게 결론이다.*

* 그러니깐. Xenomai로 기존 커널 (PREEMPT 까지 적용되어있으니 나쁘지 않다.)을 올리거나.
* 기존 커널에서 priority를 50이하 49로 줘서 코드를 짜서 테스트해보고, pthread를 적용해서 성능이 나오는지 보는 2가지 방향이 있다.
* 또는 2가지 다 해보는 3번째 방법까지 3가지 방법을 생각할 수 있다.

-

