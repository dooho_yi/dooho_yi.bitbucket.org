
######2015Jun06 16:11:27+0900

DJ amsterdam 의 sequel.

하고 싶은 거 하자. 이야기 나눈 후에. 요약된 게 약 3가지인데.

1) rec&play 모듈
2) 정전식 grid 방식. - byop 할때, 해봤던.. 정전식 터치 스크린의 오브젝트 적인 확장.
3) 이미지 play / DJ amsterdam - 책(두호) / 원정(포스터)

mozzi / rpi. 중 뭘로 구현할까? 하다가.. 책을 만들꺼니까. 오브젝트가 작게 달렸으면 좋겠고..
그래서, mozzi로 진행하기로 했다.

-

mozzi로 진행을 하려면, 그래도 arduino nano 를 가지고 하려면, 한계가 부딪힐 것 같아서, teensy 3.1로 하기도 결정. 하고 3개 구매 했다. 곧 도착 할 것으로 보이는 WIFI ESP8266 보드 4개도 이용이 가능 할 것이다. 그렇게 되면 WIFI로 연결하는 책이 되겠지.

-

teensy는 아직 도착하려면 시간이 걸릴 것이니까. 일단 arduino nano 기반으로 테스트를 한다.
test용 오브젝트를 몇가지 만들어 낸다.

**sound feedback**을 주는 오브젝트도 필요하지만, **graph로 visual feedback**을 주는 오브젝트도 필요할 것 같다.

-

1) mozzi / sine-freq.
2) mozzi / wave-speed.
3) mozzi / sine-vol.
4) mozzi / wave-vol.
5) graph, scope -> rpi + openframeworks + arduino micro

등등을 생각해봤고. 이중. 1, 3은 만들어서 넘겼고.
2, 4, 5번은 아직이고.
이중 2/4는 빨리 진행했으면 좋겠는 상태.

-

######2015Jun07 00:00:41+0900

**openframeworks on rpi**

rpi에서 OF를 돌리는 경우에 대해서 알아봤다. 이유는.. 5번 경우. 그래프 표시를 효과적으로 하기 위해서인데.. 음.. 기왕이면 pure data로 하면 좋겠으나.. 뭐 속도면에서나.. OF가 성능이 더 나을 것이므로.. 별 생각없이 ... 예전에 arduino GUI 코드도 있었고 해서..

-

<http://openframeworks.cc/setup/linux-codeblocks/>
<http://openframeworks.cc/setup/raspberrypi/>
<http://openframeworks.cc/setup/raspberrypi/Raspberry-Pi-Workflow-Overview.html>
<http://openframeworks.cc/setup/raspberrypi/Raspberry-Pi-Getting-Started.html>

이쯤되면.. 꽤 잘 준비되어있는 편인거 같다.

-

######2015Jun07 00:21:35+0900

**understanding & working with mozzi**

<http://sensorium.github.io/Mozzi/learn/introductory-tutorial/>

<img src="http://sensorium.github.io/Mozzi/learn/introductory-tutorial/images/fig4.jpg" width=500px>

기본 셋팅.

<https://github.com/sensorium/Mozzi/releases> : mozzi 다운로드.
<http://arduino.googlecode.com/files/arduino-1.0.5-r2-windows.zip> : mozzi는 arduino 1.0.5 이상은 아직 쓰지 말것.
<https://github.com/sensorium/Mozzi>
wav파일을 헤더 파일로 변환해주는 툴. : <http://forum.arduino.cc/index.php?topic=300170.0> : char2mozzi.py
<https://github.com/sensorium/Mozzi/blob/master/extras/python/char2mozzi.py>
```
#!/usr/bin/env python

##@file char2mozzi.py
#  @ingroup util
#   A script for converting raw 8 bit sound data files to wavetables for Mozzi.
#
#   Usage: 
#   >>>char2mozzi.py <infile outfile tablename samplerate>
#   
#   @param infile       The file to convert, RAW(headerless) Signed 8 bit PCM.
#   @param outfile  The file to save as output, a .h file containing a table for Mozzi.
#   @param tablename    The name to give the table of converted data in the new file.
#   @param samplerate   The samplerate the sound was recorded at.  Choose what make sense for you, if it's not a normal recorded sample.
#
#   @note Using Audacity to prepare raw sound files for converting:
#   
#   For generated waveforms like sine or sawtooth, set the project
#   rate to the size of the wavetable you wish to create, which must
#   be a power of two (eg. 8192), and set the selection format
#   (beneath the editing window) to samples. Then you can generate
#   and save 1 second of a waveform and it will fit your table
#   length.
#   
#   For a recorded audio sample, set the project rate to the
#   Mozzi AUDIO_RATE (16384 in the current version). 
#   Samples can be any length, as long as they fit in your Arduino.
#   
#   Save by exporting with the format set to "Other uncompressed formats",
#   "Header: RAW(headerless)" and "Encoding: Signed 8 bit PCM".
#   
#   Now use the file you just exported, as the "infile" to convert.
#   
#   @author Tim Barrass 2010-12
#   @fn char2mozzi

import sys, array, os, textwrap, random

if len(sys.argv) != 5:
        print ('Usage: char2mozzi.py <infile outfile tablename samplerate>')
        sys.exit(1)

[infile, outfile, tablename, samplerate] = sys.argv[1:]

def char2mozzi(infile, outfile, tablename, samplerate):
    fin = open(os.path.expanduser(infile), "rb")
    print ("opened " + infile)
    uint8_tstoread = os.path.getsize(os.path.expanduser(infile))
    ##print uint8_tstoread
    valuesfromfile = array.array('b') # array of signed int8_t ints
    try:
        valuesfromfile.fromfile(fin, uint8_tstoread)
    finally:
        fin.close()
    
    values=valuesfromfile.tolist()
    fout = open(os.path.expanduser(outfile), "w")
    fout.write('#ifndef ' + tablename + '_H_' + '\n')
    fout.write('#define ' + tablename + '_H_' + '\n \n')
    fout.write('#if ARDUINO >= 100'+'\n')
    fout.write('#include "Arduino.h"'+'\n')
    fout.write('#else'+'\n')
    fout.write('#include "WProgram.h"'+'\n')
    fout.write('#endif'+'\n')   
    fout.write('#include <avr/pgmspace.h>'+'\n \n')
    fout.write('#define ' + tablename + '_NUM_CELLS '+ str(len(values))+'\n')
    fout.write('#define ' + tablename + '_SAMPLERATE '+ str(samplerate)+'\n \n')
    outstring = 'const int8_t __attribute__((section(".progmem.data"))) ' + tablename + '_DATA [] = {'
    try:
        for i in range(len(values)):
            ## mega2560 boards won't upload if there is 33, 33, 33 in the array, so dither the 3rd 33 if there is one
            if (values[i] == 33) and (values[i+1] == 33) and (values[i+2] == 33):
                values[i+2] = random.choice([32, 34])
            outstring += str(values[i]) + ", "
    finally:
        outstring +=  "};"
        outstring = textwrap.fill(outstring, 80)
        fout.write(outstring)
        fout.write('\n\n#endif /* ' + tablename + '_H_ */\n')
        fout.close()
        print ("wrote " + outfile)

char2mozzi(infile, outfile, tablename, samplerate)
```

---

######2015Jun07 12:44:29+0900

회의.

-

글자나 단어를 하나씩 인식하면 어떨까. 글자나 단어를 읽으면서, 속도나 볼륨이 바뀌게 하면 어떨까?

<div><img src="image001.png" width=500px></div>

글자/단어 위치 인식은,
스캐닝을 하면 될 것 같다.
기존의 GND 라인을 따로따로 digital pin으로 할당하고. HIGH/LOW를 통해 active한 주사선을 결정해주면서 빠른 속도로 scanning 하면 2D 를 인식가능할 것이다. 기술적으로 흥미로운 지점이 있다.

-

analog입력으로 저항을 읽어 내는 것도 가능하다. 굳이 스캐닝을 하지 않아도. -> 악기 같은 것은 정확하게 인식할 필요가 없으면, 그렇게 하는게 더 효과가 좋을 수도 있도. slide도 가능해지기 때문에.

-

<div><img src="image002.png" width=500px></div>

꽝! 이라고 그려져있는 것. 만화의 글적글적. 살금살금 등등의 의성어가 만화처럼 적혀있고 이것을 소리로 작동시킬 수 있게 한다.

-

책이니까. 게임북. 은 어떨까? 오디오 게임 북.
추리 게임 북 같은거. 아가사크리스티의 밀실사건. 같은거. 대답으로 고르고 지시하는 페이지로 따라가면서 읽는 거.

<div><img src="image003.png" width=500px></div>

그런데, 그림이 '꽝' 같이 추상적이라면. 만화적 표기의 의성어 처럼 메타적이라면.

1) 이야기가 독립적일 수도 있겠다. - 같은 이미지 북에 대해서 다른 sd flash 데이터를 넣으면, 다른 맥락이 작용해서.
추리소설이었다가, 연예소설이었다가. 레시피가 되기도 하고.

2) 전자장치가 있어서. 같은 페이지가 맥락에 따라서 다른 내용이 나올 수도 있을 것이다. - 4-3-2-8-7- 페이지를 거쳐서 5 페이지에 도달한 경우와 4-8-7-2- 페이지를 거쳐 5페이지에 도달했을때, 다른 이야기가 펼쳐진다는 말이다.

-

아날로그로 한다고 하면, 가느다란 선도 좋을 것 같다. 길이가 제대로 인식이 안되더라. 8B 연필이라던가.

<div><img src="image004.png" width=500px></div>

-

아날로그로 해서, 도레미파솔라시도가 각각 stripe으로 되어있고, 예전 이태리 다비드 할아버지네 물건에서 봤던 거 처럼. 그래픽 악보처럼. 할 수가 있을 것이다. -> 근데, 그러면, 정확한 음정이 안나오게 되서. 어려울수도 있다. -> digital로 할 수도 있다! scanning을 해서 위치를 잡아내면되지.

<div><img src="image005.png" width=500px></div>

-

플랫폼을 만들려고 한다는데, 그렇다면, 제품으로 뽑아야 할 것이고. 나무로 제작하면 좋겠다. -> 종이가 좋은데 -> punch card로 하는 방식이면, 종이로 한다는 게 무색해질 수가 있다. 왜 (쉽게 망가지는) 종이로 하는가? 대답이 필요한데.. 6개월에 한번씩 밑판의 회로 종이를 갈아줘야 한다던가. 그렇게 생각하고.. 기능적으로 탄탄하게 (다비드 할아버지 물건 처럼) 장시간 사용할 수 있게 만들면 좋겠다.

<div><img src="image006.png" width=500px></div>

-

punch card 하니까 얘기가 나와서 말인데,
오르골처럼도 만들 수 있는 거 아닌가? 돌출된 부분이 있어서, 1줄로 된 어떤 것을 연주하는 거지.

<div><img src="image007.png" width=500px></div>

-> 머 좀 넘 복잡스럽고. 그런걸 만들고 싶은건 아니고. 머 굳이 한다면 이런식이 나은데..

<div><img src="image008.png" width=500px></div>

-> ㅇㅇ. 알겠음. but, 난 종이 오브제에도 관심이 있다. (오토마타 같은.)

-

체스판도 생각했었다. 체스말이 움직이면, 예를들어, 나이트가 움직이면, '나를 따르라!' 퀸이 움직이면, '신이시여, 우리를 보살피소서', 왕은 '하늘이 붉게 물들었구나' 등등. -> 체스판은 너무 뻔하지만, 뭐 재밌다고 해주자.

<div><img src="image009.png" width=500px></div>

---

책에 대해서 말인데. 너무 난 복잡하고 어렵게 생각안했으면 좋겠다. 내가 생각하는 책은 정말 8 페이지 짜리 소책자일 뿐이다.

<div><img src="image010.png" width=500px></div>

noise book 이라고 하자. 그럼 white / brown 노이즈 등등.. 그래픽적이고 컨셉적으로 표현한 짧은 책이면 된다.

<div><img src="image011.png" width=500px></div>

<div><img src="image012.png" width=500px></div>
그리고, 이야기 북, 추리 북, 연애 북, 악기 북, 야채 북.... 여러가지 재미있는 것들을 역시 8p 형으로 (어쩌면, 브로마이드로 펼칠수도 있는 버젼으로..) 간략하게 제작해서 모아본다.

<div><img src="image013.png" width=500px></div>
그러다가, 아이디어가 좀 모이면 장편의 북으로 기획해보고. 그러면 된다. 첨부터 '내용' 에 대해서 고민하면서 에너지 소모하지 말자.

-

악기가 각각 한 줄에 할당되어있고, 여러 악기가 있으며, 드럼/베이스/피아노.. 한페이지에서 나오면 좋겠고. 패턴이랄까. 그런게 표시되어있고.. 그래서 긱 / jam을 할 수 있는 게 있으면 좋겠다. -> ableton 에서 track을 하듯이..  여러 track이 저장되어있고. 이를 구간별로 연주하는 것도 좋겠다. 이런거 할려면, DJ amsterdam 같은 DJ 페이지가 되는 거 같고. 그럴려면, loop & play가 구현이 되어야 한다. -> 머 그렇게 까지 하자는 건 아니고. 좀더 아날로그 하게..

<div><img src="image014.png" width=500px></div>

-

<div><img src="image015.png" width=500px></div>

그냥 한줄이 그려져있고, 연주를 하는 과정이 그림 처럼 표시되면 어떨까. 선을 따라서 손가락을 움직이면 미리 만들어진 멜로디가 나온다. -> 박자적인 부분은 멜로디를 사용자가 인지하게 되면, 스스로 만들어나갈 것이다.

박자를 동글동글로 표현해도 괜찮겠다. 안에서 머물다가 나올 수 있게. (이런 방식은 하나의 악보 표기법일 수도 있겠다.)

-

그러니까 이런식으로 피아노도 있고, 베이스도 있고, 드럼도 있고.. 각각이 그냥 있고. 아날로그적으로 (진짜 악기 같이. ableton이 아니라.) 있어서.. 여럿이 하면 좋겠는데. -> 그렇다면 크게 만들어서 A3나.. 전지 사이즈로 해서.. 여럿이서 각각 한 줄씩 담당해서 하면 가능하겠는데? -> 그렇다.

-

<div><img src="image016.png" width=500px></div>

노래의 느낌에 따라서, 곡선이 아닌 직선. 또는 다른 종류의 선. 점선등등 일수가 있다.
그럼 베이스 같이 반복적인 거는 그냥 1 자가 될 수도 있겠는데 언제 나가야 할지 몇번연주하는 건지 정말 헷갈릴텐데.. -> 그럼 뭐.. 원자력 모양 처럼 동글동글 돌릴까? / 숫자를 적어줘도 되고.

-

빅 밴드 / 오케스트라. 가 한장의 종이에 다 들어있다고 보면 된다. (rec & loop 없이.. / 있어도 되고. 그런건 추가기능이다.)
큰종이 / 포스터에다가 잘 만들어서 (저항치나 물질의 특성도 고려해서, 고르게 인쇄해서 정말 잘 사용할 수 있게 만들어서) 팔 수도 있겠다!

-

좋다좋다. 재밌겠다. (끝.)

---

######2015Jun07 14:09:43+0900

회의 내용이 흥미진진.

-

이제는 뭔가 만들어보자.

지난 번 리스트 다시 가져와보면.

1. mozzi / sine-freq.
2. mozzi / wave-speed.
3. mozzi / sine-vol.
4. mozzi / wave-vol.
5. graph, scope -> rpi + openframeworks + arduino micro

이랬는데. 2번이랑 4번은 일단 보류하고.. 5번은? 뭐 틈틈히 하고.

지금은 새로운 주제.

-

######2015Jun07 15:12:40+0900

지난 번 얘기했던 내용은. 뭔가 중간적인 인터페이스가 필요하고.. mobmuplat 처럼 gui component를 정의하면 될 것인데. 그게 걸맞는 종이 회로용 버젼을 만들어봐야 한다고 했다.

크게, analog / digital / sequence of switches. 세가지에 대해서..

-

지금으로서는.. 이러한 모듈화가 필요할지 어떨지 의문도 들기도 하고.. 음.

-

만들고자 하는 것이 뭐라고 봐야 하는 걸까?

-

지금 한줄 버젼으로 하는 게. 좀 문제가 있는게.. 2 layer라는 게 고려가 잘 안되어있다.

-

pogo pin 같은게 있어야 접촉 기구가 안정적으로 될 것 같다.

그렇지 않으면 뭔가를 제작해야 하는데.. 악어 클립을 개량하거나.. 등등.

-

책을 만드는 건 좋은데. 이걸 어떻게 연결 시킬 것인지 의문이다.

-

제일 먼저 해볼만한 것들이.

1. noise book
2. scanning
3. 1-liner method

정도로 요약.

스캐닝하는 거는. 대표적으로.. 다비드 할아버지 꺼 같은거. 연주되는 어떤것.
원 라이너도. 그렇다.

-

반투명 종이/PET 탑이 있으면 좋겠다.

디지탈 방식에서도 투명해야 보이고.
아날로그 방식에서는 칸막이가 걸림돌이 된다.

-

글자에 대한 거는.
ABCDEFG를 해도 좋을 것 같다. 노래 소리까지 해서. 노래로.
A-B-C-D-EFG~ H-I-J-K-LMN~

---

######2015Jun07 16:58:59+0900

연결 방식을 고안해야 하는데..

---

######2015Jun18 15:58:46+0900

어쩌다가 이게 나한테 쉐어가 되었는지 모르겠지만, 유용해보이는 글이다..

<https://docs.google.com/document/d/1ElH6kK4ZsIgavpTLQ5zeR3uSoCK8TsLoH_fYYILPtwE/edit#>
혹시나 공유해제 될까봐.. 다운도 받아놨음. 파일명은 -> '체결방식.pdf' / '체결방식.zip' (html이다.)

-

커뮤니케이션 북스에서 이쪽 아이디어에 관심이 있는 것 같다.. 마승길씨 개인적 관심에 그칠 수도 있지만.. / 그치만 그들은 어차피 회사... 너무 좋아할 일은 아니지만, 신중하게 .. 그렇지만, 긍정적으로 생각해볼 수 있다.

---

######2015Jun27 23:59:51+0900

커넥터 아이디어..

* 그냥 스프링.. 스프링으로 눌른다. 5mm 정도의 짧은 스프링, 탕탕한걸로.. 두께를 많이 희생 안하면서 눌러준다.
* 스프링 방법에다가.. 스프링 직경에 딱 맞는.. 구슬을 구해서 1칸 안쪽에 끼워준다. 접촉이 좀더 안정적일 것 같다.
* 예전에 했던 파일클립 + 자석..
* 판형 스프링.. 태엽 같은 소재로 판형 스프링을 폭 5mm 길이 한 2cm 정도로 잘라서. 말아서 끼운다. 동글려진 부분이 스프링 효과를 가지면서.. 접촉면이 부드럽게 눌린다.
* 같은 건데.. 선형 스프링으로 그렇게 해도  좋다..

스프링 종류를 했을 때에는.. 납땜 혹은 접촉을 안정적으로 할 수 있는 방법이 필요하다. 납땜이 안될 수가 있으니깐...

태엽에서 가져다가 써도 좋을 것 같다.. 아래 사진 같은거.. 싸구려 줄자등에서 꺼내면 된다.
```
벨트 폭 7cm 길이 2.2 m 용 자동차단봉에 사용하던 태엽스프링 입니다. 개발 생산하고 남은것으로 스프링 규격은 두께 0.24 mm , 폭 7 mm , 길이 3,800 mm SuS 301 EH 이며 수량은 1,000 ea 입니다. 필요하신 분 연락주세요. 011-9594-4901
```
2007년의 글이니까 아마 연락해도 구할 수 없을 테지만.. 이런식으로 누군가 제작을 하고 남은 재료를 구할 수도 있다.

<div><img src="planarspring.jpg"></div>

아니면, 가격이 비싸지 않으면, 아래와 같은 스프링을 구하거나.. 제작의뢰해도 된다.

<div><img src="20100613155267.jpg" width= 300px></div>

수은 전지 홀더에도 쓸만해보이는게 있다.

<div><img src="http://www.pjrc.com/teensy/td_libs_Time_8.jpg"></div>

---

