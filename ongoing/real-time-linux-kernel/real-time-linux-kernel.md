######2015Jun14 23:50:15+0900

**real-time linux kernel**

CONFIG_PREEMPT_RT
Xenomai
RTAI

"라즈베리파이를 아두이노 처럼 실시간 (모터 돌리고 하는 거, 그냥 돌리는 거 말고, 정밀하게 CNC 같이 - time-critical task) 으로 쓰고 싶으면 할 수 있는 방법들이 몇가지 나와있는데.. 리눅스커널의 CONFIG_PREEMPT_RT 패치라던가.. 가상머신으로 리눅스를 가상화 시키고 아두이노 같은 RTOS를 메인 OS로 가져가는 방법 (Xenomai / RTAI) 등등.. 이들의 성능을 실험으로 비교한거에요. // 솔직히 일때문에 보고 있긴한데.. 이런게 잘 해결되면, 라즈베리로 오디오 작업하는 거에도 도움이 될 것 같아요.. 지금 얻어 쓰고 있는 Satellite CCRMA 커널도 이렇게 real-time 최적화 되어있는 커널인데.. 아직은 스스로 만들 능력이 없지만, 만들고 싶다고 생각을 종종했음."

-

"Xenomai 는 인터럽트가 많이 걸리는 physical task에 강하다고 하고.. (CNC같이) PREEMT_RT 패치 걸린 Linux는 스케쥴링이 잦은 시스템 (아마도 서버 같은거? 뭔지 잘 모르겠음) 에 좋은 성능을 가진다고 한다. 근데, Xenomai는 리눅스가 아니어서 어차피 코드를 새로 짜야하고 (POSIX 등 지원함).. PREEMT_RT패치는 걸어도 Linux 인것에는 변함이 없으므로.. 일단 나쁘지 않다.. 단 커널 태스크를 하나하나 chrt로 우선순위를 조정해줘야만 성능이 나오므로 참고."

-

논문.

<http://www.diva-portal.org/smash/get/diva2:158978/FULLTEXT01.pdf>

"결론은 Xenomai가 짱이더라는 얘기고.. 아마도 그래서.. LinuxCNC도 http://wiki.linuxcnc.org/cgi-bin/wiki.pl... Xenomai를 쓰고있나보다."

<http://wiki.linuxcnc.org/cgi-bin/wiki.pl?RaspbianXenomaiBuild>